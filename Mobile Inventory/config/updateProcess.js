(function (dmsi) {
    "use strict";
    
    dmsi.updateProcess = {

        serviceIsRunning: false,
        updateIsRunning: false,
        
        init: function () {
            logWriter.writeLog("updateProcess.js",0,"init()");
        },
        
        checkOutstanding: function() {
            return false;
        },
        
        updateControl: function () {
            logWriter.writeLog("updateProcess.js",0,"updateControl()");
            var that = this;
            
            var connection = navigator.onLine ? "online" : "offline";
            if ( ! dmsi.config.isConnected() ) {
                logWriter.writeLog("updateProcess.js",0,"updateControl(): Not connected");
                return;
            }
            
            if(that.serviceIsRunning) {
                logWriter.writeLog("updateProcess.js",0,"updateControl(): Service already running");
                return;
            }
            
            that.serviceIsRunning = true;
            
            // Running inside a timeout so we'll be async ...
            setTimeout(function() {
                
                // Check database for outbound data needing to be sent ...
                logWriter.writeLog("updateProcess.js",0,"updateControl(): started");
                that.processData();
                
                logWriter.writeLog("updateProcess.js",0,"updateControl(): done");                
                
            }, 0);
            
            that.serviceIsRunning = false;
        },
        
        goingOffline: function () {
            logWriter.writeLog("updateProcess.js",0,"goingOffline()");
            
        },
        
        comingOnline: function () {
            logWriter.writeLog("updateProcess.js",0,"comingOnline()");  
            if ( dmsi.model.isUserLoggedIn === true )
                dmsi.updateProcess.updateControl();
        },
        
        processData: function () {
            //    Get the contents of the sequences table to iterate through the other tables ...
            logWriter.writeLog("updateProcess.js",0,"processData()" + dmsi.updateProcess.updateIsRunning);
            var that = this;
            var sequenceSet;
            
            if(dmsi.updateProcess.updateIsRunning) {
                logWriter.writeLog("updateProcess.js",0,"processData(): Already running, returning.");
                return;
            }
            
            dmsi.updateProcess.updateIsRunning = true;
            
            dmsi.dbManager.getSequencesQuery().done ( function (sequencesResultSet) {
                 that.sequenceSet = sequencesResultSet;
                 
                 if(that.sequenceSet !== undefined) {
                    var seq = 0;
                    
                    var processLoop = function() {
                        logWriter.writeLog("updateProcess.js",0,"processData() getSequencesQuery: loop starting seq " + seq + " of " + that.sequenceSet.rows.length);
                        
                        //We lose the synchronous processing inside the loop so set the updateIsRunning at the beginning of each pass and unset it only when the number of rows is exceded
                        dmsi.updateProcess.updateIsRunning = true;
                        
                        if ( seq < that.sequenceSet.rows.length) {
                            var rowSequence = that.sequenceSet.rows.item(seq).sequence;
                            var tranType = that.sequenceSet.rows.item(seq).transaction_type;
                            seq+= 1;    
                            that.processSequence(rowSequence, tranType).done ( processLoop );
                        }
                        else {
                            //When this loop ends, unset the updateIsRunning flag so the next set can start
                            logWriter.writeLog("updateProcess.js",0,"processData() getSequencesQuery: loop ending seq " + seq + " of " + that.sequenceSet.rows.length);
                            dmsi.updateProcess.updateIsRunning = false;
                        }

                        return;
                    }
                    processLoop();
                 }
                else {
                    dmsi.updateProcess.updateIsRunning = false;
                }
            })
            .fail (function (error){
                dmsi.updateProcess.updateIsRunning = false;
            });        
            
            logWriter.writeLog("updateProcess.js",0,"processData(): ending");           
        },
        
        processSequence: function ( sequence, tranType ) {
            var that = this;
            var deferred = $.Deferred();
            
            logWriter.writeLog("updateProcess.js",0,"processSequence(): starting sequence " + sequence);
            
            switch(tranType) {
                case "MoveTag":
                        dmsi.dbManager.doTagMove(sequence).done(function () {
                            deferred.resolve();
                        });
                        break;
                case "VerifyTag":
                        dmsi.dbManager.doTagVerify(sequence).done(function () {
                            deferred.resolve();
                        });
                        break;
                case "WorkorderCompletion":
                        dmsi.dbManager.doWorkorderCompletion(sequence).done(function () {
                            logWriter.writeLog("updateProcess.js",0,"processSequence(): ending sequence " + sequence);
                            deferred.resolve();
                        });
                        break;
                case "DepartmentScanning":
                        dmsi.dbManager.doDepartmentScanning(sequence).done(function () {
                            logWriter.writeLog("updateProcess.js",0,"processSequence(): ending sequence " + sequence);
                            deferred.resolve();
                        });
                        break;                
                default:
/*                        dmsi.dbManager.deleteSequence(sequence).done(function () {
                            logWriter.writeLog("updateProcess.js",0,"processSequence(): Deleted sequence " + sequence + " for " + tranType);                    
                            deferred.resolve();
                        });
*/                        deferred.resolve();
                        break;
            }
            
            return deferred.promise();
        },
    }    
    
    document.addEventListener("offline", dmsi.updateProcess.goingOffline);

    document.addEventListener("online", dmsi.updateProcess.comingOnline);
}( dmsi ));