
( function( dmsi ) {

    "use strict";

    dmsi.config = {
        settings: {
            "version": "1.0"
        },
        messages: {
            "loginFailed": "Invalid User ID or Password.",
            "invalidURL": "Invalid URL.",
            "cannotLogin": "Session has expired.",
            "noConnection": "Device is not connected.",
            "noServer": "Unable to login to server.",
            "printerNotFound": "Invalid printer.",
            "logoutOutstandingData": "There are records waiting to be sent to Agility. You cannot log out until the records are processed."
        },
        views: {
            "login": "views/login.html",
            "mainMenu": "views/mainMenu.html",
            "settings": "views/settings.html",
            "tagInfo": "views/tagInfo.html",
            "moveTags": "views/moveTags.html",
            "workfiles": "views/workfiles.html",
            "verifyTags": "views/verifyTags.html",
            "splitTags": "views/splitTags.html",
            "printTags": "views/printTags.html",
            "workorderCompletion": "views/workorderCompletion.html",
            "departmentSelection": "views/departmentSelection.html",
            "departmentScanning": "views/departmentScanning.html",
            "departmentScanOnly": "views/departmentScanOnly.html"
        },
        jsdoSettings: {
            "serviceURI": "",  //http://restapps.dmsi.com:8980/MobileWH
            "catalogURIs": "",     
            "authenticationModel": "Form",
            "displayFields": "Name",
            "resourceName": "dsCustomer"        
    	},
        rollbaseSettings: {
            baseUrl : "https://diy.dmsi.com/rest/api/"     
        },        
        isConnected : function() {
            var connection = navigator.onLine ? "online" : "offline";
            if ( connection === "online" )
                return true;
            else
                return false;
        }
     };

} ( dmsi ) );