(function (dmsi) {
    "use strict";
    
    dmsi.dbManager = {
        
        jsdoSession: null,
        
        db: null,               
        
        init: function () {
            logWriter.writeLog("dbManager.js",0,"init()");

            this.dbSequence = 0;
            this.openDB();
	        this.createTables();
            
        },
        
        openDB : function() {
           var dbName = "MobileWHToolsDB";
           if (window.navigator.simulator === true) {
                // For debugin in simulator fallback to native SQL Lite
                logWriter.writeLog("dbManager.js",0,"Use built in SQL Lite");               
                this.db = window.openDatabase(dbName, "1.0", "Cordova Demo", 200000);
            }
            else {
                logWriter.writeLog("dbManager.js",0,"Use sqlitePlugin");
                this.db = window.sqlitePlugin.openDatabase(dbName);
            }        
        },
        
        deleteTables : function() {
            logWriter.writeLog("dbManager.js",0,"DeleteTables");
        	this.db.transaction(function(tx) {
    		    tx.executeSql("DROP TABLE sequences" , []);
        	});    
            
            this.db.transaction(function(tx) {
    		    tx.executeSql("DROP TABLE moveTags" , []);
        	});     
            
            this.db.transaction(function(tx) {
        		tx.executeSql("DROP TABLE verifyTags" , []);
            });
            
            this.db.transaction(function(tx) {
        		tx.executeSql("DROP TABLE splitTags" , []); 
            });
            
            this.db.transaction(function(tx) {
        		tx.executeSql("DROP TABLE workorderCompletion" , []); 
            });
            
            this.db.transaction(function(tx) {
        		tx.executeSql("DROP TABLE departmentScanning" , []); 
            });
        },  
        
        createTables : function() {
            logWriter.writeLog("dbManager.js",0,"createTable");
        	this.db.transaction(function(tx) {
        		tx.executeSql("CREATE TABLE IF NOT EXISTS sequences(" +
                                "sequence INTEGER, " +
                                "transaction_type TEXT, " +    //    MT - Move tag, TV - Tag verify ...
                                "workfile_id TEXT, " +
                                "workfile_seq INTEGER, " +
                                "created_date DATETIME" +
                                ")", []);
            	});
            
        	this.db.transaction(function(tx) {
        		tx.executeSql("CREATE TABLE IF NOT EXISTS moveTags(" +
                                "sequence INTEGER, " +
                                "branch_id TEXT, " +
                                "tag TEXT, " +
                                "tag_desc TEXT, " +
                                "new_location TEXT, " +
                                "created_date DATETIME" +
                                ")", []);
            	});            

        	this.db.transaction(function(tx) {
        		tx.executeSql("CREATE TABLE IF NOT EXISTS verifyTags(" +
                                "sequence INTEGER, " +
                                "branch_id TEXT, " +
                                "workfile_id TEXT, " +
                                "workfile_seq INTEGER, " +
                                "tag TEXT, " +
                                "tag_desc TEXT, " +
                                "ready_to_process BOOLEAN, " +
                                "created_date DATETIME" +
                                ")", []);
            	});
            
             this.db.transaction(function(tx) {
        		tx.executeSql("CREATE TABLE IF NOT EXISTS splitTags(" +
                                "sequence INTEGER, " +
                                "branch_id TEXT, " +
                                "tag TEXT, " +
                                "item TEXT, " +
                                "size TEXT, " +
                                "desc TEXT, " +
                                "dimension_string TEXT, " +
                                "thickness DECIMAL, " + 
                                "width DECIMAL, " +
                                "length DECIMAL, " +
                                "piece_count INTEGER, " +
                                "lotcode BOOLEAN, " +
                                "contentcode BOOLEAN, " +
                                "location TEXT, " +
                                "lot TEXT, " +     
                                "content TEXT, " +
                                "split_qty DECIMAL, " +
                                "split_qty_uom TEXT, " +
                                "split_qty_conv DECIMAL, " +
                                "remaining_qty DECIMAL, " +
                                "created_date DATETIME" +
                                ")", []);
            	});   
            
            this.db.transaction(function(tx) {
        		tx.executeSql("CREATE TABLE IF NOT EXISTS workorderCompletion(" +
                                "sequence INTEGER, " +
                                "branch_id TEXT, " +
                                "wo_id INTEGER, " +
                                "qty_completing INTEGER, " +
                                "default_location TEXT, " +
                                "created_date DATETIME" +
                                ")", []);
            	});
            
            this.db.transaction(function(tx) {
        		tx.executeSql("CREATE TABLE IF NOT EXISTS departmentScanning(" +
                                "sequence INTEGER, " +
                                "branch_id TEXT, " +
                                "tran_id INTEGER, " +
                                "schedule_group_id INTEGER, " +
                                "schedule_group_qty_completed DECIMAL, " +
                                "reason_codes TEXT, " +
                                "note TEXT, " +
                                "num_employees INTEGER, " +
                                "created_date DATETIME" +
                                ")", []);
            	});
        },                
        
        getLastSequence : function () {
            logWriter.writeLog("dbManager.js",0,"getLastSequence()");
            var that = this;
            var dfd = $.Deferred();
            
            dmsi.dbManager.db.transaction(function(tx) {
                   tx.executeSql(
                        "SELECT sequence FROM sequences ORDER BY sequence DESC LIMIT 1", 
                        [], 
                        selectSuccess,
                        selectFail)
              	});                                          
            
            function selectSuccess (tx, rs) {
                if ( rs.rows !== undefined ) {
                    if ( rs.rows.length > 0 ) {
                        var row = rs.rows.item(rs.rows.length - 1).sequence;
                        if ( row !== undefined )
                        that.dbSequence = row;
                        logWriter.writeLog("dbManager.js",0,"getLastSequence(): Last sequence found " + that.dbSequence);
                    }
                    else {
                        that.dbSequence = 0;
                        logWriter.writeLog("dbManager.js",0,"getLastSequence(): No record found " + that.dbSequence);                        
                    }
                }
                else {
                    that.dbSequence = 0;
                    logWriter.writeLog("dbManager.js",0,"getLastSequence(): No result found " + that.dbSequence);                        
                }                   
                dfd.resolve();
            }
            
            function selectFail (tx, rs) {
                logWriter.writeLog("dbManager.js",0,"getLastSequence(): Query failed, starting at 0");
                that.dbSequence = 0;
                dfd.reject();
            }
            
            return dfd.promise();
        },
        
        setSequence : function (transactionType, workfileID, workfileSeq) {
            logWriter.writeLog("dbManager.js",0,"setSequence()");
            var that = this;
            var dfd = $.Deferred();
    		var addedOn = new Date();
            
            that.getLastSequence().done ( function () {
                that.dbSequence = that.dbSequence + 1;
                           
                dmsi.dbManager.db.transaction(function(tx) {
                    tx.executeSql("INSERT INTO sequences(sequence, transaction_type, workfile_id, workfile_seq, created_date) VALUES (?,?,?,?,?)",
                 				 [that.dbSequence, transactionType, workfileID, workfileSeq, addedOn],
                                  insertSuccess,
                                  insertFail);
            	});                                          
                
                function insertSuccess (tx, rs) {   
                    logWriter.writeLog("dbManager.js",0,"setSequence(): Sequence set to  " + that.dbSequence);
                    dfd.resolve();
                }
                
                function insertFail (tx, rs) {
                    logWriter.writeLog("dbManager.js",99,"setSequence(): Insert sequences failed for " + that.dbSequence + "!!!");
                    logWriter.writeLog("dbManager.js",99,"setSequence(): " + rs.code + ", " + rs.message);
                    dfd.reject();
                }
            });
            
            return dfd.promise();
        },
        
        addMoveTagTag : function(branch, tag, tag_desc, location) {
            
            logWriter.writeLog("dbManager.js",0,"addMoveTagTag()");                                   
            
            var that = this;
            
            //  This is writing a 'tag' sequence just to retain the sequence value.  ALl updating will be done on the 'MoveTag' sequence
            that.setSequence("Tag","",0).done ( function () {
                that.dbSequence = that.dbSequence + 1;
                
            	dmsi.dbManager.db.transaction(function(tx) {
                    logWriter.writeLog("dbManager.js",0,"addMoveTagTag(): Sequence " + that.dbSequence);
            		var addedOn = new Date();
            		tx.executeSql("INSERT INTO moveTags(sequence, branch_id, tag, tag_desc, new_location, created_date) VALUES (?,?,?,?,?,?)",
            					  [that.dbSequence, branch, tag, tag_desc, location, addedOn],
            					   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",0,"addMoveTagTag(): Tag added as sequence " + that.dbSequence);
                                       dmsi.moveTags.viewModel.getTagListDataSource(); 
                                   },
            					   function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"addMoveTagTag(): Insert moveTags failed for " + that.dbSequence + "!!!");
                                        logWriter.writeLog("dbManager.js",99,"addMoveTagTag(): " + rs.code + ", " + rs.message);
                                   });
            	});
            });
        },

        updateMoveTagTag : function(branch, tag, tag_desc, location) {
            
            logWriter.writeLog("dbManager.js",0,"updateMoveTagTag()");
            
            var that = this;
            
            //  This is writing a 'tag' sequence just to retain the sequence value.  ALl updating will be done on the 'MoveTag' sequence
        	dmsi.dbManager.db.transaction(function(tx) {
                logWriter.writeLog("dbManager.js",0,"updateMoveTagTag(): Tag " + tag);
        		var addedOn = new Date();
        		tx.executeSql("UPDATE moveTags SET tag_desc=? WHERE branch_id=? AND tag=? AND new_location=''",
        					  [tag_desc, branch, tag],
        					   function(tx, rs){
                                   logWriter.writeLog("dbManager.js",0,"updateMoveTagTag(): Tag updated for " + tag);
                                   dmsi.moveTags.viewModel.getTagListDataSource(); 
                               },
        					   function(tx, rs){
                                    logWriter.writeLog("dbManager.js",99,"updateMoveTagTag(): Update moveTags failed for '" + tag + "'!!!");
                                    logWriter.writeLog("dbManager.js",99,"updateMoveTagTag(): " + rs.code + ", " + rs.message);
                               });
        	});
        },
            
        addSplitTagTag : function(sequence, branch, tag, item, size, desc, dimension_string, thickness, width, length, piece_count, lotcode, contentcode, location, lot, content, splitQty, splitQtyUOM, splitQtyConv, remainingQty) {
            
            logWriter.writeLog("dbManager.js",0,"addSplitTagTag()");                                   
            
            var that = this;
                
            dmsi.dbManager.db.transaction(function(tx) {
                var addedOn = new Date();
            	tx.executeSql("INSERT INTO splitTags(sequence, branch_id, tag, item, size, desc, dimension_string, thickness, width, length, piece_count, lotcode, contentcode, location, lot, content, split_qty, split_qty_uom, split_qty_conv, remaining_qty) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            				  [sequence, branch, tag, item, size, desc, dimension_string, thickness, width, length, piece_count, lotcode, contentcode, location, lot, content, splitQty, splitQtyUOM, splitQtyConv, remainingQty],
            				   function(tx, rs){
                                   logWriter.writeLog("dbManager.js",0,"addSplitTagTag(): Tag added as sequence " + sequence);
                                   dmsi.splitTags.viewModel.getFromTagListDataSource(); 
                              },
            				  function(tx, rs){
                                   logWriter.writeLog("dbManager.js",99,"addSplitTagTag(): Insert splitTags failed for " + sequence + "!!!");
                                   logWriter.writeLog("dbManager.js",99,"addSplitTagTag(): " + rs.code + ", " + rs.message);
                              });
            	});
           
        },
            
       addVerifyTagTag : function(branch, workfile_id, workfile_seq, tag, tag_desc) {
            
            logWriter.writeLog("dbManager.js",0,"addVerifyTagTag()");
           
            var that = this;
            
            that.setSequence("VerifyTagInProcess",workfile_id, workfile_seq).done ( function () {
                
            	dmsi.dbManager.db.transaction(function(tx) {
                    logWriter.writeLog("dbManager.js",0,"addVerifyTagTag(): Sequence " + that.dbSequence);
            		var addedOn = new Date();
            		tx.executeSql("INSERT INTO verifyTags(sequence, branch_id, workfile_id, workfile_seq, tag, tag_desc, ready_to_process, created_date) VALUES (?,?,?,?,?,?,?,?)",
            					  [that.dbSequence, branch, workfile_id, workfile_seq, tag, tag_desc, false, addedOn],
            					   function(tx, rs){                                                                              
                                       logWriter.writeLog("dbManager.js",0,"addVerifyTagTag(): Tag verification added as sequence " + that.dbSequence);
                                       dmsi.verifyTags.viewModel.getTagListDataSource();                                      
                                   },
            					   function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"addVerifyTagTag(): Insert worfileTags failed for " + that.dbSequence + "!!!");
                                        logWriter.writeLog("dbManager.js",99,"addVerifyTagTag(): " + rs.code + ", " + rs.message);
                                   });
            	});
            });
        },

       updateVerifyTagTag : function(branch, workfile_id, workfile_seq, tag, tag_desc) {
            
            logWriter.writeLog("dbManager.js",0,"updateVerifyTagTag()");
                       
            var that = this;
            
            that.setSequence("VerifyTagInProcess",workfile_id, workfile_seq).done ( function () {
                
            	dmsi.dbManager.db.transaction(function(tx) {
                    logWriter.writeLog("dbManager.js",0,"updateVerifyTagTag(): Sequence " + that.dbSequence);
            		var addedOn = new Date();
            		tx.executeSql("UPDATE verifyTags SET tag_desc=? WHERE branch_id=? AND workfile_id=? AND workfile_seq=? AND tag=?",
            					  [tag_desc,branch, workfile_id, workfile_seq, tag],
            					   function(tx, rs){                                                                        
                                       logWriter.writeLog("dbManager.js",0,"updateVerifyTagTag(): Tag verification update for tag " + tag);
                                       dmsi.verifyTags.viewModel.getTagListDataSource();                                      
                                   },
            					   function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"updateVerifyTagTag(): Update worfileTags failed for tag " + tag + "!!!");
                                        logWriter.writeLog("dbManager.js",99,"updateVerifyTagTag(): " + rs.code + ", " + rs.message);
                                   });
            	});
            });
        },

        assignTagLocation : function (newLocID, numTagsMoved) {
            var that = this;
            logWriter.writeLog("dbManager.js",0,"assignTagLocation()");
            var successMessage = "Moved ";
            
            if (numTagsMoved > 1) {
                successMessage = successMessage + numTagsMoved + " tags to drop location " + newLocID;
            }
            else {
                successMessage = successMessage + numTagsMoved + " tag to drop location " + newLocID;
            }
                     
            that.setSequence("MoveTag","",0).done ( function () {
              
                dmsi.dbManager.db.transaction(function(tx) {
                    tx.executeSql(
                         "UPDATE moveTags SET new_location=?, sequence=? WHERE branch_id=? AND new_location=?", 
                         [newLocID,that.dbSequence,dmsi.model.session.branchId,''], 
                         function(tx, rs){
                            logWriter.writeLog("dbManager.js",0,"assignTagLocation(): location " + newLocID + " updated as sequence " + that.dbSequence + " for " + rs.rowsAffected + " rows");
                            $("#moveTagsStaticNotification").data("kendoNotification").show({SuccessfulMoveMessage: successMessage},"SuccessfulMove");                                           
                                                                                       
                             dmsi.updateProcess.updateControl();
                         },                               
                         function(tx, rs){
                            logWriter.writeLog("dbManager.js",99,"assignTagLocation(): Update location failed for sequence " + that.dbSequence + "!!!");
                            logWriter.writeLog("dbManager.js",99,"assignTagLocation(): " + rs.code + ", " + rs.message);
                         })
                });                                          
            });
        },
        
        assignVerifyTagsReadyToProcess : function (workfileId, workfileSeq, workfileIdSeqDesc, numTagsVerified) {
            var that = this;
            logWriter.writeLog("dbManager.js",0,"assignVerifyTagsReadyToProcess()");
            var successMessage = "Verified ";
            
            if (numTagsVerified > 1) {
                successMessage = successMessage + numTagsVerified + " tags for workfile " + workfileIdSeqDesc;
            }
            else {
                successMessage = successMessage + numTagsVerified + " tag for workfile " + workfileIdSeqDesc;
            }
                     
            that.setSequence("VerifyTag",workfileId,workfileSeq).done ( function () {
              
                dmsi.dbManager.db.transaction(function(tx) {
                    tx.executeSql(
                         "UPDATE verifyTags SET ready_to_process=?, sequence=? WHERE branch_id=? AND workfile_id=? AND workfile_seq=? AND ready_to_process=?", 
                         [true,that.dbSequence,dmsi.model.session.branchId,workfileId,workfileSeq,false], 
                         function(tx, rs){
                            logWriter.writeLog("dbManager.js",0,"assignVerifyTagsReadyToProcess(): workfile " + workfileIdSeqDesc + " updated as sequence " + that.dbSequence);
                            $("#verifyTagsStaticNotification").data("kendoNotification").show({SuccessfulVerifyMessage: successMessage},"SuccessfulVerify");                                                                      
                   
                            dmsi.updateProcess.updateControl();
                         },                               
                         function(tx, rs){
                            logWriter.writeLog("dbManager.js",99,"assignVerifyTagsReadyToProcess(): Update Ready To Process failed for sequence " + that.dbSequence + "!!!");
                            logWriter.writeLog("dbManager.js",99,"assignVerifyTagsReadyToProcess(): " + rs.code + ", " + rs.message);
                         })
                });                                          
            });
        },
       
       addWorkorderCompletion : function(branch,WOID,completionQty,location) {
            
            logWriter.writeLog("dbManager.js",0,"addWorkorderCompletion()");                                   
            
            var that = this;
            
            //  This is writing a 'workorderCompletion' sequence just to retain the sequence value.  All updating will be done on the 'workorderCompletion' sequence
            that.setSequence("WorkorderCompletion","",0).done ( function () {
                //that.dbSequence = that.dbSequence + 1;
                
            	dmsi.dbManager.db.transaction(function(tx) {
                    logWriter.writeLog("dbManager.js",0,"addWorkorderCompletion(): Sequence " + that.dbSequence);
            		var addedOn = new Date();
            		tx.executeSql("INSERT INTO workorderCompletion(sequence, branch_id, wo_id, qty_completing, default_location) VALUES (?,?,?,?,?)",
            					  [that.dbSequence, branch, WOID, completionQty, location],
            					   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",0,"addWorkorderCompletion(): Work Order ID " + WOID + " updated as sequence " + that.dbSequence);
                                       $("#workorderCompletionStaticNotification").data("kendoNotification").show({SuccessfulWorkorderCompletionMessage: 'Work order submitted for completion.'},"SuccessfulWorkorderCompletion");                                                                      
                   
                                       dmsi.updateProcess.updateControl();
                                   },
            					   function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"addWorkorderCompletion(): Work Order Completion failed for " + that.dbSequence + "!!!");
                                        logWriter.writeLog("dbManager.js",99,"addWorkorderCompletion(): " + rs.code + ", " + rs.message);
                                   });
            	});
            });
        },
 
       addDepartmentScanning : function(branch,WOID,scheduleGroupId,completionQty,reasonCodes,note,numEmployees) {
            
            logWriter.writeLog("dbManager.js",0,"addDepartmentScanning()");                                   
            
            var that = this;
           
            //  This is writing a 'departmentScanning' sequence just to retain the sequence value.  All updating will be done on the 'departmentScanning' sequence
            that.setSequence("DepartmentScanning","",0).done ( function () {
                //that.dbSequence = that.dbSequence + 1;
                
            	dmsi.dbManager.db.transaction(function(tx) {
                    logWriter.writeLog("dbManager.js",0,"addDepartmentScanning(): Sequence " + that.dbSequence);
            		var addedOn = new Date();
            		tx.executeSql("INSERT INTO departmentScanning(sequence,\
                                    branch_id,\
                                    tran_id,\
                                    schedule_group_id,\
                                    schedule_group_qty_completed,\
                                    reason_codes,\
                                    note,\
                                    num_employees,\
                                    created_date) VALUES (?,?,?,?,?,?,?,?,?)",
            					  [that.dbSequence,branch,WOID,scheduleGroupId,completionQty,reasonCodes,note,numEmployees,addedOn],
            					   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",0,"addDepartmentScanning(): Work Order ID " + WOID + " updated as sequence " + that.dbSequence);
                                       
                                       if(completionQty > 0) { // 0 is for pause
                                           $("#departmentScanningStaticNotification").data("kendoNotification").show({SuccessfulDepartmentScanningMessage: 'Department submitted for completion.'},"SuccessfulWorkorderCompletion");                                                                      
                                           document.getElementById("departmentScanningWOID").focus();
                                       }
                   
                                       dmsi.updateProcess.updateControl();
                                   },
            					   function(tx, rs){                                
                                        logWriter.writeLog("dbManager.js",99,"addDepartmentScanning(): Work Order Completion failed for " + that.dbSequence + "!!!");
                                        logWriter.writeLog("dbManager.js",99,"addDepartmentScanning(): " + rs.code + ", " + rs.message);
                                   });
            	});
            });
        },
        
       getSequencesQuery : function () {
            
            var that = this;
            var dfd = $.Deferred();
            
            logWriter.writeLog("dbManager.js",0,"getSequencesQuery()");
           
            dmsi.dbManager.db.transaction(function(tx) {
                   tx.executeSql(
                        "SELECT * FROM sequences ORDER BY sequence ASC", 
                        [], 
                        selectSuccess,
                        selectFail)
              	});                                          
            
           
           
            function selectSuccess (tx, rs) {
                logWriter.writeLog("dbManager.js",0,"getSequences(): Query complete ");
                dfd.resolve(rs);
            }
            
            function selectFail (tx, rs) {
                logWriter.writeLog("dbManager.js",0,"getSequences(): Query failed " + rs.code + ", " + rs.message);
                dfd.reject();
            }
            
            return dfd.promise();
        },
        
        doTagMove : function (sequence) {
            var that = this;
            var deferred = $.Deferred();
            var calledSequence = sequence;

            logWriter.writeLog("dbManager.js",0,"doTagMove() Sequence " + calledSequence);

            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                "SELECT * FROM moveTags WHERE sequence=? AND new_location <> ''", 
                [calledSequence],
                function(tx, rs){
                    if(rs !== undefined && rs.rows.length > 0 ) {                                
                        var moveTagsResultTable= []; 
                        var i = 0;
                        for(i=0;i<rs.rows.length;i++){
                            logWriter.writeLog("dbManager.js",0,"doTagMove(): Moving  " + rs.rows.item(i).sequence + " " + rs.rows.item(i).tag);
                            moveTagsResultTable.push(rs.rows.item(i));                                        
                        }

                        var promiseTagMove = dmsi.model.moveInventory(moveTagsResultTable);

                        promiseTagMove.done(function () {
                             logWriter.writeLog("dbManager.js",0,"doTagMove():promise.done");
                            
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                    "DELETE FROM moveTags WHERE sequence=?", 
                                    [calledSequence], 
                                    function(tx, rs){   
                                        logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete tags completed for " + calledSequence);
                                        
                                        dmsi.dbManager.db.transaction(function(tx) {
                                   		tx.executeSql(
                                               "DELETE FROM sequences WHERE sequence=?", 
                                               [calledSequence], 
                                               function(tx, rs){   
                                                   logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence completed for " + calledSequence);
                                                   dmsi.dbManager.db.transaction(function(tx) {
                                   		            tx.executeSql(
                                                           "DELETE FROM sequences WHERE sequence<=? AND ( transaction_type=? OR transaction_type=? )",
                                                           [calledSequence,'MoveTag','Tag'], 
                                                           function(tx, rs){   
                                                               logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence completed for MoveTag where seq < " + calledSequence);
                                                               deferred.resolve();
                                                           },                               
                                                           function(tx, rs){
                                                               logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence failed for MoveTag: " + rs.code + ", " + rs.message);
                                                               deferred.resolve();
                                                           })
                                       	        });                                                                                                                                      
                                               },                               
                                               function(tx, rs){
                                                   logWriter.writeLog("dbManager.js",99,"doTagMove(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                                   deferred.resolve();
                                               })
                                       	});                                 
                                    },                               
                                    function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"doTagMove(): Delete tags failed: " + rs.code + ", " + rs.message);
                                        deferred.resolve();
                                    })
                            	});                                 
                        });
                        promiseTagMove.fail(function (errorMsg) {
                            logWriter.writeLog("dbManager.js",99,"doTagMove(): promise fail: " + errorMsg);
                            deferred.reject();
                        });
                    }
                    else {
                        var promise = dmsi.model.moveInventory(moveTagsResultTable);

                        promise.done(function () {
                            dmsi.dbManager.db.transaction(function(tx) {
                       		tx.executeSql(
                                   "DELETE FROM sequences WHERE sequence=?", 
                                   [calledSequence], 
                                   function(tx, rs){   
                                       logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence completed for " + calledSequence);
                                       deferred.resolve();
                                   },                               
                                   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",99,"doTagMove(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                       deferred.resolve();
                                   })
                           	});                                 
                        });
                        deferred.resolve();                    
                    }
                },
                function(tx, rs){
                    logWriter.writeLog("dbManager.js",99,"doTagMove(): Query failed " + rs.code + ", " + rs.message);
                    var promise = dmsi.model.moveInventory(moveTagsResultTable);

                    promise.done(function () {
                        dmsi.dbManager.db.transaction(function(tx) {
                   		tx.executeSql(
                               "DELETE FROM sequences WHERE sequence=?", 
                               [calledSequence], 
                               function(tx, rs){   
                                   logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence completed for " + calledSequence);
                                   deferred.resolve();
                               },                               
                               function(tx, rs){
                                   logWriter.writeLog("dbManager.js",99,"doTagMove(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                   deferred.resolve();
                               })
                       	});                                 
                    });
                    deferred.reject();
                })
            });                                          
            return deferred.promise();
        },
        
        doTagVerify : function (sequence) {
            var that = this;
            var deferred = $.Deferred();
            var calledSequence = sequence;            

            logWriter.writeLog("dbManager.js",0,"doTagVerify() Sequence " + calledSequence);

            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                "SELECT * FROM verifyTags WHERE sequence=?", 
                [calledSequence],
                function(tx, rs){
                    if(rs !== undefined && rs.rows.length > 0 ) {                                
                        var verifyTagsResultTable= []; 
                        var i = 0;
                        for(i=0;i<rs.rows.length;i++){
                            var currWorkfileID = rs.rows.item(i).workfile_id;
                            var currWorkfileSeq = rs.rows.item(i).workfile_seq;
                            logWriter.writeLog("dbManager.js",0,"doTagVerify(): Verifying  " + rs.rows.item(i).sequence + " " + rs.rows.item(i).ready_to_process + " " + rs.rows.item(i).tag);
                            verifyTagsResultTable.push(rs.rows.item(i));                                        
                        }

                        var promiseTagVerify = dmsi.model.verifyTags(verifyTagsResultTable);

                        promiseTagVerify.done(function () {
                             logWriter.writeLog("dbManager.js",0,"doTagVerify(): promise.done");
                            
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                    "DELETE FROM verifyTags WHERE sequence=?", 
                                    [calledSequence], 
                                    function(tx, rs){   
                                        logWriter.writeLog("dbManager.js",0,"doTagVerify(): Delete verifyTags completed for " + calledSequence);
                                        
                                        dmsi.dbManager.db.transaction(function(tx) {
                                   		tx.executeSql(
                                               "DELETE FROM sequences WHERE sequence=?", 
                                               [calledSequence], 
                                               function(tx, rs){   
                                                   logWriter.writeLog("dbManager.js",0,"doTagVerifyn(): Delete sequence completed for " + calledSequence);
                                                   dmsi.dbManager.db.transaction(function(tx) {
                                   		            tx.executeSql(
                                                           "DELETE FROM sequences WHERE sequence<=? AND transaction_type=? AND workfile_id=? AND workfile_seq=?", 
                                                           [calledSequence,'VerifyTagInProcess',currWorkfileID, currWorkfileSeq], 
                                                           function(tx, rs){   
                                                               logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence completed for VerifyTagInProcess seq < " + calledSequence);
                                                               deferred.resolve();
                                                           },                               
                                                           function(tx, rs){
                                                               logWriter.writeLog("dbManager.js",0,"doTagMove(): Delete sequence failed for verifyTagInProcess: " + rs.code + ", " + rs.message);
                                                               deferred.resolve();
                                                           })
                                       	        });                                                                                                                                           
                                               },                               
                                               function(tx, rs){
                                                   logWriter.writeLog("dbManager.js",99,"doTagVerify(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                                   deferred.resolve();
                                               })
                                       	});                                 
                                    },                               
                                    function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"doTagVerify(): Delete verifyTTags failed: " + rs.code + ", " + rs.message);
                                        deferred.resolve();
                                    })
                            	});                                 
                        });
                        promiseTagVerify.fail(function (errorMsg) {
                            logWriter.writeLog("dbManager.js",99,"doTagVerify(): promise fail: " + errorMsg);
                            deferred.reject();
                        });                                    
                    }
                    else {
                        var promise = dmsi.model.verifyTags(verifyTagsResultTable);

                        promise.done(function () {
                            dmsi.dbManager.db.transaction(function(tx) {
                       		tx.executeSql(
                                   "DELETE FROM sequences WHERE sequence=?", 
                                   [calledSequence], 
                                   function(tx, rs){   
                                       logWriter.writeLog("dbManager.js",0,"doTagVerify(): Delete sequence completed for " + calledSequence);
                                       deferred.resolve();
                                   },                               
                                   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",99,"doTagVerify(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                       deferred.resolve();
                                   })
                           	});                                 
                        });
                        deferred.resolve();                    
                    }
                },
                function(tx, rs){
                    logWriter.writeLog("dbManager.js",99,"doTagVerify(): Query failed " + rs.code + ", " + rs.message);
                    deferred.reject();
                })
            });                                          
            return deferred.promise();
        },         
        
        doWorkorderCompletion : function (sequence) {
            var that = this;
            var deferred = $.Deferred();
            var calledSequence = sequence;            

            logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion() Sequence " + calledSequence);

            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                "SELECT * FROM workorderCompletion WHERE sequence=?", 
                [calledSequence],
                function(tx, rs){
                    if(rs !== undefined && rs.rows.length > 0 ) {                                
                        var workorderCompletionResultTable= []; 
                        var i = 0;
                        for(i=0;i<rs.rows.length;i++){
                            logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion(): Completing " + rs.rows.item(i).sequence + " " + rs.rows.item(i).wo_id + " " + rs.rows.item(i).qty_completing);
                            workorderCompletionResultTable.push(rs.rows.item(i));                                        
                        }

                        var promise = dmsi.model.completeWorkorder(workorderCompletionResultTable);

                        promise.done(function () {
                             logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion(): promise.done");
                            
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                    "DELETE FROM workorderCompletion WHERE sequence=?", 
                                    [calledSequence], 
                                    function(tx, rs){   
                                        logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion(): Delete workorderCompletion completed for " + calledSequence);
                                        
                                        dmsi.dbManager.db.transaction(function(tx) {
                                   		tx.executeSql(
                                               "DELETE FROM sequences WHERE sequence=?", 
                                               [calledSequence], 
                                               function(tx, rs){   
                                                   logWriter.writeLog("dbManager.js",0,"doWorkorderCompletionn(): Delete sequence completed for " + calledSequence);
                                                   dmsi.dbManager.db.transaction(function(tx) {
                                   		            tx.executeSql(
                                                           "DELETE FROM sequences WHERE sequence<=? AND transaction_type=?", 
                                                           [calledSequence,'WorkorderCompletion'], 
                                                           function(tx, rs){   
                                                               logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion(): Delete sequence completed for WorkorderCompletion seq < " + calledSequence);
                                                               deferred.resolve();
                                                           },                               
                                                           function(tx, rs){
                                                               logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion(): Delete sequence failed for WorkorderCompletion: " + rs.code + ", " + rs.message);
                                                               deferred.resolve();
                                                           })
                                       	        });                                                                                                                                           
                                               },                               
                                               function(tx, rs){
                                                   logWriter.writeLog("dbManager.js",99,"doWorkorderCompletion(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                                   deferred.resolve();
                                               })
                                       	});                                 
                                    },                               
                                    function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"doWorkorderCompletion(): Delete WorkorderCompletion failed: " + rs.code + ", " + rs.message);
                                        deferred.resolve();
                                    })
                            	});                                 
                        });
                        promise.fail(function (errorMsg) {
                            logWriter.writeLog("dbManager.js",99,"doWorkorderCompletion(): promise fail: " + errorMsg);
                            deferred.reject();
                        });                                    
                    }
                    else {
                        var promise = dmsi.model.completeWorkorder(workorderCompletionResultTable);

                        promise.done(function () {
                            dmsi.dbManager.db.transaction(function(tx) {
                       		tx.executeSql(
                                   "DELETE FROM sequences WHERE sequence=?", 
                                   [calledSequence], 
                                   function(tx, rs){   
                                       logWriter.writeLog("dbManager.js",0,"doWorkorderCompletion(): Delete sequence completed for " + calledSequence);
                                       deferred.resolve();
                                   },                               
                                   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",99,"doWorkorderCompletion(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                       deferred.resolve();
                                   })
                           	});                                 
                        });
                        deferred.resolve();                    
                    }
                },
                function(tx, rs){
                    logWriter.writeLog("dbManager.js",99,"doWorkorderCompletion(): Query failed " + rs.code + ", " + rs.message);
                    deferred.reject();
                })
            });                                          
            return deferred.promise();
        },  
 
         doDepartmentScanning : function (sequence) {
            var that = this;
            var deferred = $.Deferred();
            var calledSequence = sequence;            

            logWriter.writeLog("dbManager.js",0,"doDepartmentScanning() Sequence " + calledSequence);

            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                "SELECT * FROM departmentScanning WHERE sequence=?", 
                [calledSequence],
                function(tx, rs){
                    var promise;
                    var departmentScanningResultTable= []; 
                    var i = 0;
                        
                    if(rs !== undefined && rs.rows.length > 0 ) {                                
                        for(i=0;i<rs.rows.length;i++){
                            logWriter.writeLog("dbManager.js",0,"doDepartmentScanning(): Completing " + rs.rows.item(i).sequence + " " + rs.rows.item(i).tran_id + " " + rs.rows.item(i).schedule_group_qty_completed);
                            departmentScanningResultTable.push(rs.rows.item(i));                                        
                        }

                        promise = dmsi.model.completeDeptQty(departmentScanningResultTable);

                        promise.done(function () {
                             logWriter.writeLog("dbManager.js",0,"doDepartmentScanning(): promise.done");
                            
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                    "DELETE FROM departmentScanning WHERE sequence=?", 
                                    [calledSequence], 
                                    function(tx, rs){   
                                        logWriter.writeLog("dbManager.js",0,"doDepartmentScanning(): Delete departmentScanning completed for " + calledSequence);
                                        
                                        dmsi.dbManager.db.transaction(function(tx) {
                                   		tx.executeSql(
                                               "DELETE FROM sequences WHERE sequence=?", 
                                               [calledSequence], 
                                               function(tx, rs){   
                                                   logWriter.writeLog("dbManager.js",0,"doDepartmentScanningn(): Delete sequence completed for " + calledSequence);
                                                   dmsi.dbManager.db.transaction(function(tx) {
                                   		            tx.executeSql(
                                                           "DELETE FROM sequences WHERE sequence<=? AND transaction_type=?", 
                                                           [calledSequence,'DepartmentScanning'], 
                                                           function(tx, rs){   
                                                               logWriter.writeLog("dbManager.js",0,"doDepartmentScanning(): Delete sequence completed for DepartmentScanning seq < " + calledSequence);
                                                               deferred.resolve();
                                                           },                               
                                                           function(tx, rs){
                                                               logWriter.writeLog("dbManager.js",0,"doDepartmentScanning(): Delete sequence failed for DepartmentScanning: " + rs.code + ", " + rs.message);
                                                               deferred.resolve();
                                                           })
                                       	        });                                                                                                                                           
                                               },                               
                                               function(tx, rs){
                                                   logWriter.writeLog("dbManager.js",99,"doDepartmentScanning(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                                   deferred.resolve();
                                               })
                                       	});                                 
                                    },                               
                                    function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"doDepartmentScanning(): Delete DepartmentScanning failed: " + rs.code + ", " + rs.message);
                                        deferred.resolve();
                                    })
                            	});                                 
                        });
                        promise.fail(function (errorMsg) {
                            logWriter.writeLog("dbManager.js",99,"doDepartmentScanning(): promise fail: " + errorMsg);
                            deferred.reject();
                        });                                    
                    }
                    else {
                        promise = dmsi.model.completeDeptQty(departmentScanningResultTable);

                        promise.done(function () {
                            dmsi.dbManager.db.transaction(function(tx) {
                       		tx.executeSql(
                                   "DELETE FROM sequences WHERE sequence=?", 
                                   [calledSequence], 
                                   function(tx, rs){   
                                       logWriter.writeLog("dbManager.js",0,"doDepartmentScanning(): Delete sequence completed for " + calledSequence);
                                       deferred.resolve();
                                   },                               
                                   function(tx, rs){
                                       logWriter.writeLog("dbManager.js",99,"doDepartmentScanning(): Delete sequence failed: " + rs.code + ", " + rs.message);
                                       deferred.resolve();
                                   })
                           	});                                 
                        });
                        deferred.resolve();                    
                    }
                },
                function(tx, rs){
                    logWriter.writeLog("dbManager.js",99,"doDepartmentScanning(): Query failed " + rs.code + ", " + rs.message);
                    deferred.reject();
                })
            });                                          
            return deferred.promise();
        },
        
        dumpTables: function () {
            logWriter.writeLog("dbManager.js",0,"dumpTables()");
            
            logWriter.writeLog("dbManager.js",0,"sqlite_master");
            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT * FROM sqlite_master WHERE type='table' ORDER BY name", 
                    [],
                    function(tx, rs){
                        var ix = 0;
                        for(ix = 0;ix < rs.rows.length;ix++) {
                             logWriter.writeLog("dbManager.js",0,
                                            "sqlite_master:  '" +
                                            rs.rows.item(ix).type + "'  '" +
                                            rs.rows.item(ix).name + "'  '" +
                                            rs.rows.item(ix).tbl_name + "'  '" +
                                            rs.rows.item(ix).rootpage + "'  '" +
                                            rs.rows.item(ix).sql);
                        }                        
                    },
                    function(tx, rs){
                        logWriter.writeLog("dbManager.js",0,"sequences query failed " + rs.code + rs.message);
                    })
            });                                          
            logWriter.writeLog("dbManager.js",0,"");            
            
            logWriter.writeLog("dbManager.js",0,"sequences table");
            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT * FROM sequences", 
                    [],
                    function(tx, rs){
                        var ix = 0;
                        for(ix = 0;ix < rs.rows.length;ix++) {
                             logWriter.writeLog("dbManager.js",0,
                                            "sequences:  '" +
                                            rs.rows.item(ix).sequence + "'  '" +
                                            rs.rows.item(ix).transaction_type + "'  '" +
                                            rs.rows.item(ix).workfile_id + "'  '" +
                                            rs.rows.item(ix).workfile_seq + "'  '" +
                                            rs.rows.item(ix).created_date);
                        }                        
                    },
                    function(tx, rs){
                        logWriter.writeLog("dbManager.js",0,"sequences query failed " + rs.code + rs.message);
                    })
            });                                          
            logWriter.writeLog("dbManager.js",0,"");            
            
            logWriter.writeLog("dbManager.js",0,"moveTags table");
            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT * FROM moveTags", 
                    [],
                    function(tx, rs){
                        var ix = 0;
                        for(ix = 0;ix < rs.rows.length;ix++) {
                            logWriter.writeLog("dbManager.js",0,
                                            "moveTags:  '" +
                                            rs.rows.item(ix).sequence + "'  '" +
                                            rs.rows.item(ix).branch_id + "'  '" +
                                            rs.rows.item(ix).tag + "'  '" +
                                            rs.rows.item(ix).new_location + "'  '" +
                                            rs.rows.item(ix).created_date);
                        }                        
                    },
                    function(tx, rs){
                        logWriter.writeLog("dbManager.js",0,"moveTags query failed " + rs.code + rs.message);
                    })
            });                                          
            logWriter.writeLog("dbManager.js",0,"");            
            
            logWriter.writeLog("dbManager.js",0,"verifyTags table");
            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT * FROM verifyTags", 
                    [],
                    function(tx, rs){
                        var ix = 0;
                        for(ix = 0;ix < rs.rows.length;ix++) {
                            logWriter.writeLog("dbManager.js",0,
                                            "Verify Tags Record" + " '" + 
                                            rs.rows.item(ix).sequence + "'  '" +
                                            rs.rows.item(ix).branch_id + "'  '" +
                                            rs.rows.item(ix).workfile_id + "'  '" +
                                            rs.rows.item(ix).workfile_seq + "'  '" +
                                            rs.rows.item(ix).tag + "'  '" +
                                            rs.rows.item(ix).ready_to_process + "' '" +
                                            rs.rows.item(ix).created_date);
                        }                        
                    },
                    function(tx, rs){
                        logWriter.writeLog("dbManager.js",0,"verifyTags query failed " + rs.code + rs.message);                        
                    })
            });
            logWriter.writeLog("dbManager.js",0,"");  
            
            logWriter.writeLog("dbManager.js",0,"woCompletion table");
            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT * FROM workorderCompletion", 
                    [],
                    function(tx, rs){
                        var ix = 0;
                        for(ix = 0;ix < rs.rows.length;ix++) {
                            logWriter.writeLog("dbManager.js",0,
                                            "Work Order Completion Record" + " '" + 
                                            rs.rows.item(ix).sequence + "'  '" +
                                            rs.rows.item(ix).branch_id + "'  '" +
                                            rs.rows.item(ix).wo_id + "'  '" +
                                            rs.rows.item(ix).qty_completing + "'  '" +
                                            rs.rows.item(ix).default_location + "'  '" +
                                            rs.rows.item(ix).created_date);
                        }                        
                    },
                    function(tx, rs){
                        logWriter.writeLog("dbManager.js",0,"workorderCompletion query failed " + rs.code + rs.message);                        
                    })
            });
            logWriter.writeLog("dbManager.js",0,""); 
            
            logWriter.writeLog("dbManager.js",0,"departmentScanning table");
            dmsi.dbManager.db.transaction(function(tx) {
                tx.executeSql(
                    "SELECT * FROM departmentScanning", 
                    [],
                    function(tx, rs){
                        var ix = 0;
                        for(ix = 0;ix < rs.rows.length;ix++) {
                            logWriter.writeLog("dbManager.js",0,
                                            "Department Scanning Record" + " '" + 
                                            rs.rows.item(ix).sequence + "'  '" +
                                            rs.rows.item(ix).branch_id + "'  '" +
                                            rs.rows.item(ix).tran_id + "'  '" +
                                            rs.rows.item(ix).schedule_group_id + "'  '" +
                                            rs.rows.item(ix).schedule_group_qty_completed + "'  '" +
                                            rs.rows.item(ix).reason_codes + "'  '" +
                                            rs.rows.item(ix).note + "'  '" +
                                            rs.rows.item(ix).num_employees + "'  '" +
                                            rs.rows.item(ix).created_date);
                        }                        
                    },
                    function(tx, rs){
                        logWriter.writeLog("dbManager.js",0,"departmentScanning query failed " + rs.code + rs.message);                        
                    })
            });
            logWriter.writeLog("dbManager.js",0,""); 
        },
        
        outstandingData : function () {
            logWriter.writeLog("dbManager.js",0,"outstandingData()");
            var dfd = $.Deferred();

            dmsi.model.session.outstandingData = false;
            
            dmsi.dbManager.db.transaction(function(tx) {
                   tx.executeSql(
                        "SELECT sequence FROM sequences WHERE transaction_type = 'MoveTag' OR \
                                                              transaction_type = 'VerifyTag' OR \
                                                              transaction_type = 'WorkorderCompletion' OR \
                                                              transaction_type = 'DepartmentScanning'", 
                        [], 
                        selectSuccess,
                        selectFail)
              	});                                          
            
            function selectSuccess (tx, rs) {
                if ( rs.rows !== undefined ) {
                    if ( rs.rows.length > 0 ) {
                        dmsi.model.session.outstandingData = true;
                    }
                    else {
                        dmsi.model.session.outstandingData = false;
                    }
                }
                else {
                    dmsi.model.session.outstandingData = false;
                    logWriter.writeLog("dbManager.js",0,"getLastSequence(): No result found");                        
                }                   
                dfd.resolve();
            }
            
            function selectFail (tx, rs) {
                logWriter.writeLog("dbManager.js",0,"getLastSequence(): Query failed " + rs.code + " " + rs.message);
                dmsi.model.session.outstandingData = false;
                dfd.reject();
            }            
            return dfd.promise();
        },        
    };
}( dmsi ));
