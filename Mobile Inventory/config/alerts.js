/*
globals dmsi*/

( function( dmsi ) {

    "use strict";

    dmsi.alerts = {
        alert: function (title, message, callback) {
            navigator.notification.alert (
                message,
                callback || function() {},
                title,
                "OK"
            );
        },
        showLoading: function() {
            dmsi.app.showLoading ();
        },
        hideLoading: function() {
            dmsi.app.hideLoading ();
        }
    };

} ( dmsi ) );