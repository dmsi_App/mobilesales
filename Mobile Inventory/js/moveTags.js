(function( dmsi ) {
    "use strict";
    
    dmsi.moveTags = {       
        
        tagsEntered: false,
        numTagsEntered: 0,
        
        viewModel: kendo.observable ( {
            
            onBackKeyPressed: function(e) {
                logWriter.writeLog("moveTags.js",0,"onBackKeyPressed()");
                document.getElementById("moveTagsTagID").blur();
                dmsi.app.replace (dmsi.config.views.mainMenu, "slide:right");
            },
            
            clearTagID: function (e) {
                var tagInput = document.getElementById("moveTagsTagID");
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                }
            },
                                                           
            addToListSelected: function(e) {                
                var that = this;              
                var tagID = document.getElementById("moveTagsTagID").value;
                if (tagID === "undefined" || tagID === null || tagID === "") {
                    document.getElementById("moveTagsTagID").focus();
                }
                else if (that.tagsEntered === true){
                    this.fetchLocation(tagID)
                }
                else {                    
                    this.fetchTag(tagID);                            
                }
                
                if (e) {
                    e.preventDefault();
                }
            },                                               
            
            onLogout : function () {
                logWriter.writeLog("moveTags.js",0,"onLogout()");
                document.getElementById("moveTagsTagID").blur();
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
            
            fetchLocation : function (locID) {
                logWriter.writeLog("moveTags.js",0,"fetchLocation()");
                
                var that = this;
                var locationFound = false;
                                                                             
                for (var loc = 0;loc < dmsi.model.session.validLocationList.length;loc++) {
                        
                    if ((dmsi.model.session.validLocationList[loc].branch_id === dmsi.model.session.branchId) &&
                        (dmsi.model.session.validLocationList[loc].location.toUpperCase() === locID.toUpperCase())){
                           
                        locationFound = true;

                        break;
                    }                              
               }     
                
               if (locationFound === false) {
                   logWriter.writeLog("moveTags.js",0,"fetchLocation(): InvalidLocation");
                         
                   dmsi.moveTags.viewModel.fetchTag(locID);                                 
               }
               else {
                   logWriter.writeLog("moveTags.js",0,"fetchLocation(): ValidLocation");
                                                                    
                   document.getElementById("moveTagsTagID").value = ""; 
                   
                   var numTags = that.numTagsEntered;
                   
                   that.clearValues();  
                   
                   dmsi.dbManager.assignTagLocation(locID, numTags);                                               
                                       
                   document.getElementById("moveTagsTagID").focus();
               } 
            },
            
            fetchTag : function (tagID) {
                logWriter.writeLog("moveTags.js",0,"fetchTag()");
                
                var that = this;
                var connection = navigator.onLine ? "online" : "offline";
                
                if (connection === "offline") {
                    dmsi.dbManager.addMoveTagTag(dmsi.model.session.branchId,tagID, "", "");   
                    document.getElementById("moveTagsTagID").value = "";
                    document.getElementById("moveTagsTagID").focus();                          
                }
                else {                        
                    dmsi.dbManager.addMoveTagTag(dmsi.model.session.branchId,tagID, "", "");   
                    document.getElementById("moveTagsTagID").value = "";
                    
                    var promise = dmsi.model.getmoveTagsTagDataSource(tagID);                    
                    
                    promise.done(function (detailDataSource) {
                        
                        logWriter.writeLog("moveTags.js",0,"fetchTag():promise.done");
                        dmsi.model.hideLoading();

                        detailDataSource.read();
                    
                        logWriter.writeLog("moveTags.js",0,"fetchTag(): detailDataSource.at(0): " + detailDataSource.at(0) );                    
                                                   
                        if ( detailDataSource.at(0).lErrorMessage === true ) {
                             logWriter.writeLog("moveTags.js",0,"fetchTag(): InvalidTag");                        
                            dmsi.dbManager.updateMoveTagTag(dmsi.model.session.branchId, detailDataSource.at(0).tag, "Invalid tag", "");
                        }
                        else {                        
                            logWriter.writeLog("moveTags.js",0,"fetchTag(): ValidTag");
                            dmsi.dbManager.updateMoveTagTag(dmsi.model.session.branchId, detailDataSource.at(0).tag, "", "");
                        }                        
                        document.getElementById("moveTagsTagID").focus();                     
                    });
                    promise.fail(function (errorMsg) {
                        logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                    });
                }
            },
             
             onInit: function() {
                 var that = this;
             
                 logWriter.writeLog("moveTags.js",0,"onInit()");

                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: that.onTabSelect                
                 });        
                 
               $("#moveTagsStaticNotification").kendoNotification({
                    templates: [{
                        type: "SuccessfulMove",
                        template: $("#moveTagsSuccessfulMoveTemplate").html()
                    }],                 
                    autoHideAfter: 10000,
                    hideOnClick: true,
                    button: true,
                    position: {
                        pinned: true,
                        top: 0,
                        left: 0
                    }
                    
                });      
                 
                 $("#moveTagsResultList").kendoListView({
                    dataSource: {
                        data: []
                    },
                    template: $("#moveTagsResultListTemplate").text(), 
                    style: "inset",                    
                    selectable: false,
                    pullToRefresh: true                 
                });   
                 
                function onMenuChange(e) {
                  
                  var list = $('#moveTagsResultList').data('kendoListView');
                  var view = list.dataSource.view();
                    
                  logWriter.writeLog("moveTags.js",0,"onMenuChange(): Selected " + view[list.select().index()].menu_name);                 
                  
              }
                 
                $('#moveTagsTagID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }
                });    
            },
            
            onShow: function() {
                
                var that = this;
                logWriter.writeLog("moveTags.js",0,"onShow()");
                document.getElementById("moveTagsbranchbarid").textContent = dmsi.model.session.branchId;
                that.clearValues();
                that.getTagListDataSource();
                
                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },
            
            getTagListDataSource: function() {
                           
                var that = this;
                
                dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                "SELECT * FROM moveTags WHERE branch_id=? AND new_location=? ORDER BY sequence DESC", 
                                [dmsi.model.session.branchId,''], 
                                function(tx, rs){   
                                    var moveTagsResultTable= []; 
                                    var i=0;
                                   
                                    if (rs.rows.length > 0) {                                                                              
                                        document.getElementById("moveTagsBtn").text = "Add Tag/Process Move"; 
                                        document.getElementById("moveTagsTagLocHeading").innerHTML = "Tag or Drop Location";
                                        that.tagsEntered = true;
                                        that.numTagsEntered = rs.rows.length;
                                    }
                                    else {                                                                           
                                     document.getElementById("moveTagsBtn").text = "Add Tag to List"; 
                                     document.getElementById("moveTagsTagLocHeading").innerHTML = "Tag";
                                     that.tagsEntered = false;
                                     that.numTagsEntered = 0;
                                    }
                                    
                                    for(i=0;i<rs.rows.length;i++){
                                        moveTagsResultTable.push(rs.rows.item(i));
                                        
                                    }
                                    
                                    var newDataSource = new kendo.data.DataSource({data: moveTagsResultTable}); 
                                    $("#moveTagsResultList").data("kendoListView").setDataSource(newDataSource); 
                                    $("#moveTagsResultList").data("kendoListView").dataSource.read(); 
                                    dmsi.app.scroller().reset(); 
                                },                               
                                function(tx, rs){
                                })
                            	});                                             
            },
            
            clearValues: function() {
                var that = this;
                logWriter.writeLog("moveTags.js",0,"clearValues()");
                dmsi.app.scroller().reset(); 
                document.getElementById("moveTagsTagID").value = "";             
                var emptyDataSource = new kendo.data.DataSource({data: []});
                $("#moveTagsResultList").data("kendoListView").setDataSource(emptyDataSource); 
                document.getElementById("moveTagsBtn").text = "Add Tag to List";
                document.getElementById("moveTagsTagLocHeading").innerHTML = "Tag";
                that.tagsEntered = false;
                that.numTagsEntered = 0;
            },
            
            deleteTagListEntry: function(tagToDelete, tagSequenceToDelete) {
                             var that = this;
                             logWriter.writeLog("moveTags.js",0,"DeleteTagListEntry()");
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                "DELETE FROM moveTags WHERE branch_id=? AND tag=? AND sequence=?", 
                                [dmsi.model.session.branchId,tagToDelete,tagSequenceToDelete], 
                                function(tx, rs){   
                                    that.getTagListDataSource();                                     
                                },                               
                                function(tx, rs){
                                })
                            	});                                 
            },  
            
            keyPressed: function (e){
                
                var that = this;               
                
                if(e.keyCode === 13){                                                                                    
                    that.addToListSelected(); 
                }   
            },
            
            keyUp: function (e){
                var locationFound = false;
                var that = this;
                var tagID = document.getElementById("moveTagsTagID").value;
                 
                if (that.tagsEntered === true){ 
                 for (var loc = 0;loc < dmsi.model.session.validLocationList.length;loc++) {
                     
                     if ((dmsi.model.session.validLocationList[loc].branch_id === dmsi.model.session.branchId) &&
                        (dmsi.model.session.validLocationList[loc].location.toUpperCase() === tagID.toUpperCase())){                                  
                                   
                         locationFound = true;

                         break;
                     }                              
                }
              }
                        
                if ((locationFound === false) && (that.tagsEntered === true)) {                       
                    document.getElementById("moveTagsBtn").text = "Add Tag/Process Move";                     
                }
                else if (locationFound === false) {
                    document.getElementById("moveTagsBtn").text = "Add Tag to List";  
                }
                else {
                    document.getElementById("moveTagsBtn").text = "Process Move";                          
                } 
              
            }
           
        })
    };

} ( dmsi ) ); //pass in global namespace
