/*config.messages.noWorkfiles...*/
(function( dmsi ) {
    "use strict";
    
    dmsi.workfiles = {       
        
        viewModel: kendo.observable ( {
            listView: null,
            listViewDataSource: null,
            
            onInit: function() {
                var that = this;
             
                logWriter.writeLog("workfiles.js",0,"onInit()");

            },
            
            onShow: function() {
                var that = this;
                dmsi.app.scroller().reset(); 
                logWriter.writeLog("wokfiles.js",0,"onShow()");
                
                document.getElementById("workfilebranchbar").textContent = dmsi.model.session.branchId;
                
                // clear out the old list
                $("#workfileList").html("");
                
                // If the device is not online, we can't find the workfiles, so show the connection error message and halt.
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                this.fetchWorkfiles();
                
                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },
            
            onBackKeyPressed: function(e) {
                logWriter.writeLog("workfiles.js",0,"onBackKeyPressed()");
                dmsi.app.replace (dmsi.config.views.mainMenu, "slide:right");
            },
            
            onLogout : function () {
                logWriter.writeLog("workfiles.js",0,"onLogout()");
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
            
            onWorkfileListChange: function(e) {
                  
                  var list = $('#workfileList').data('kendoListView');
                  var view = list.dataSource.view();
                    
                  logWriter.writeLog("workfiles.js",0,"onWorkfileListChange(): Selected " + view[list.select().index()].workfileIdSeqDesc );
                     
                  dmsi.model.session.workfile_id       = view[list.select().index()].workfile_id;
                  dmsi.model.session.workfile_seq      = view[list.select().index()].workfile_seq;
                  dmsi.model.session.workfileIdSeqDesc = view[list.select().index()].workfileIdSeqDesc;
                  dmsi.model.session.workfileSelectionCriteria = view[list.select().index()].selection_criteria;
                  dmsi.model.session.workfileLocationRange = view[list.select().index()].location_range;
                  dmsi.model.session.workfileCreatedDate = view[list.select().index()].created_date;
                
                  dmsi.app.navigate (dmsi.config.views.verifyTags, "slide");                  
                  
            },
            
            fetchWorkfiles: function() {
                logWriter.writeLog("workfiles.js",0,"fetchWorkfiles()");
                
                
                
                // set a variable that we can reference within the promise .done() method.
                var that = this;
                
                // Go get the data.
                var promise = dmsi.model.getWorkfilesDataSource();
                promise.done(function (dataSource) {
                    dataSource.read().then(function() {
                      that.listViewDataSource = dataSource;
                      var theData = dataSource.data();
                          // if there is no data, show the "no worfiles available" message
                          if (theData.length === 0) {
                              $("#workfileListEmpty").addClass('visible');
                              $("#workfileListEmpty").removeClass('hidden');
                              $("#workfileList").addClass('hidden');
                              $("#workfileList").removeClass('visible');
                          } else {
                              // show the workfiles list
                              $("#workfileListEmpty").addClass('hidden');
                              $("#workfileListEmpty").removeClass('visible');
                              $("#workfileList").addClass('visible');
                              $("#workfileList").removeClass('hidden');
                              
                              
                              // edit the data to concatenate the workfileid, sequence, and if present description.  This 
                              // apparently can't be done in the template because the templating language is too weak, 
                              // so we have to format it for display here in the javascript.                                                            
                              for (var x = 0; x < theData.length; x++) {
                                  theData[x].workfileIdSeqDesc = theData[x].workfile_id;
                                  
                                  // prepend a 0 to the workfile sequence if < 10;
                                  if (String(theData[x].workfile_seq).length === 1) {
                                      theData[x].workfileIdSeqDesc = theData[x].workfileIdSeqDesc + "0" + String(theData[x].workfile_seq)
                                  } else {
                                      theData[x].workfileIdSeqDesc = theData[x].workfileIdSeqDesc + String(theData[x].workfile_seq)
                                  }
                                  
                                  // If there's a description, add a " - " and the descritption, else skip it.
                                  if (typeof(theData[x].description) !== 'undefined' && String(theData[x].description).length > 0) {
                                       theData[x].workfileIdSeqDesc =  theData[x].workfileIdSeqDesc + ' - ' + theData[x].description;
                                  }
                              }
                              
                              var formattedDataSource = new kendo.data.DataSource({data: theData});
                              
                              that.listView = $("#workfileList").kendoListView({
                                dataSource: formattedDataSource,
                                template: kendo.template($("#workfileTemplate").html()),
                                selectable: true,
                                change: that.onWorkfileListChange
                              });
                          }
                        
                        
                    });
                    
                });
            }                           
            
            
         })
    
    
    }
    
    
} ( dmsi ) ); //pass in global namespace