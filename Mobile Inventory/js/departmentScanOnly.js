(function( dmsi ) {
    "use strict";
    
    dmsi.departmentScanOnly = {       
        
        viewModel: kendo.observable ( {
            
            onWindowResize: function() {
                var title = dmsi.model.session.selectedDept;
                
                $("#departmentScanOnlyTabTitle").html(title);
            },
            
            onBackKeyPressed: function() {
                logWriter.writeLog("departmentScanOnly.js",0,"onBackKeyPressed()");
                dmsi.app.replace (dmsi.config.views.departmentSelection, "slide:right");
            },
            
            clearWOID: function (e) {
                logWriter.writeLog("departmentScanOnly.js",0,"clearWOID()");
                var clearWOIDFunc = this;
                
                clearWOIDFunc.clearValues(); 
                clearWOIDFunc.hideViewWOInfoFields(true);

                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },
            
            WOIDEntered: function(e) 
            {   
                var WOID = 0;
                var WOIDEnteredFunc = this;
                
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                
                if (document.getElementById("departmentScanOnlyWOID").value.trim() === "" || 
                    document.getElementById("departmentScanOnlyWOID").value === null || 
                    document.getElementById("departmentScanOnlyWOID").value === "undefined")
                    WOID = 0;
                else
                    WOID = parseInt(document.getElementById("departmentScanOnlyWOID").value);
                
                if (WOID === 0 || WOID === null || WOID === "undefined" || isNaN(WOID))
                {
                    WOIDEnteredFunc.clearValues();
                    WOIDEnteredFunc.hideViewWOInfoFields(true);
                    
                    if (WOID !== 0){
                        $("#departmentScanOnlyStaticNotification").data("kendoNotification").show({ErrorDepartmentScanOnlyMessage: "You must specify a valid work order ID."},"ErrorWorkorderCompletion");  
                    }
                    
                    return;
                }
                
                logWriter.writeLog("departmentScanOnly.js",0,"WOIDEntered()");
                
                var promise = dmsi.model.getScheduledDepartmentDataSource(dmsi.model.session.selectedDept, WOID);

                promise.done(function (detailDataSource) 
                {
                    logWriter.writeLog("departmentScanOnly.js",0,"WOIDEntered():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("departmentScanOnly.js",0,"WOIDEntered(): detailDataSource.at(0)[0]: " + detailDataSource.at(0)[0] );
                    
                    if ( detailDataSource.at(0)[0].lErrorMessage === true ) 
                    {
                        WOIDEnteredFunc.clearValues();
                        WOIDEnteredFunc.hideViewWOInfoFields(true);
                        
                        $("#departmentScanOnlyStaticNotification").data("kendoNotification").show({ErrorDepartmentScanOnlyMessage: detailDataSource.at(0)[0].cMessage},"ErrorWorkorderCompletion");  
                    }
                    else 
                    {      
                        //Use jQuery for list view as that was the only way it worked on Android 4.4.4
                        $("#departmentScanOnlyWOIDInfo").text(WOID);
                        $("#departmentScanOnlyNextDept").text(detailDataSource.at(0)[0].next_department);
                        $("#departmentScanOnlySOID").text(detailDataSource.at(0)[0].so_id);
                        $("#departmentScanOnlyWOPhrase").text(detailDataSource.at(0)[0].wo_phrase.replace(/\r\n/g,"<br />"));
                        
                        WOIDEnteredFunc.hideViewWOInfoFields(false);  
                        
                        var WOIDControl = document.getElementById("departmentScanOnlyWOID");
                        WOIDControl.value = "";
                        WOIDControl.text  = "";
                        WOIDControl.focus();
                        $("div.km-scroll-container").css('transform','');
                        document.getElementById("departmentScanOnlyWOID").focus();
                    }
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });
                
            },   
            
            onLogout : function () {
                logWriter.writeLog("departmentScanOnly.js",0,"onLogout()");
                
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
                         
            onInit: function() {
                 var onInitFunc = this;
                
                 logWriter.writeLog("departmentScanOnly.js",0,"onInit()");           
                
                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: onInitFunc.onTabSelect                
                 });       
                 
                 $("#departmentScanOnlyStaticNotification").kendoNotification({
                     templates: [{
                         type: "SuccessfulWorkorderCompletion",
                         template: $("#departmentScanOnlySuccessfulTemplate").html()
                     },
                     {
                         type: "ErrorWorkorderCompletion",
                         template: $("#departmentScanOnlyErrorTemplate").html()
                     }],                 
                     autoHideAfter: 4000,                    
                     hideOnClick: true,
                     button: true,
                     position: {
                         pinned: true,
                         top: 0,
                         left: 0
                     }
                     
                 });
                 
                $('#departmentScanOnlyWOID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }
                });
            
                // Register the event listener
                window.addEventListener("resize", this.onWindowResize, true);     
            },
            
            onShow: function() {
                
                var onShowFunc = this;
                logWriter.writeLog("departmentScanOnly.js",0,"onShow()");
                onShowFunc.clearValues();
                onShowFunc.hideViewWOInfoFields(true);
                onShowFunc.onWindowResize();
                document.getElementById("departmentScanOnlybranchbarid").textContent = dmsi.model.session.branchId;
                $("div.km-scroll-container").css('transform','');
                document.getElementById("departmentScanOnlyWOID").focus();
            }, 
            
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            }, 

            
            clearValues: function() {
                logWriter.writeLog("departmentScanOnly.js",0,"clearValues()");
                
                //Use jQuery for list view as that was the only way it worked on Android 4.4.4
                $("#departmentScanOnlyNextDept").text("");
                $("#departmentScanOnlySOID").text("");
                $("#departmentScanOnlyWOIDInfo").text("");
                $("#departmentScanOnlyWOPhrase").text("");
                
                var WOIDControl = document.getElementById("departmentScanOnlyWOID");

                WOIDControl.value = "";
                WOIDControl.text  = "";
                WOIDControl.focus();
                this.currWOID = 0;
            },
            
           hideViewWOInfoFields: function (setHidden) {
                logWriter.writeLog("departmentScanOnly.js",0,"hideViewWOInfoFields()");     
               
               if (setHidden) {
                document.getElementById("departmentScanOnlyInfo").style.display = "none";
               }     
               else {
                document.getElementById("departmentScanOnlyInfo").style.display = "";
               }
            },          
            
            WOIDKeyPressed: function (e){
                
                var WOIDKeyPressedFunc = this;               
                 
                if(e.keyCode === 13){                                                                                    
                    WOIDKeyPressedFunc.WOIDEntered(); 
                    var input = document.getElementById("departmentScanOnlyWOID");
                    if (input.value.length > 0) {
                        setTimeout( function () { input.setSelectionRange(0,999); }, 0);            
                    }                    
                }   
            },
        })
    };

} ( dmsi ) ); //pass in global namespace
