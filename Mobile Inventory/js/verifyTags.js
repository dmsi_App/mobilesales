(function( dmsi ) {
    "use strict";
    
    dmsi.verifyTags = {       
        
        tagsEntered: false,
        numTagsEntered: 0,
        origHeight: 0,
        origWidth: 0,         
        
        viewModel: kendo.observable ( {                                  
            
            onWindowResize: function() {
                var that      = this;                                                                                               
                var hwDiff    = (dmsi.verifyTags.origHeight - document.documentElement.clientWidth);
                var whDiff    = (dmsi.verifyTags.origWidth - document.documentElement.clientHeight);
                var abshwDiff = Math.abs(hwDiff);
                var abswhDiff = Math.abs(whDiff);               
                
                /* handle orienation changes - should not hide the search button */
                if (dmsi.verifyTags.origHeight !== document.documentElement.clientHeight &&
                    abshwDiff < 100 && 
                    abswhDiff < 100) {
                    dmsi.verifyTags.origHeight = document.documentElement.clientHeight;
                    dmsi.verifyTags.origWidth = document.documentElement.clientWidth;
                        
                }
                
                if (document.documentElement.clientHeight < dmsi.verifyTags.origHeight){                
                    $('#verifyTagsUpdateBtn').hide();
                    $('#verifyTagsUpdateBtnContainer').hide();
                    $('#verifyTagsFooter').hide();                    
                }
                else{
                    $('#verifyTagsUpdateBtn').show();
                    $('#verifyTagsUpdateBtnContainer').show();
                    $('#verifyTagsFooter').show();         
                }                
                
            },
            
            onBackKeyPressed: function() {
                logWriter.writeLog("verifyTags.js",0,"onBackKeyPressed()");
                document.getElementById("verifyTagsTagID").blur();
                dmsi.app.replace (dmsi.config.views.workfiles, "slide:right");
            },             
            
            clearTagID: function (e) {
                var tagInput = document.getElementById("verifyTagsTagID");
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                }
            },
                                                           
            addToListSelected: function(e) {                
                var that = this;              
                var tagID = document.getElementById("verifyTagsTagID").value;
                if (tagID === "undefined" || tagID === null || tagID === "") {
                    document.getElementById("verifyTagsTagID").focus();
                }                
                else {                    
                    this.fetchTag(tagID);                            
                }
                
                if (e) {
                    e.preventDefault();
                }
            },                        
                       
            
            onLogout : function () {
                logWriter.writeLog("verifyTags.js",0,"onLogout()");
                document.getElementById("verifyTagsTagID").blur();
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
            
            setReadyToProcess : function () {
                logWriter.writeLog("verifyTags.js",0,"setReadyToProcess()");
                
                var that = this;                
                                                                                            
                if (that.tagsEntered === false) {
                   logWriter.writeLog("setReadyToProcess.js",0,"setReadyToProcess(): No tags entered - nothing to update");                                                                   
               }
               else {
                   logWriter.writeLog("verifyTags.js",0,"setReadyToProcess(): Tags found to update");
                                                                    
                   document.getElementById("verifyTagsTagID").value = ""; 
                   
                   var numTags = that.numTagsEntered;
                   
                   that.clearValues();          
                   
                   dmsi.dbManager.assignVerifyTagsReadyToProcess(dmsi.model.session.workfile_id, dmsi.model.session.workfile_seq,dmsi.model.session.workfileIdSeqDesc, numTags);                                               
                                       
                   document.getElementById("verifyTagsTagID").focus();
               } 
            },
            
            fetchTag : function (tagID) {
                logWriter.writeLog("verifyTags.js",0,"fetchTag()");
                
                var that = this;
                var connection = navigator.onLine ? "online" : "offline";
                
                if (connection === "offline") {
                    dmsi.dbManager.addVerifyTagTag(dmsi.model.session.branchId,dmsi.model.session.workfile_id,dmsi.model.session.workfile_seq,tagID, "");   
                    document.getElementById("verifyTagsTagID").value = "";
                    document.getElementById("verifyTagsTagID").focus();                          
                }
                else {                        
                    document.getElementById("verifyTagsTagID").value = "";
                    dmsi.dbManager.addVerifyTagTag(dmsi.model.session.branchId,dmsi.model.session.workfile_id,dmsi.model.session.workfile_seq, tagID, "");
                    
                    var promise = dmsi.model.getmoveTagsTagDataSource(tagID);

                    promise.done(function (detailDataSource) {                        
                        logWriter.writeLog("verifyTags.js",0,"fetchTag():promise.done");
                        
                        dmsi.model.hideLoading();

                        detailDataSource.read();
                    
                        logWriter.writeLog("verifyTags.js",0,"fetchTag(): detailDataSource.at(0): " + detailDataSource.at(0) );                    
                                                   
                        if ( detailDataSource.at(0).lErrorMessage === true ) {
                             logWriter.writeLog("verifyTags.js",0,"fetchTag(): InvalidTag");                        
                            dmsi.dbManager.updateVerifyTagTag(dmsi.model.session.branchId,dmsi.model.session.workfile_id,dmsi.model.session.workfile_seq, detailDataSource.at(0).tag, "Invalid tag");
                        }
                        else {                        
                            logWriter.writeLog("verifyTags.js",0,"fetchTag(): ValidTag");
                            dmsi.dbManager.updateVerifyTagTag(dmsi.model.session.branchId,dmsi.model.session.workfile_id,dmsi.model.session.workfile_seq, detailDataSource.at(0).tag, "");
                                
                        }                        
                        document.getElementById("verifyTagsTagID").focus();                     
                    });
                    promise.fail(function (errorMsg) {
                        logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                    });
                }
            },
                      
            
             onInit: function() {
                 var that = this;
             
                 logWriter.writeLog("verifyTags.js",0,"onInit()");

                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: that.onTabSelect                
                 });        
                 
               $("#verifyTagsStaticNotification").kendoNotification({
                    templates: [{
                        type: "SuccessfulVerify",
                        template: $("#verifyTagsSuccessfulVerifyTemplate").html()
                    }],                 
                    autoHideAfter: 10000,
                    hideOnClick: true,
                    button: true,
                    position: {
                        pinned: true,
                        top: 0,
                        left: 0
                    }
                    
                });      
                 
                 $("#verifyTagsResultList").kendoListView({
                    dataSource: {
                        data: []
                    },
                    template: $("#verifyTagsResultListTemplate").text(), 
                    style: "inset",                    
                    selectable: false,
                    pullToRefresh: true                 
                });   
                                 
                 
                $('#verifyTagsTagID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }
                });
            
                // Register the event listener
                window.addEventListener("resize", this.onWindowResize, true);                  
            },
            
            onShow: function() {
                
                var that = this;
                logWriter.writeLog("verifyTags.js",0,"onShow()");
                dmsi.verifyTags.origHeight = document.documentElement.clientHeight;
                dmsi.verifyTags.origWidth = document.documentElement.clientWidth;               
                document.getElementById("verifyTagsbranchbarid").textContent = dmsi.model.session.branchId;         
                document.getElementById("verifyTagsWorkfileIdSeqDesc").textContent = dmsi.model.session.workfileIdSeqDesc;
                document.getElementById("verifyTagsSelectionCriteria").textContent = dmsi.model.session.workfileSelectionCriteria;
                document.getElementById("verifyTagsLocationRange").textContent = dmsi.model.session.workfileLocationRange;
                
                that.onWindowResize();
                that.clearValues();
                that.getTagListDataSource();         
                
                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },
            
            getTagListDataSource: function() {
                           
                var that = this;
                
                dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                "SELECT * FROM verifyTags WHERE branch_id=? AND workfile_id=? AND workfile_seq=? AND ready_to_process=? ORDER BY sequence DESC", 
                                [dmsi.model.session.branchId,dmsi.model.session.workfile_id,dmsi.model.session.workfile_seq,false], 
                                function(tx, rs){   
                                    var verifyTagsResultTable= []; 
                                    var i=0;
                                 
                                    if (rs.rows.length > 0) {                                                                                                                     
                                        that.tagsEntered = true;
                                        that.numTagsEntered = rs.rows.length;
                                    }
                                    else {                                                                                                                
                                     that.tagsEntered = false;
                                     that.numTagsEntered = 0;
                                    }
                                    
                                    for(i=0;i<rs.rows.length;i++){
                                        verifyTagsResultTable.push(rs.rows.item(i));
                                        
                                    }
                                    
                                    var newDataSource = new kendo.data.DataSource({data: verifyTagsResultTable}); 
                                    $("#verifyTagsResultList").data("kendoListView").setDataSource(newDataSource); 
                                    $("#verifyTagsResultList").data("kendoListView").dataSource.read();     
                                    dmsi.app.scroller().reset();
                                },                               
                                function(tx, rs){
                                })
                            	});                                             
            },
            
            clearValues: function() {
                var that = this;
                logWriter.writeLog("verifyTags.js",0,"clearValues()");
                dmsi.app.scroller().reset(); 
                document.getElementById("verifyTagsTagID").value = "";             
                var emptyDataSource = new kendo.data.DataSource({data: []});
                $("#verifyTagsResultList").data("kendoListView").setDataSource(emptyDataSource); 
                document.getElementById("verifyTagsBtn").text = "Add Tag to List";
                document.getElementById("verifyTagsTagLocHeading").text = "Tag";                
                that.tagsEntered = false;
                that.numTagsEntered = 0;                
            },
            
            deleteTagListEntry: function(tagToDelete, tagSequenceToDelete) {
                             var that = this;
                             logWriter.writeLog("verifyTags.js",0,"DeleteTagListEntry()");                             
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                "DELETE FROM verifyTags WHERE branch_id=? AND workfile_id=? AND workfile_seq=? AND tag=? AND ready_to_process=? AND sequence=?", 
                                [dmsi.model.session.branchId,dmsi.model.session.workfile_id,dmsi.model.session.workfile_seq,tagToDelete,false,tagSequenceToDelete], 
                                function(tx, rs){   
                                    that.getTagListDataSource();                                     
                                },                               
                                function(tx, rs){
                                })
                            	});                                 
            },  
            
            keyPressed: function (e){
                
                var that = this;               
                
                if(e.keyCode === 13){                                                                                    
                    that.addToListSelected(); 
                }   
            },
            
            keyUp: function (e){
                var that = this;
                var tagID = document.getElementById("verifyTagsTagID").value;                                                                        
              
            }
           
        })
    };

} ( dmsi ) ); //pass in global namespace
