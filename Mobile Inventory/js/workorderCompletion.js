(function( dmsi ) {
    "use strict";
    
    dmsi.workorderCompletion = {       
        
        currWOID: 0,
        currQtyRemaining: 0,
        workorderCompleteInProcess: false,
        
        viewModel: kendo.observable ( {
            
            onWindowResize: function() {
                var title = "";
                
                //Small phones and the gun
                if (document.documentElement.clientWidth < 350) {
                    title = "WO Completion";    
                }
                else {
                    title = "Work Order Completion";
                }
                
                $("#workorderCompletionTabTitle").html(title);
            },
            
            onBackKeyPressed: function() {
                logWriter.writeLog("workorderCompletion.js",0,"onBackKeyPressed()");
                document.getElementById("workorderCompletionWOID").blur();
                dmsi.app.replace (dmsi.config.views.mainMenu, "slide:right");
            },
            
            clearWOID: function (e) {
                logWriter.writeLog("workorderCompletion.js",0,"clearWOID()");
                var clearWOIDFunc = this;
                
                clearWOIDFunc.clearValues(); 
                clearWOIDFunc.hideViewWOInfoFields(true);

                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },
            
            clearLocation: function (e) {
                var locationInput = document.getElementById("workorderCompletionLocation");
                locationInput.value = "";
                locationInput.text  = "";
                locationInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            }, 
            
            clearCompletionQty: function (e) {
                var completionQtyInput = document.getElementById("workorderCompletionQty");
                completionQtyInput.value = "";
                completionQtyInput.text  = "";
                completionQtyInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            }, 
            
            WOIDEntered: function(e) 
            {        
                var WOIDEnteredFunc = this;
                
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                
                if (document.getElementById("workorderCompletionWOID").value.trim() === "" || document.getElementById("workorderCompletionWOID").value === null || document.getElementById("workorderCompletionWOID").value === "undefined"){
                    var WOID = 0;
                }
                else {
                    var WOID = parseInt(document.getElementById("workorderCompletionWOID").value);
                }
                
                if (WOID === 0 || WOID === null || WOID === "undefined" || isNaN(WOID))
                {
                    WOIDEnteredFunc.clearValues();
                    WOIDEnteredFunc.hideViewWOInfoFields(true);
                    
                    if (WOID !== 0){
                        $("#workorderCompletionStaticNotification").data("kendoNotification").show({ErrorWorkorderCompletionMessage: "You must specify a valid work order ID."},"ErrorWorkorderCompletion");  
                    }
                    
                    return;
                }
                
                if (WOIDEnteredFunc.currWOID === WOID) {
                    return;
                }
                
                WOIDEnteredFunc.currWOID = WOID;
                
                logWriter.writeLog("workorderCompletion.js",0,"WOIDEntered()");
                
                var promise = dmsi.model.getWOIDInfoDataSource(WOID);

                promise.done(function (detailDataSource) 
                {
                    logWriter.writeLog("workorderCompletion.js",0,"WOIDEntered():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("workorderCompletion.js",0,"WOIDEntered(): detailDataSource.at(0): " + detailDataSource.at(0) );
                    
                    if ( detailDataSource.at(0).lErrorMessage === true ) 
                    {
                        WOIDEnteredFunc.clearValues();
                        WOIDEnteredFunc.hideViewWOInfoFields(true);
                        
                        $("#workorderCompletionStaticNotification").data("kendoNotification").show({ErrorWorkorderCompletionMessage: detailDataSource.at(0).cMessage},"ErrorWorkorderCompletion");  
                    }
                    else 
                    {      
                        //Use jQuery for list view as that was the only way it worked on Android 4.4.4
                        $("#workorderCompletionSOID").text(detailDataSource.at(0).so_id);
                        $("#workorderCompletionCompletedQty").text(detailDataSource.at(0).qty_remaining + " of " + detailDataSource.at(0).qty_ordered);
                        $("#workorderCompletionWOPhrase").text(detailDataSource.at(0).wo_phrase.replace(/\r\n/g,"<br />"));

                        document.getElementById("workorderCompletionLocation").value = detailDataSource.at(0).default_location;
                        document.getElementById("workorderCompletionQty").value = 1;

                        var button = document.getElementById('workorderCompletionCompleteAllBtn');
                        button.innerText = button.textContent = 'Complete All (' + detailDataSource.at(0).qty_remaining + ')';
                        
                        WOIDEnteredFunc.hideViewWOInfoFields(false);
                        WOIDEnteredFunc.currQtyRemaining = detailDataSource.at(0).qty_remaining;   
                    }
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });
                
            },   
            
            processCompleteAll: function(e) {
                logWriter.writeLog("workorderCompletion.js",0,"processCompleteAll" );
                
                var WOButtonControl = document.getElementById("workorderCompletionCompleteAllBtn");

                WOButtonControl.focus();
                
                this.processWOComplete(this.currQtyRemaining);
            },
            
            processCompleteQty: function(e) {
                logWriter.writeLog("workorderCompletion.js",0,"processCompleteQty" );
                var WOButtonControl = document.getElementById("workorderCompletionCompleteQtyBtn");

                WOButtonControl.focus();
                
                this.processWOComplete(parseInt(document.getElementById("workorderCompletionQty").value));
            },
                
            processWOComplete: function(completionQty) {    
                logWriter.writeLog("workorderCompletion.js",0,"processWOComplete qty of " + completionQty );   

                var connection = navigator.onLine ? "online" : "offline";
                var processWOCompleteFunc = this;
                var location = document.getElementById("workorderCompletionLocation").value.trim();
                
                if (processWOCompleteFunc.workorderCompleteInProcess === true) {
                    return;
                }
                
                processWOCompleteFunc.workorderCompleteInProcess = true;
                
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    processWOCompleteFunc.workorderCompleteInProcess = false;
                    return;
                }
               
                if (completionQty === undefined ||
                    completionQty === null ||
                    completionQty === 0 || 
                    isNaN(completionQty)) {
                     $("#workorderCompletionStaticNotification").data("kendoNotification").show({ErrorWorkorderCompletionMessage: "You must specify a completion quantity."},"ErrorWorkorderCompletion");  
                     processWOCompleteFunc.workorderCompleteInProcess = false;
                     return;
                }
                
                if (location === undefined ||
                    location === null ||
                    location === "" ||
                    location === "N/A") {
                     $("#workorderCompletionStaticNotification").data("kendoNotification").show({ErrorWorkorderCompletionMessage: "You must specify a valid location."},"ErrorWorkorderCompletion");  
                     processWOCompleteFunc.workorderCompleteInProcess = false;
                     return;
                }
                else
                {
                   var foundLocation = false;
                   for (var loc = 0;loc < dmsi.model.session.validLocationList.length;loc++) {
                            
                        if ((dmsi.model.session.validLocationList[loc].branch_id === dmsi.model.session.branchId) &&
                            (dmsi.model.session.validLocationList[loc].location.toUpperCase() === location.toUpperCase())){
                               
                            foundLocation = true;

                            break;
                        }                              
                   }            
                     
                   if(!foundLocation) {
                      $("#workorderCompletionStaticNotification").data("kendoNotification").show({ErrorWorkorderCompletionMessage: "You must specify a valid location."},"ErrorWorkorderCompletion");  
                      processWOCompleteFunc.workorderCompleteInProcess = false;
                      return;                    
                   }                                   
                }
                
                dmsi.dbManager.addWorkorderCompletion(dmsi.model.session.branchId,parseInt(document.getElementById("workorderCompletionWOID").value),completionQty,location);
                
                processWOCompleteFunc.clearValues();  
                processWOCompleteFunc.hideViewWOInfoFields(true);
                
                $("div.km-scroll-container").css('transform','');
                
                processWOCompleteFunc.workorderCompleteInProcess = false;     
                
            },   
            
            
            onLogout : function () {
                logWriter.writeLog("workorderCompletion.js",0,"onLogout()");
                document.getElementById("workorderCompletionWOID").blur();
                
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
                         
            onInit: function() {
                 var onInitFunc = this;
                
                 logWriter.writeLog("workorderCompletion.js",0,"onInit()");           
                
                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: onInitFunc.onTabSelect                
                 });       
                 
                 $("#workorderCompletionStaticNotification").kendoNotification({
                     templates: [{
                         type: "SuccessfulWorkorderCompletion",
                         template: $("#workorderCompletionSuccessfulTemplate").html()
                     },
                     {
                         type: "ErrorWorkorderCompletion",
                         template: $("#workorderCompletionErrorTemplate").html()
                     }],                 
                     autoHideAfter: 4000,                    
                     hideOnClick: true,
                     button: true,
                     position: {
                         pinned: true,
                         top: 0,
                         left: 0
                     }
                     
                 });      
             
                 
                $('#workorderCompletionWOID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }
                });
            
                // Register the event listener
                window.addEventListener("resize", this.onWindowResize, true);     
            },
            
            onShow: function() {
                
                var onShowFunc = this;
                logWriter.writeLog("workorderCompletion.js",0,"onShow()");
                onShowFunc.clearValues();
                onShowFunc.hideViewWOInfoFields(true);
                onShowFunc.onWindowResize();
                document.getElementById("workorderCompletionbranchbarid").textContent = dmsi.model.session.branchId;
                $("div.km-scroll-container").css('transform','');
            }, 
            
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            }, 

            
            clearValues: function() {
                logWriter.writeLog("workorderCompletion.js",0,"clearValues()");
                
                document.getElementById("workorderCompletionLocation").value = "";
                
                //Use jQuery for list view as that was the only way it worked on Android 4.4.4
                $("#workorderCompletionSOID").text("");
                $("#workorderCompletionCompletedQty").text("");
                $("#workorderCompletionWOPhrase").text("");
                
                var WOIDControl = document.getElementById("workorderCompletionWOID");

                WOIDControl.value = "";
                WOIDControl.text  = "";
                WOIDControl.focus();
                this.currWOID = 0;
            },
            
           hideViewWOInfoFields: function (setHidden) {
                logWriter.writeLog("workorderCompletion.js",0,"hideViewWOInfoFields()");     
               
               if (setHidden) {
                document.getElementById("workorderCompletionFooter").style.visibility = "hidden";
                document.getElementById("workorderCompletionSOIDHd").parentElement.style.display = "none";
                document.getElementById("workorderCompletionCompletedQtyHd").parentElement.style.display = "none";
                document.getElementById("workorderCompletionWOPhraseHd").parentElement.style.display = "none";
               }     
               else {
                document.getElementById("workorderCompletionFooter").style.visibility = "visible";
                document.getElementById("workorderCompletionSOIDHd").parentElement.style.display = "";
                document.getElementById("workorderCompletionCompletedQtyHd").parentElement.style.display = "";
                document.getElementById("workorderCompletionWOPhraseHd").parentElement.style.display = "";
               }
            },          
            
            WOIDKeyPressed: function (e){
                
                var WOIDKeyPressedFunc = this;               
                 
                if(e.keyCode === 13){                                                                                    
                    WOIDKeyPressedFunc.WOIDEntered(); 
                }   
            },

            WOLocationEntered: function (e){
                logWriter.writeLog("workorderCompletion.js",0,"WOLocationEntered()");  
                this.endEditing(true);
            },
        })
    };

} ( dmsi ) ); //pass in global namespace
