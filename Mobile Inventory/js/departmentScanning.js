(function( dmsi ) {
    "use strict";
    
    dmsi.departmentScanning = {       
        
        currWOID: 0,
        currQtyRemaining: 0,
        currBOMQtyType: "",
        workorderCompleteInProcess: false,
        scheduleGroupID: "",
        panelBar: null,
        
        viewModel: kendo.observable ( {
            
            onWindowResize: function() {
                var title = dmsi.model.session.selectedDept;
                
                $("#departmentScanningTabTitle").html(title);
            },
            
            onBackKeyPressed: function() {
                logWriter.writeLog("departmentScanning.js",0,"onBackKeyPressed()");
                dmsi.app.replace (dmsi.config.views.departmentSelection, "slide:right");
            },
            
            clearWOID: function (e) {
                logWriter.writeLog("departmentScanning.js",0,"clearWOID()");
                var clearWOIDFunc = this;
                
                clearWOIDFunc.clearValues(); 
                clearWOIDFunc.hideViewWOInfoFields(true);

                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },
            
            clearCompletionQty: function (e) {
                var completionQtyInput = document.getElementById("departmentScanningQty");
                completionQtyInput.value = "";
                completionQtyInput.text  = "";
                completionQtyInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            }, 

            clearNumEmployees: function (e) {
                var numEmployeesInput = document.getElementById("departmentScanningNumEmployees");
                numEmployeesInput.value = "";
                numEmployeesInput.text  = "";
                numEmployeesInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            }, 
            
            WOIDEntered: function(e) {        
                var WOIDEnteredFunc = this;
                
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                
                var WOID;
                
                if (document.getElementById("departmentScanningWOID").value.trim() === "" || document.getElementById("departmentScanningWOID").value === null || document.getElementById("departmentScanningWOID").value === "undefined"){
                    WOID = 0;
                }
                else {
                    WOID = parseInt(document.getElementById("departmentScanningWOID").value);
                }
                
                if (WOID === 0 || WOID === null || WOID === "undefined" || isNaN(WOID))
                {
                    WOIDEnteredFunc.clearValues();
                    WOIDEnteredFunc.hideViewWOInfoFields(true);
                    
                    if (WOID !== 0){
                        $("#departmentScanningStaticNotification").data("kendoNotification").show({ErrorDepartmentScanningMessage: "You must specify a valid work order ID."},"ErrorWorkorderCompletion");  
                    }
                    
                    return;
                }
                
                if (WOIDEnteredFunc.currWOID === WOID) {
                    return;
                }
                
                WOIDEnteredFunc.currWOID = WOID;
                
                logWriter.writeLog("departmentScanning.js",0,"WOIDEntered()");
                
                var promise = dmsi.model.getScheduledDepartmentDataSource(dmsi.model.session.selectedDept, WOID);

                promise.done(function (detailDataSource) 
                {
                    logWriter.writeLog("departmentScanning.js",0,"WOIDEntered():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("departmentScanning.js",0,"WOIDEntered(): detailDataSource.at(0)[0]: " + detailDataSource.at(0)[0] );
                    
                    if ( detailDataSource.at(0)[0].lErrorMessage === true ) 
                    {
                        WOIDEnteredFunc.clearValues();
                        WOIDEnteredFunc.hideViewWOInfoFields(true);
                        
                        $("#departmentScanningStaticNotification").data("kendoNotification").show({ErrorDepartmentScanningMessage: detailDataSource.at(0)[0].cMessage},"ErrorWorkorderCompletion");  
                    }
                    else 
                    {   
                        WOIDEnteredFunc.scheduleGroupID = detailDataSource.at(0)[0].schedule_group_id;
                        
                        //Use jQuery for list view as that was the only way it worked on Android 4.4.4
                        var total = detailDataSource.at(0)[0].schedule_group_qty_remaining + detailDataSource.at(0)[0].schedule_group_qty_completed;
                        $("#departmentScanningCompletedQty").text(detailDataSource.at(0)[0].schedule_group_qty_remaining + " of " + total);
                        
                        $("#departmentScanningNextDept").text(detailDataSource.at(0)[0].next_department);
                        $("#departmentScanningSOID").text(detailDataSource.at(0)[0].so_id);
                        $("#departmentScanningWOPhrase").text(detailDataSource.at(0)[0].wo_phrase.replace(/\r\n/g,"<br />"));

                        $("#departmentScanningReasonList option:selected").prop("selected", false);
                        $("#departmentScanningNotes").val("");
                        
                        document.getElementById("departmentScanningQty").value = detailDataSource.at(0)[0].default_partial_qty;

                        var button = document.getElementById('departmentScanningCompleteAllBtn');
                        button.innerText = button.textContent = 'Complete All (' + detailDataSource.at(0)[0].schedule_group_qty_remaining + ')';  
                        
                        button = document.getElementById("departmentScanningStartStopTimeBtn");
                        button.innerText = "STOP TIME TRACKING";
                        
                        WOIDEnteredFunc.currBOMQtyType = detailDataSource.at(0)[0].bom_qty_type;                        
                        WOIDEnteredFunc.hideViewWOInfoFields(false);
                        WOIDEnteredFunc.currQtyRemaining = detailDataSource.at(0)[0].schedule_group_qty_remaining;
                        
                        document.getElementById("departmentScanningWOID").focus();
                    }
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });
                
            },   
            
            processCompleteAll: function(e) {
                logWriter.writeLog("departmentScanning.js",0,"processCompleteAll" );
                
                var WOButtonControl = document.getElementById("departmentScanningCompleteAllBtn");

                WOButtonControl.focus();
                
                this.processWOComplete(this.currQtyRemaining, false);
            },
            
            processCompleteQty: function(e) {
                logWriter.writeLog("departmentScanning.js",0,"processCompleteQty" );
                var WOButtonControl = document.getElementById("departmentScanningCompleteQtyBtn");

                WOButtonControl.focus();
                
                this.processWOComplete(document.getElementById("departmentScanningQty").value, false);
            },
                
            processWOComplete: function(completionQty,timeTracking) {    
                logWriter.writeLog("departmentScanning.js",0,"processWOComplete qty of " + completionQty );   

                var connection = navigator.onLine ? "online" : "offline";
                var processWOCompleteFunc = this;
                
                if (processWOCompleteFunc.workorderCompleteInProcess === true) {
                    return;
                }
                
                processWOCompleteFunc.workorderCompleteInProcess = true;
                
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    processWOCompleteFunc.workorderCompleteInProcess = false;
                    return;
                }
               
                if ((completionQty === undefined ||
                     completionQty === null ||
                     completionQty === 0 || 
                     isNaN(completionQty)) &&
                     !timeTracking) {
                     $("#departmentScanningStaticNotification").data("kendoNotification").show({ErrorDepartmentScanningMessage: "You must specify a completion quantity."},"ErrorWorkorderCompletion");  
                     processWOCompleteFunc.workorderCompleteInProcess = false;
                     return;
                }  
                
                var reasonsCtl = document.getElementById("departmentScanningReasonList");
                var i;
                var reasons = "";
                for(i=0; i<reasonsCtl.length; i++) {
                    if(reasonsCtl[i].selected){
                        if(reasons === "")
                            reasons = reasonsCtl[i].text;
                        else
                            reasons = reasons + "," + reasonsCtl[i].text;
                    }
                }
                
                dmsi.dbManager.addDepartmentScanning(dmsi.model.session.branchId,
                    parseInt(document.getElementById("departmentScanningWOID").value),
                    processWOCompleteFunc.scheduleGroupID,
                    completionQty,
                    reasons,
                    document.getElementById("departmentScanningNotes").value,
                    parseInt(document.getElementById("departmentScanningNumEmployees").value));
                
                if(!timeTracking) {
                    processWOCompleteFunc.hideViewWOInfoFields(true);
                    processWOCompleteFunc.clearValues();
                    $("div.km-scroll-container").css('transform','');
                }           
                
                processWOCompleteFunc.workorderCompleteInProcess = false;         
            },   
                     
            onLogout : function () {
                logWriter.writeLog("departmentScanning.js",0,"onLogout()");
                
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
                         
            onInit: function() {
                 var onInitFunc = this;
                
                 logWriter.writeLog("departmentScanning.js",0,"onInit()");           
                
                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: onInitFunc.onTabSelect                
                 });       
                 
                 $("#departmentScanningStaticNotification").kendoNotification({
                     templates: [{
                         type: "SuccessfulWorkorderCompletion",
                         template: $("#departmentScanningSuccessfulTemplate").html()
                     },
                     {
                         type: "ErrorWorkorderCompletion",
                         template: $("#departmentScanningErrorTemplate").html()
                     }],                 
                     autoHideAfter: 4000,                    
                     hideOnClick: true,
                     button: true,
                     position: {
                         pinned: true,
                         top: 0,
                         left: 0
                     }
                     
                 });     
                 
                $('#departmentScanningWOID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }
                });
                
                // Register the event listener
                window.addEventListener("resize", this.onWindowResize, true);     
            },
            
            onShow: function() {
                
                var onShowFunc = this;
                logWriter.writeLog("departmentScanning.js",0,"onShow()");
                
                if (window.localStorage["dept_scanning_woinfo_expanded"] !== undefined) {
                    var userSetting = dmsi.model.userDefaults.userId + "_true";
                    if (window.localStorage["dept_scanning_woinfo_expanded"].toUpperCase() === userSetting.toUpperCase())
                        document.getElementById("deptScanningExpandButton").innerText = "Less info...";
                    else
                        document.getElementById("deptScanningExpandButton").innerText = "More info..."; 
                }
                else
                    document.getElementById("deptScanningExpandButton").innerText = "Less info...";
                
                onShowFunc.clearValues();
                onShowFunc.hideViewWOInfoFields(true);
                onShowFunc.onWindowResize();
                document.getElementById("departmentScanningbranchbarid").textContent = dmsi.model.session.branchId;  
                    
                /* fill in department scanning reason codes */
                var list = $("#departmentScanningReasonList");
                list.empty();
                
                if (dmsi.model.session.reasons !== undefined && dmsi.model.session.reasons.length > 0){
                    var theData = dmsi.model.session.reasons;
                    
                    if (dmsi.isMobile.iPhone())
                        list.append("<option disabled></option>")
                    
                    for (var j = 0; j < theData.length; j++) { 
                        list.append("<option value=\"" + theData[j].reason + "\">" + theData[j].reason + "</option>");
                    }
                }
                
                $("#departmentScanningNumEmployees").val(dmsi.model.session.deptDefaultNumEmployees);
                
                // reset values changed by STOP TIME TRACKING
                document.getElementById("departmentScanningStartStopTimeBtn").innerText === "STOP TIME TRACKING";
                
                $("div.km-scroll-container").css('transform','');
                document.getElementById("departmentScanningWOID").focus();
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            }, 
          
            clearValues: function() {
                logWriter.writeLog("departmentScanning.js",0,"clearValues()");
                
                //Use jQuery for list view as that was the only way it worked on Android 4.4.4
                $("#departmentScanningNextDept").text("");
                $("#departmentScanningSOID").text("");
                $("#departmentScanningCompletedQty").text("");
                $("#departmentScanningWOPhrase").text("");
                
                $("#departmentScanningReasonList option:selected").prop("selected", false);
                $("#departmentScanningNotes").val("");
                
                var ctl = document.getElementById("departmentScanningStartStopTimeBtn");
                ctl.innerText = "STOP TIME TRACKING";
                
                ctl = document.getElementById("departmentScanningWOID");
                ctl.value = "";
                ctl.text  = "";
                ctl.focus();
                this.currWOID = 0;
            },
            
           hideViewWOInfoFields: function (setHidden) {
                var deptScanning = this;
               
                logWriter.writeLog("departmentScanning.js",0,"hideViewWOInfoFields()");     
               
                if(setHidden){
                    document.getElementById("departmentScanningStartStopTimeBtnDIV").style.display = "none";   
                    document.getElementById("departmentScanningNumEmployeesGroupBox").style.display = "none"; 
                    document.getElementById("departmentScanningListGroupBox").style.display = "none";
                       
                    document.getElementById("departmentScanningInfo").style.display = "none";  
                    document.getElementById("departmentScanningCompleteDIV").style.display = "none"; 
                }
                else {
                    document.getElementById("departmentScanningCompleteDIV").style.display = "";
                    
                    var TrackTimeVisible = (dmsi.model.session.deptTrackTime) ? '' :'none',
                        ReasonCodeVisible = (dmsi.model.session.deptAllowReasonCodes && dmsi.model.session.reasons !== undefined && dmsi.model.session.reasons.length > 0) ? '' :'none';
                       
                    document.getElementById("departmentScanningStartStopTimeBtnDIV").style.display = TrackTimeVisible;   
                    document.getElementById("departmentScanningNumEmployeesGroupBox").style.display = TrackTimeVisible; 
                    document.getElementById("departmentScanningListGroupBox").style.display = ReasonCodeVisible;
                       
                    document.getElementById("departmentScanningInfo").style.display = "";
                }               
                
                if(deptScanning.currBOMQtyType === "Total Qty") {
                    document.getElementById("departmentScanningQtyContainer").style.display = "none";
                    document.getElementById("departmentScanningCompleteQtyBtn").style.display = "none";
                }
                else {
                    document.getElementById("departmentScanningQtyContainer").style.display = "";
                    document.getElementById("departmentScanningCompleteQtyBtn").style.display = "";
                }
               
                if(document.getElementById("deptScanningExpandButton").innerText === "More info...") {
                    document.getElementById("deptScanningCollapseDIV").style.display = "none";
                } 
                else {
                    document.getElementById("deptScanningCollapseDIV").style.display = "";
                }
            },          
            
            WOIDKeyPressed: function (e){
                
                var deptScanning = this;               
                 
                if(e.keyCode === 13){ 
                    logWriter.writeLog("departmentScanning.js",0,"WOIDKeyPressed()");
              
                    deptScanning.WOIDEntered();
                    var input = document.getElementById("departmentScanningWOID");
                    if (input.value.length > 0) {
                        setTimeout( function () { input.setSelectionRange(0,999); }, 0);            
                    } 
                }   
            },
            
            stopStartTime: function(){
                logWriter.writeLog("departmentScanning.js",0,"stopStartTime()");
                
                var deptScanning = this;                
                var input = document.getElementById("departmentScanningStartStopTimeBtn");
                
                if(input.innerText === "STOP TIME TRACKING"){
                    deptScanning.processWOComplete(0,true);
                    
                    $("#departmentScanningReasonList option:selected").prop("selected", false);                
                    $("#departmentScanningNotes").val("");
                    
                    document.getElementById("departmentScanningCompleteDIV").style.display = "none";
                    
                    input.innerText = "START TIME TRACKING";
                }
                else {
                    deptScanning.currWOID = 0;
                    
                    deptScanning.WOIDEntered();
                    
                    document.getElementById("departmentScanningCompleteDIV").style.display = "";
                }
            },
            
            onExpandCollapse: function(e) {
                if(document.getElementById("deptScanningExpandButton").innerText === "More info...") {
                    document.getElementById("deptScanningCollapseDIV").style.display = "";
                    document.getElementById("deptScanningExpandButton").innerText = "Less info...";                    
                    window.localStorage["dept_scanning_woinfo_expanded"] = dmsi.model.userDefaults.userId + "_true";
                } else  {
                    document.getElementById("deptScanningCollapseDIV").style.display = "none";
                    document.getElementById("deptScanningExpandButton").innerText = "More info...";
                    window.localStorage["dept_scanning_woinfo_expanded"] = dmsi.model.userDefaults.userId + "_false";
                }
            },
        })
    };

} ( dmsi ) ); //pass in global namespace
