/*
(function (dmsi) {
    "use strict";
*/  
    var logWriter = {};
    logWriter.loggingLevel = 0;
    logWriter.showTrace = false;
    
    logWriter.writeLog = function (source, severity, message) { // severity: 0 - info, 99 - error.  Anything else debug
        var that = this;
        
        function callback1 () {
            
        }
        
        function callback2 () {
            
        }
        
        if ( severity >= that.loggingLevel ) {
            
            console.log(source + ": " + severity + " | " + message);
    
            if ( logWriter.showTrace ) {
                var stackList = "";
                
                try {
                    throw new Error();
                }
                catch(e) {
                    //  Split the stack trace into lines ...
                    var stackLines = e.stack.split('\n');
                    for(var line in stackLines) {
                        if(line > 0) {
                            // console.log("line " + line + ": " + stackLines[line]);
                            var stackFields = stackLines[line].split(' ');
                            for(var field in stackFields) {
                                if ( stackFields[field] === "at" )
                                    if ( parseInt(field) < stackFields.length ) {
                                        var func = parseInt(field) + 1;
                                        if(stackList !== "")
                                          stackList = stackList + ",";
                                        stackList = stackList + stackFields[func];
                                    }
                            }
                        }
                    }
                }                
                console.log(stackList);
            }
        }
    };
/*    
}( dmsi )); */       