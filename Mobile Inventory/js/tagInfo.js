(function( dmsi ) {
    "use strict";
    
    dmsi.tagInfo = {

        
        viewModel: kendo.observable ( {
            
            onInit: function() {
                 var that = this;
             
                 logWriter.writeLog("tagInfo.js",0,"onInit()");

                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: that.onTabSelect                
                 });        
                                
                $('#tagInfoTagID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }  
                });
                
                $("#tagInfoPrintStaticNotification").kendoNotification({
                     templates: [{
                         type: "SuccessfulPrint",
                         template: $("#tagInfoPrintSuccessfulTemplate").html()
                     }],                 
                     autoHideAfter: 10000,
                     hideOnClick: true,
                     button: true,
                     position: {
                         pinned: true,
                         top: 0,
                         left: 0
                     }
                });
                // Register the event listeners
                document.getElementById("tagInfoPrintDiv").style.visibility = "hidden"; 
            },
            
            onShow: function() {                
                var that = this;
                dmsi.app.scroller().reset(); 
                logWriter.writeLog("tagInfo.js",0,"onShow()");
                document.getElementById("taginfobranchbarid").textContent = dmsi.model.session.branchId;
                that.clearValues();
                that.loadLabelPrinters();
                document.getElementById("tagInfoPrintDiv").style.visibility = "hidden"; 
                document.getElementById("tagInfoTagID").focus();
                
                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },
            
            loadLabelPrinters: function () {
                var deferred = $.Deferred();
                
                dmsi.model.userDefaults.labelPrinterList.length = 0;
                        
                var promise = dmsi.model.getLabelPrintersDataSource();

                promise.done(function (labelPrintersDataSource) {
                    labelPrintersDataSource.read().then(function() {
                        var defaultPrinter = "";
                        var theData = labelPrintersDataSource.data();
                        
                        for (var x = 0; x < theData.length; x++) {
                            dmsi.model.userDefaults.labelPrinterList.push(theData[x].printer_id.toUpperCase());
                            
                            if ( theData[x].printer_id === window.localStorage["labelPrinter_rem"] ) {
                                defaultPrinter = theData[x].printer_id;
                            }
                        }
                        document.getElementById("tagInfoPrinter").value = defaultPrinter;
                    });
                    return deferred.resolve();
                });                
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                    return deferred.reject();
                });
            },
                        
            onBackKeyPressed: function(e) {
                logWriter.writeLog("tagInfo.js",0,"onBackKeyPressed()");
                document.getElementById("tagInfoTagID").blur();
                dmsi.app.replace (dmsi.config.views.mainMenu, "slide:right");
            },
            
            onPrinterChange: function () {
                var printer = document.getElementById("tagInfoPrinter");
                window.localStorage["labelPrinter_rem"] = printer.value;
            },
            
            clearTagID: function (e) {
                var tagInput = document.getElementById("tagInfoTagID");
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();
                }                	
            },
            
            clearPrinter: function (e) {
              var tagInput = document.getElementById("tagInfoPrinter");
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();
                }                	                
                
            },
            
            findTag: function() {
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                
                dmsi.model.session.tagInfoTag = "";
                document.getElementById("tagInfoPrintDiv").style.visibility = "hidden";
                
                var tagID = document.getElementById("tagInfoTagID").value;
                this.fetchTag(tagID);
                this.clearTagID();
            },
            
            printTag : function() {
                var that = this;
                logWriter.writeLog("tagInfo.js",0,"printTag()");
                
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                
                function printTagRecord ( branch, tag, printer, sequence ) {
                    this.branch_id  = branch;
                    this.tag        = tag;
                    this.printer_id = printer;
                    this.sequence   = sequence;
                }

                var labelPrinter = document.getElementById("tagInfoPrinter").value;
                labelPrinter = labelPrinter.toUpperCase();
                
                var found = false;
                for ( var ix = 0;ix < dmsi.model.userDefaults.labelPrinterList.length;ix++) {
                    if ( dmsi.model.userDefaults.labelPrinterList[ix] === labelPrinter )
                        found = true;
                }
                if(!found) {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.printerNotFound, "error");
                    return;                    
                }
                
                document.getElementById("tagInfoPrinter").blur();
                var printTagDataSet = [];
                var printTagRec = new printTagRecord(dmsi.model.session.branchId,dmsi.model.session.tagInfoTag,labelPrinter,1);
                
                printTagDataSet.push(printTagRec);
              
                window.localStorage["labelPrinter_rem"] = labelPrinter;
                
                var promise = dmsi.model.printTag(printTagDataSet);

                promise.done(function (detailDataSource) {
                    logWriter.writeLog("tagInfo.js",0,"printTag():promise.done");
                    $("#tagInfoPrintStaticNotification").data("kendoNotification").show({SuccessfulPrintMessage: "Tag printed"},"SuccessfulPrint");
                });
            },
            
            onLogout : function () {
                logWriter.writeLog("tagInfo.js",0,"onLogout()");
                document.getElementById("tagInfoTagID").blur();
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },

            fetchTag : function (tagID) {
                logWriter.writeLog("tagInfo.js",0,"fetchTag()");
                var that = this;
                
                var promise = dmsi.model.getTagInfoDataSource(tagID);

                promise.done(function (detailDataSource) {
                    logWriter.writeLog("tagInfo.js",0,"fetchTag():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("tagInfo.js",0,"fetchTag(): detailDataSource.at(0): " + detailDataSource.at(0) );
                    
                    if ( detailDataSource.at(0).lErrorMessage === true ) {

                        document.getElementById("tagInfoTagHd").text = "";
                        document.getElementById("tagInfoTag").text = "";
                        document.getElementById("tagInfoItemSizeDesc").text = detailDataSource.at(0).cMessage;
                        document.getElementById("tagInfoItemSizeDesc").value = detailDataSource.at(0).cMessage;
                        document.getElementById("tagInfoLocHd").text = "";
                        document.getElementById("tagInfoLoc").text = "";
                        document.getElementById("tagInfoLotHd").text = "";
                        document.getElementById("tagInfoLot").text = "";
                        document.getElementById("tagInfoContHd").text = "";
                        document.getElementById("tagInfoCont").text = "";
                        document.getElementById("tagInfoTallyHd").text = "";
                        document.getElementById("tagInfoTally").text = "";
                        document.getElementById("tagInfoQohHd").text = "";
                        document.getElementById("tagInfoQoh").text = "";
                        document.getElementById("tagInfoUom").text = "";
                        document.getElementById("tagInfoCommittedHd").text = "";
                        document.getElementById("tagInfoCommitted").text = "";
                        document.getElementById("tagInfoExtDescHd").text = "";
                        document.getElementById("tagInfoExtDesc").text = "";
                    }
                    else {
                        var item = detailDataSource.at(0).item;
                        var size = detailDataSource.at(0).size;
                        var desc = detailDataSource.at(0).description;
                        
                        document.getElementById("tagInfoTagHd").text = "Tag:";
                        document.getElementById("tagInfoTag").text = detailDataSource.at(0).tag;
                        
                        document.getElementById("tagInfoItemSizeDesc").text = item + " " + size + " " + desc;
                        if ( detailDataSource.at(0).location !== "" &&
                             detailDataSource.at(0).location !== undefined &&
                             detailDataSource.at(0).location !== "N/A" ) {
                            document.getElementById("tagInfoLocHd").parentElement.style.display = "";
                            document.getElementById("tagInfoLocHd").text = "Location:";
                            document.getElementById("tagInfoLoc").text = detailDataSource.at(0).location;
                        }
                        else {
                            document.getElementById("tagInfoLocHd").parentElement.style.display = "none";
                        }
                        
                        if ( detailDataSource.at(0).lot !== "" &&
                             detailDataSource.at(0).lot !== undefined &&
                             detailDataSource.at(0).lot !== "N/A" ) {
                            document.getElementById("tagInfoLot").parentElement.style.display = "";
                            document.getElementById("tagInfoLotHd").text = "Lot:";
                            document.getElementById("tagInfoLot").text = detailDataSource.at(0).lot;
                        }   
                        else {
                            document.getElementById("tagInfoLot").parentElement.style.display = "none";
                        }   
                    
                        if (( detailDataSource.at(0).content !== "" &&
                              detailDataSource.at(0).content !== undefined &&
                              detailDataSource.at(0).content !== "N/A") &&
                            ( detailDataSource.at(0).tally_string === "" ||
                              detailDataSource.at(0).tally_string === undefined )) {
                            document.getElementById("tagInfoCont").parentElement.style.display = "";
                            document.getElementById("tagInfoContHd").text = "Content:";
                            document.getElementById("tagInfoCont").text = detailDataSource.at(0).content;
                        }
                        else {
                            document.getElementById("tagInfoCont").parentElement.style.display = "none";
                        }
                        
                        if ( detailDataSource.at(0).tally_string !== "" &&
                             detailDataSource.at(0).tally_string !== undefined ) {
                            document.getElementById("tagInfoTally").parentElement.style.display = "";
                            document.getElementById("tagInfoTallyHd").text = "Tally:";
                            document.getElementById("tagInfoTally").text = detailDataSource.at(0).tally_string;
                        }
                        else {
                            document.getElementById("tagInfoTally").parentElement.style.display = "none";
                        }

                        if ( detailDataSource.at(0).quantity_on_hand.toString() !== "" &&
                             detailDataSource.at(0).quantity_on_hand !== undefined ) {
                            document.getElementById("tagInfoQoh").parentElement.style.display = "";
                            document.getElementById("tagInfoQohHd").text = "On hand:";
                            document.getElementById("tagInfoQoh").text = detailDataSource.at(0).quantity_on_hand.toFixed(4);
                            document.getElementById("tagInfoUom").text = detailDataSource.at(0).stocking_uom;
                        }
                        else {
                            document.getElementById("tagInfoQoh").parentElement.style.display = "none";
                        }
                        
                        if ( detailDataSource.at(0).commit_info !== "" &&
                             detailDataSource.at(0).commit_info !== undefined ) {
                            document.getElementById("tagInfoCommitted").parentElement.style.display = "";
                            document.getElementById("tagInfoCommittedHd").text = "Committed:";
                            document.getElementById("tagInfoCommitted").text = detailDataSource.at(0).commit_info;
                        }
                        else {
                            document.getElementById("tagInfoCommitted").parentElement.style.display = "none";
                        }
                        
                        if ( detailDataSource.at(0).extended_desc !== "" &&
                             detailDataSource.at(0).extended_desc !== undefined ) {
                            document.getElementById("tagInfoExtDescHd").parentElement.style.display = "";
                            document.getElementById("tagInfoExtDescHd").text = "Ext desc:";
                            document.getElementById("tagInfoExtDesc").text = 
                                detailDataSource.at(0).extended_desc.replace(/\r\n/g,"<br />");    
                        }
                        else {
                            document.getElementById("tagInfoExtDesc").parentElement.style.display = "none";
                        }
                        
                        dmsi.model.session.tagInfoTag = detailDataSource.at(0).tag;
                        document.getElementById("tagInfoPrintDiv").style.visibility = "visible";
                    }
                    /*
                    that.clearTagID();
                    */
                                        
                    logWriter.writeLog("tagInfo.js",0,"tagInfoItemSizeDesc.text: " + document.getElementById("tagInfoItemSizeDesc").text);
                    logWriter.writeLog("tagInfo.js",0,"tagInfoItemSizeDesc.value: " + document.getElementById("tagInfoItemSizeDesc").value);
                    logWriter.writeLog("tagInfo.js",0,"detailDataSource.at(0).cMessage: " + detailDataSource.at(0).cMessage);
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });
            },
        
             clearValues: function() {
                logWriter.writeLog("tagInfo.js",0,"clearValues()");
                
                document.getElementById("tagInfoTagID").value = "";
                document.getElementById("tagInfoTagHd").text = "";
                document.getElementById("tagInfoTag").text = "";
                document.getElementById("tagInfoItemSizeDesc").text = "";
                document.getElementById("tagInfoLocHd").text = "";
                document.getElementById("tagInfoLoc").text = "";
                document.getElementById("tagInfoLotHd").text = "";
                document.getElementById("tagInfoLot").text = "";
                document.getElementById("tagInfoContHd").text = "";
                document.getElementById("tagInfoCont").text = "";
                document.getElementById("tagInfoTallyHd").text = "";
                document.getElementById("tagInfoTally").text = "";
                document.getElementById("tagInfoQohHd").text = "";
                document.getElementById("tagInfoQoh").text = "";
                document.getElementById("tagInfoUom").text = "";
                document.getElementById("tagInfoCommittedHd").text = "";
                document.getElementById("tagInfoCommitted").text = "";
                document.getElementById("tagInfoExtDescHd").text = "";
                document.getElementById("tagInfoExtDesc").text = "";
            },
                    
            keyPressed: function (e){
                
                var that = this;
                
                if(e.keyCode === 13){
                    if ( e.target.id === "tagInfoTagID" )
                        that.findTag();
                    if ( e.target.id === "tagInfoPrinter" )
                        that.printTag();
                }
            },
           
        })
    };

} ( dmsi ) ); //pass in global namespace
