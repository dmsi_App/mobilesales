
( function ( dmsi ) {
    "use strict";

    dmsi.login = {

        viewModel: kendo.observable( {
            
            errorMessage: "",
            
            onInit: function () {
                
                logWriter.writeLog("login.js",0,"onInit()");
                $("#staticNotification").kendoNotification({
                    animation: {
                        open: {
                            effects: "slideIn:down"
                        },
                        close: {
                            effects: "slideIn:down",
                            reverse: true
                        }
                    },
                    position: {
                        top: 1,
                        left: 1
                    },
                    //stacking: "up",
                    autoHideAfter: 7000,
                });      
              
                $("#loginRemember").prop("checked",false);    
            },
            
            onShow: function () {
                logWriter.writeLog("login.js",0,"onShow()");
                if(window.localStorage["username_rem"] !== 'undefined' && window.localStorage["password_rem"] !== 'undefined') 
                {
                    if(window.localStorage["remember_me"] === "true" ){                 
                        $('#loginUsername').val(window.localStorage["username_rem"]);
                        $('#loginPassword').val(window.localStorage["password_rem"]);
                        $("#loginRemember").prop("checked",true);    
                    }else{
                        $("#loginUsername").val("");
                        $("#loginPassword").val("");
                        $("#loginRemember").prop("checked",false);    
                    }                
                }else{
                    $("#loginUsername").val("");
                    $("#loginPassword").val("");
                    $("#loginRemember").prop("checked",true);    
                }
            },
            
            keyDown: function (e){
                
                var that = this;
                
                if(e.keyCode === 13){
                
                    e.currentTarget.blur();
                    
                    that.onUserLogin(e);
                }
            },
            
            onUserLogin: function (e) {
                            
                var that = this,
                    connection = navigator.onLine ? "online" : "offline",
                    rememberMe = $("#loginRemember").is(":checked");
                     
                logWriter.writeLog("login.js",0, "onUserLogin()");
                logWriter.writeLog("login.js",0, connection);
                 
                dmsi.model.userDefaults.userIdWithTenant = $("#loginUsername").val();      
                dmsi.model.userDefaults.userPassword = $("#loginPassword").val();
               
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                }
                else if ((dmsi.model.userDefaults.userIdWithTenant === 'undefined') || (dmsi.model.userDefaults.userIdWithTenant === '') ||
                    (dmsi.model.userDefaults.userPassword     === 'undefined') || (dmsi.model.userDefaults.userPassword     === '')){
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.loginFailed, "error");
                }
                else{
                    dmsi.model.userDefaults.userId = that.getFirstPart(dmsi.model.userDefaults.userIdWithTenant);

                    if(rememberMe){               
                      window.localStorage["username_rem"] = dmsi.model.userDefaults.userIdWithTenant;
                      window.localStorage["password_rem"] = dmsi.model.userDefaults.userPassword;
                      window.localStorage["remember_me"] = rememberMe;
                    }else{
                      window.localStorage["username_rem"] = "";
                      window.localStorage["password_rem"] = ""; 
                      window.localStorage["remember_me"] = rememberMe;
                    }
                    
                    dmsi.model.userLogin();
                }
            },

            onSettings: function (e) {
                logWriter.writeLog("login.js",0,"onSettings()");
                dmsi.app.replace (dmsi.config.views.settings, "slide");
            },
            
            getFirstPart: function (str) {

                var index = str.indexOf("@");
                logWriter.writeLog("login.js",0,"getFirstPart()");
                if (index > 0){
                  dmsi.model.userDefaults.tenant = (str.split('@')[1]);
                  str = str.split('@')[0];
                } else {
                  index = str.indexOf(".");
                    
                    if (index > 0) {
                       dmsi.model.userDefaults.tenant = (str.split('.')[1]); 
                       str = str.split('.')[0];
                    }
                }

                if (dmsi.model.userDefaults.tenant === "crmdevel") {
                    dmsi.config.rollbaseSettings.baseUrl = "https://devel.dmsi.com/rest/api/";
                }

                return str;
            },
            
            getVersion: function () {
                return dmsi.config.settings.version;
            },
            
        } ),
    
    
    };

}( dmsi ) ); //pass in global namespace