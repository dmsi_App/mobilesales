
( function( dmsi ) {
    "use strict";

    dmsi.mainMenu = {

        viewModel: kendo.observable ( {

            onInit: function() {
               var that = this;
                
                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: that.onTabSelect                
                 });        
                
                that.onBranchInit();
                                
            },
                                    
            onShow: function() {
                
                var that = this;

               that.clearValues();

             }, 
                        
            clearValues: function() {
                
            },
                                        
        })
    };

} ( dmsi ) ); //pass in global namespace