(function( dmsi ) {
    "use strict";
    
    dmsi.splitTags = {       
        origHeight: 0,
        origWidth: 0, 
        tagsEntered: false,
        numTagsEntered: 0,
        defaultLabelPrinter: "",
        currFromTagTag: "",
        currFromTagItem: "",
        currFromTagSize: "",
        currFromTagDesc: "",
        currFromTagDimensionString: "",
        currFromTagThickness: 0,
        currFromTagWidth: 0,
        currFromTagLength: 0,
        currFromTagPieceCount: 0,
        currFromTagLotCode: true,
        currFromTagContentCode: true,
        currFromTagLocation: "",
        currFromTagLot: "",
        currFromTagContent: "",
        currFromTagQtyUOM: "",
        currFromTagQtyConv: "",
        currFromTagOnHandQty: 0,
        currFromTagSequence: 0,
        currFromTagsTotalQty: 0,
        currToTagItem: "", 
        currToTagNewTag: false,
        currToTagTag: "",
        currToTagWidth: 0,
        currToTagLength: 0,        
        currToTagLotCode: true,
        currToTagOrigLocation: "",
        currToTagOrigLot: "",
        currToTagOrigContent: "",
        currToTagContentCode: true,
        currToTagPieceCount: 0,
        currToTagQtyUOM: "",
        currToTagExistingQty: 0,   
        currFromTagList: [],
        splitInProcess: false,
        
        viewModel: kendo.observable ( {
                                  
            onWindowResize: function() {
                var that      = this;                                                                                               
                var hwDiff    = (dmsi.splitTags.origHeight - document.documentElement.clientWidth);
                var whDiff    = (dmsi.splitTags.origWidth - document.documentElement.clientHeight);
                var abshwDiff = Math.abs(hwDiff);
                var abswhDiff = Math.abs(whDiff);               
                                
                /* handle orienation changes - should not hide the process split button */
                if (dmsi.splitTags.origHeight !== document.documentElement.clientHeight &&
                    abshwDiff < 100 && 
                    abswhDiff < 100) {
                    dmsi.splitTags.origHeight = document.documentElement.clientHeight;
                    dmsi.splitTags.origWidth = document.documentElement.clientWidth;
                        
                }
                
                if (document.documentElement.clientHeight < dmsi.splitTags.origHeight){                
                    $('#splitTagsUpdateBtn').hide();
                    $('#splitTagsUpdateBtnContainer').hide();
                    $('#splitTagsFooter').hide();                    
                }
                else{
                    $('#splitTagsUpdateBtn').show();
                    $('#splitTagsUpdateBtnContainer').show();
                    $('#splitTagsFooter').show();         
                }                
                
            },
            
            onBackKeyPressed: function() {
                logWriter.writeLog("splitTags.js",0,"onBackKeyPressed()");
                document.getElementById("splitTagsFromTagID").blur();
                dmsi.app.replace (dmsi.config.views.mainMenu, "slide:right");
            },
            
            clearFromTagID: function (e) {
                var tagInput = document.getElementById("splitTagsFromTagID");
                var that = this;
                that.clearFromTagValues();    
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },
            
            clearFromTagQty: function (e) {
                var tagInput = document.getElementById("splitTagsFromTagQty");
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },
            
            clearToTagID: function (e) {
                var tagInput = document.getElementById("splitTagsToTagID");
                var that = this;
                that.clearToTagValues();  
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },
            
            clearToTagLocation: function (e) {
                var tagInput = document.getElementById("splitTagsToTagLocation");
                tagInput.value = "";
                tagInput.text  = "";
                tagInput.focus();
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            }, 
            
            clearToTagLot: function (e) {
                
                if (document.getElementById("splitTagsToTagLot").disabled === false) {
                    var tagInput = document.getElementById("splitTagsToTagLot");
                    tagInput.value = "";
                    tagInput.text  = "";
                    tagInput.focus();
                }
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },    
            
            clearPrinter: function (e) {
                               
                var printerInput = document.getElementById("splitTagsPrinter");
                printerInput.value = "";
                printerInput.text  = "";
                printerInput.focus();                
                
                if (e) {
                    e.preventDefault();                    
                    e.preventDefault();                    
                }
            },  
            
            fromTagEntered: function(e) {                
                var that = this;         
                var tagID = document.getElementById("splitTagsFromTagID").value;
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                                
                if (tagID === "undefined" || tagID === null || tagID === "") {
                    that.clearFromTagValues(); 
                    document.getElementById("splitTagsFromTagID").blur();
                }
                else if (tagID === that.currFromTagTag) {
                    document.getElementById("splitTagsFromTagID").blur();                      
                }
                else if (tagID === document.getElementById("splitTagsToTagID").value) { 
                     that.clearFromTagValues();
                     $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "From tag cannot match to tag."},"ErrorSplit");                    
                     return;                    
                }             
                else {  
                    for (var tag = 0;tag < that.currFromTagList.length;tag++) {
                        
                          if (that.currFromTagList[tag] === tagID) {
                              that.clearFromTagValues();
                              $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Tag has already been added to list."},"ErrorSplit");                                       
                              return;                                                            
                          }                              
                    }  
                        
                    document.getElementById("splitTagsFromTagQty").value = "";    
                    
                    that.currFromTagContent = "";
                    that.currFromTagLocation = "";
                    that.currFromTagLot = "";                   
                    that.currFromTagQtyConv = "";
                    that.currFromTagOnHandQty = 0;
                                    
                    if (that.tagsEntered === false) {
                       that.currFromTagItem = "";
                       that.currFromTagSize = "";
                       that.currFromTagDesc = "";
                       that.currFromTagDimensionString = "";
                       that.currFromTagThickness = 0;
                       that.currFromTagWidth = 0;
                       that.currFromTagLength = 0;
                       that.currFromTagPieceCount = 0;
                       that.currFromTagLotCode = true;
                       that.currFromTagContentCode = true; 
                       that.currFromTagQtyUOM = "";               
                                                              
                       if (that.currToTagItem === "undefined" ||
                           that.currToTagItem === null ||
                           that.currToTagItem === "") {
                          document.getElementById("splitTagsFromTagInfo").textContent = "";  
                       }                       
                    }
                    
                    this.fetchFromTag(tagID);                      
                }
                
                //if (e) {
                 //   e.preventDefault();
               // }
            },   
            
            toTagEntered: function(e) {                
                var that = this;              
                var tagID = document.getElementById("splitTagsToTagID").value;
                var connection = navigator.onLine ? "online" : "offline";
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                                
                if (tagID === "undefined" || tagID === null || tagID === "") {
                    that.clearToTagValues();       
                    document.getElementById("splitTagsToTagID").blur();
                }     
                else if (tagID === that.currToTagTag) {
                    document.getElementById("splitTagsToTagID").blur();
                }
                else if (tagID === document.getElementById("splitTagsFromTagID").value) { 
                    that.clearToTagValues();  
                    document.getElementById("splitTagsToTagID").focus();      
                    $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "To tag cannot match a from tag."},"ErrorSplit");                     
                    return;                    
                }              
                else {   
                     for (var tag = 0;tag < that.currFromTagList.length;tag++) {
                        
                          if (that.currFromTagList[tag] === tagID) {
                              that.clearToTagValues();
                              $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Tag exists in the tags to split list."},"ErrorSplit");                               
                              return;                                                            
                          }                              
                    }  
                    
                    this.fetchToTag(tagID);                     
                }
                
                //if (e) {
                //    e.preventDefault();
                //}
            },  
            
            addToListSelected: function(applyFocus) {                
                var that = this;              
                var tagID = document.getElementById("splitTagsFromTagID").value;
                var splitQty = Number(document.getElementById("splitTagsFromTagQty").value);
                var remainingQty = (that.currFromTagOnHandQty - splitQty);
                remainingQty = remainingQty.toFixed(10);
                var newTagSequence = that.currFromTagSequence + 1;                   
                
                if (tagID === "undefined" || tagID === null || tagID === "") {                  
                    return;                    
                    document.getElementById("splitTagsFromTagID").focus();
                } 
             
                if (splitQty === 0) { 
                    $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "A valid split qty must be specified."},"ErrorSplit");                                           
                    return;
                }   
                
                if (splitQty < 0 ){
                   $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Split qty cannot be negative."},"ErrorSplit");
                    return;                    
                }                                                     
            
                var stringSplitQty      = document.getElementById("splitTagsFromTagQty").value; 
                var stringSplitQtyArray = stringSplitQty.split('.');                      
                
                if (stringSplitQtyArray.length === 2 &&
                    stringSplitQtyArray[1].length > 10) {
                   $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Split qty cannot contain more than 10 decimals."},"ErrorSplit");                                           
                    return;                        
                }
                
                if (splitQty > that.currFromTagOnHandQty) {
                    document.getElementById("splitTagsFromTagQty").value = that.currFromTagOnHandQty;
                    $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Split qty cannot exceed on hand qty."},"ErrorSplit");   
                    return;
                }                
                     
                                                  
                if (document.getElementById("splitTagsToTagID").value !== "" &&
                    document.getElementById("splitTagsToTagID").value !== null &&
                    document.getElementById("splitTagsToTagID").value !== "undefined"){  
                      that.currToTagQtyUOM = that.currFromTagQtyUOM;
                }
                
                dmsi.dbManager.addSplitTagTag(newTagSequence, dmsi.model.session.branchId, tagID, that.currFromTagItem, that.currFromTagSize, that.currFromTagDesc, that.currFromTagDimensionString, that.currFromTagThickness, that.currFromTagWidth, that.currFromTagLength, that.currFromTagPieceCount, that.currFromTagLotCode, that.currFromTagContentCode, that.currFromTagLocation, that.currFromTagLot, that.currFromTagContent, splitQty, that.currFromTagQtyUOM, that.currFromTagQtyConv, remainingQty);                      
                that.clearFromTagValues();
                
                if (applyFocus === true) {
                  document.getElementById("splitTagsFromTagID").focus(); 
                }
                
              
            },      
            
            getNewTag: function(e) {
                var that = this;
                var connection = navigator.onLine ? "online" : "offline";
              
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
                
                var promise = dmsi.model.getNewTagDataSource();

                promise.done(function (detailDataSource) {
                    logWriter.writeLog("splitTags.js",0,"getNewTag():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("splitTags.js",0,"getNewTag(): detailDataSource.at(0): " + detailDataSource.at(0) );
                    
                    if ( detailDataSource.at(0).lErrorMessage === true ) {
                        $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: detailDataSource.at(0).cMessage},"ErrorSplit");                         
                        return;                        
                    }
                    else { 
                        that.currToTagTag = detailDataSource.at(0).tag; 
                        that.currToTagExistingQty = 0;    
                        that.currToTagNewTag = true;
                                                           
                        if (that.currFromTagItem !== "undefined" &&  
                            that.currFromTagItem !== null &&
                            that.currFromTagItem !== "") {
                            that.currToTagItem = that.currFromTagItem;  
                            that.currToTagWidth = that.currFromTagWidth;
                            that.currToTagLength = that.currFromTagLength;
                            that.currToTagLotCode = that.currFromTagLotCode;                           
                            that.currToTagContentCode = that.currFromTagContentCode;                                
                            that.currToTagOrigLocation = "";
                            that.currToTagOrigLot = "";
                            that.currToTagOrigContent = "";
                            that.currToTagPieceCount = that.currFromTagPieceCount;
                            that.currToTagQtyUOM = that.currFromTagQtyUOM;
                        }
                        else {
                           that.currToTagItem = "";
                           that.currToTagWidth = 0;
                           that.currToTagLength = 0;
                           that.currToTagLotCode = true;                            
                           that.currToTagContentCode = true;
                           that.currToTagOrigLocation = "";
                           that.currToTagOrigLot = "";
                           that.currToTagOrigContent = "";                            
                           that.currToTagPieceCount = 0;
                           that.currToTagQtyUOM = "";                          
                        }
                                  
                        var dTotalToTagQty = that.currFromTagsTotalQty;

                        document.getElementById("splitTagsToTagID").value = detailDataSource.at(0).tag;                                    
                        document.getElementById("splitTagsToTagTotalQty").textContent = dTotalToTagQty + " " + that.currToTagQtyUOM;                                                
                        document.getElementById("splitTagsToTagExistingQty").textContent = that.currToTagExistingQty + " " + that.currToTagQtyUOM;                                                           
                       
                    }                                                            
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });               
                
            },
            
            processSplit: function(e) {                
                var connection = navigator.onLine ? "online" : "offline";
                var that = this;
                
                if (that.splitInProcess === true) {
                    return;
                }
                
                that.splitInProcess = true;
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    that.splitInProcess = false;
                    return;
                }
               
                if (that.tagsEntered === false) {
                    $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify at least one tag to split from."},"ErrorSplit");  
                    that.splitInProcess = false;
                    return;
                }
                
                var cToTagIDValue = document.getElementById("splitTagsToTagID").value.trim();
                
                if (cToTagIDValue === undefined ||
                    cToTagIDValue === null ||
                    cToTagIDValue === "") {
                   $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify a split to tag."},"ErrorSplit");
                    that.splitInProcess = false;
                    return;                        
                }
                
                if (document.getElementById("splitTagsToTagLocation").value === undefined ||
                    document.getElementById("splitTagsToTagLocation").value === null ||
                    document.getElementById("splitTagsToTagLocation").value === "" ||
                    document.getElementById("splitTagsToTagLocation").value === "N/A") {
                     $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify a valid location."},"ErrorSplit");  
                     that.splitInProcess = false;
                     return;
                }
                else
                {
                   var foundLocation = false;
                   var location = document.getElementById("splitTagsToTagLocation").value;
                   for ( var ix = 0;ix < dmsi.model.session.validLocationList.length;ix++) {
                       if ( dmsi.model.session.validLocationList[ix].location.toUpperCase() === location.toUpperCase() )
                           foundLocation = true;
                   }           
                     
                   if(!foundLocation) {
                      $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify a valid location."},"ErrorSplit");  
                      that.splitInProcess = false;
                      return;                    
                   }                                   
                }
                
                var cLotValue = document.getElementById("splitTagsToTagLot").value.trim();
                
                if  ((document.getElementById("splitTagsToTagLot").disabled === false) &&
                    (cLotValue === undefined ||
                     cLotValue === null ||
                     cLotValue === "" ||
                     cLotValue === "N/A")) {
                    $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify a valid lot."},"ErrorSplit"); 
                    that.splitInProcess = false;
                    return;
                }
                
                var labelPrinter = document.getElementById("splitTagsPrinter").value.toUpperCase();
                
                if (document.getElementById("splitTagsPrintTagsOptionList").value === "Auto print") {                
                           
                   if (labelPrinter === undefined ||
                       labelPrinter === null ||
                       labelPrinter === "") {
                       $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify a valid printer."},"ErrorSplit"); 
                       that.splitInProcess = false;
                       return;                           
                   }
                   else {
                       var foundPrinter = false;
                       
                        for ( var ix = 0;ix < dmsi.model.userDefaults.labelPrinterList.length;ix++) {
                            if ( dmsi.model.userDefaults.labelPrinterList[ix] === labelPrinter )
                                foundPrinter = true;
                        }           
                       
                        if(!foundPrinter) {
                           $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "You must specify a valid printer."},"ErrorSplit");
                           that.splitInProcess = false;
                           return;                    
                        }                       
                       
                   }     
                }

                that.disableEnableFields(true);
                
                window.localStorage["splitTagsPrintOption_rem"] = document.getElementById("splitTagsPrintTagsOptionList").value;   
                
                if (document.getElementById("splitTagsPrintTagsOptionList").value === "Auto print") {  
                  window.localStorage["labelPrinter_rem"] = labelPrinter;     
                  that.defaultLabelPrinter = labelPrinter;
                }
                
                var promise = dmsi.model.splitTagsStep1(document.getElementById("splitTagsToTagID").value,document.getElementById("splitTagsToTagTotalQty").textContent,document.getElementById("splitTagsToTagLocation").value,document.getElementById("splitTagsToTagLot").value,
                                                   that.currToTagExistingQty,that.currToTagOrigLocation,that.currToTagOrigLot,that.currToTagOrigContent,document.getElementById("splitTagsPrintTagsOptionList").value, labelPrinter);

                promise.done(function () {
                     var printOption = document.getElementById("splitTagsPrintTagsOptionList").value;
                     var newTag = document.getElementById("splitTagsToTagID").value;
                     var successMessage = "Split ";
                     var numTagsPrinted = dmsi.model.session.printTagsResultsTable.length;                     
            
                     if (that.numTagsEntered > 1) {
                        successMessage = successMessage + that.numTagsEntered + " tags into tag " + newTag;
                     }
                     else {
                        successMessage = successMessage + that.numTagsEntered + " tag into tag " + newTag;
                     }
                     
                     if (document.getElementById("splitTagsPrintTagsOptionList").value === "Auto print") {
                        if (numTagsPrinted > 1) {                                                   
                            successMessage = successMessage + " and printed " + numTagsPrinted + " tags to printer " + labelPrinter;
                        }
                        else {
                            successMessage = successMessage + " and printed 1 tag to printer " + labelPrinter;
                        }
                     }
                    
                     dmsi.model.session.printTagsItemInfo = document.getElementById("splitTagsFromTagInfo").textContent;
                     that.splitInProcess = false;
                     that.disableEnableFields(false);                    
                     that.clearValues();  
                    
                    
                    $("#splitTagsStaticNotification").data("kendoNotification").show({SuccessfulSplitMessage: successMessage},"SuccessfulSplit");
                    
                     if (printOption === "Prompt to print") { 
                       dmsi.app.navigate (dmsi.config.views.printTags, "slide"); 
                     }                   
                });   
                
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("splitTags.js",99,"promise fail: " + errorMsg); 
                    that.splitInProcess = false;
                });
            },   
            
            loadLabelPrinters: function () {
                var deferred = $.Deferred();
                var that = this;
                
                dmsi.model.userDefaults.labelPrinterList.length = 0;
                        
                var promise = dmsi.model.getLabelPrintersDataSource();

                promise.done(function (labelPrintersDataSource) {
                    labelPrintersDataSource.read().then(function() {
                        var defaultPrinter = "";
                        var theData = labelPrintersDataSource.data();
                        
                        for (var x = 0; x < theData.length; x++) {
                            dmsi.model.userDefaults.labelPrinterList.push(theData[x].printer_id.toUpperCase());                                                       
                            if (theData[x].printer_id.toUpperCase() === window.localStorage["labelPrinter_rem"]) {
                                that.defaultLabelPrinter = theData[x].printer_id.toUpperCase();
                            }
                        }  
                        
                        that.onShowStep2();
                    });
                    return deferred.resolve();
                });                
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("splitTags.js",99,"promise fail: " + errorMsg);
                    return deferred.reject();
                });
            },
            
            onLogout : function () {
                logWriter.writeLog("splitTags.js",0,"onLogout()");
                document.getElementById("splitTagsFromTagID").blur();
                
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
                       
            
            fetchFromTag : function (tagID) {
                logWriter.writeLog("splitTags.js",0,"fetchFromTag()");
                var that = this;
                
                var promise = dmsi.model.getTagInfoDataSource(tagID);

                promise.done(function (detailDataSource) {
                    logWriter.writeLog("splitTags.js",0,"fetchFromTag():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("splitTags.js",0,"fetchFromTag(): detailDataSource.at(0): " + detailDataSource.at(0) );
                    
                    if ( detailDataSource.at(0).lErrorMessage === true ) {
                        that.clearFromTagValues();
                        $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: detailDataSource.at(0).cMessage},"ErrorSplit");                      
                        return;
                        
                    }
                    else {
                        var item   = detailDataSource.at(0).item;
                        var size   = detailDataSource.at(0).size;
                        var desc   = detailDataSource.at(0).description;
                        var qty    = detailDataSource.at(0).display_uom_quantity;
                        var qtyUOM = detailDataSource.at(0).display_uom;   
                        var dimensionString = detailDataSource.at(0).dimension_string;                           
                        
                        if (detailDataSource.at(0).inv_tag_type === "Mult Unit, Sngl PC Cnt & Lngth" ||
                            detailDataSource.at(0).inv_tag_type === "Sngl Unit & PC Cnt, Mult Lngth") {
                           that.clearFromTagValues();
                           $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Tag type assigned to item not allowed."},"ErrorSplit"); 
                           return;                                                        
                        }
                        
                        if ((that.currFromTagItem !== "undefined" &&
                             that.currFromTagItem !== null &&
                             that.currFromTagItem !== "" &&
                             that.currFromTagItem !== item) ||
                            (that.currToTagItem !== "undefined" &&
                             that.currToTagItem !== null &&
                             that.currToTagItem !== "" &&
                             that.currToTagItem !== item)){
                            that.clearFromTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Item on from/to tags must match."},"ErrorSplit");                      
                            return;                            
                        } 
                        
                        if ( detailDataSource.at(0).has_multiple_contents === true ) {
                            that.clearFromTagValues();     
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Tag has multiple content level records."},"ErrorSplit");                                        
                            return;                                                                    
                        }                        
                        
                        if ((that.currFromTagItem !== "undefined" &&
                             that.currFromTagItem !== null &&
                             that.currFromTagItem !== "" &&
                             that.currFromTagWidth !== detailDataSource.at(0).width) ||
                            (that.currToTagItem !== "undefined" &&
                             that.currToTagItem !== null &&
                             that.currToTagItem !== "" &&
                             that.currToTagWidth !== detailDataSource.at(0).width)){
                            that.clearFromTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Width on from/to tags must match."},"ErrorSplit");                              
                            return;                            
                        } 
                        
                        if ((that.currFromTagItem !== "undefined" &&
                             that.currFromTagItem !== null &&
                             that.currFromTagItem !== "" &&
                             that.currFromTagLength !== detailDataSource.at(0).length) ||
                            (that.currToTagItem !== "undefined" &&
                             that.currToTagItem !== null &&
                             that.currToTagItem !== "" &&
                             that.currToTagLength !== detailDataSource.at(0).length)){
                            that.clearFromTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Length on from/to tags must match."},"ErrorSplit");                              
                            return;                            
                        } 
                         
                        if ((that.currFromTagItem !== "undefined" &&
                             that.currFromTagItem !== null &&
                             that.currFromTagItem !== "" &&
                             that.currFromTagPieceCount !== detailDataSource.at(0).piece_count) ||
                            (that.currToTagItem !== "undefined" &&
                             that.currToTagItem !== null &&
                             that.currToTagItem !== "" &&
                             that.currToTagPieceCount !== detailDataSource.at(0).piece_count)){
                            that.clearFromTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Piece count on from/to tags must match."},"ErrorSplit");                             
                            return;                            
                        }                                                 
                        
                        if (that.currFromTagItem === "undefined" ||
                            that.currFromTagItem === null ||
                            that.currFromTagItem === "") {
                            that.currFromTagItem = item;
                            that.currFromTagSize = size;
                            that.currFromTagDesc = desc;
                            that.currFromTagDimensionString = dimensionString;
                            that.currFromTagThickness = detailDataSource.at(0).thickness;
                            that.currFromTagWidth = detailDataSource.at(0).width;                            
                            that.currFromTagLength = detailDataSource.at(0).length;
                            that.currFromTagPieceCount = detailDataSource.at(0).piece_count;
                            that.currFromTagLotCode = detailDataSource.at(0).lotcode;
                            that.currFromTagContentCode = detailDataSource.at(0).contentcode;
                        }                        
                        
                        if (detailDataSource.at(0).lotcode === false) {                            
                            document.getElementById("splitTagsToTagLot").disabled = true;
                        }
                        else {                              
                            document.getElementById("splitTagsToTagLot").disabled = false;
                        }                           
                        
                        that.currFromTagTag = tagID;
                        that.currFromTagQtyUOM = detailDataSource.at(0).display_uom;
                        that.currFromTagQtyConv = detailDataSource.at(0).display_uom_conv_factor;
                        that.currFromTagLocation = detailDataSource.at(0).location;
                        that.currFromTagLot = detailDataSource.at(0).lot;
                        that.currFromTagContent = detailDataSource.at(0).content;
                        that.currFromTagOnHandQty= detailDataSource.at(0).display_uom_quantity;
                                              
                        if (dimensionString !== "undefined" &&
                            dimensionString !== null &&
                            dimensionString !== "") {
                          document.getElementById("splitTagsFromTagInfo").textContent = item + " " + size + " " + desc + " - " + dimensionString;   
                        }
                        else {
                          document.getElementById("splitTagsFromTagInfo").textContent = item + " " + size + " " + desc;       
                        }
                      
                        document.getElementById("splitTagsFromTagExistingQty").textContent = qty + " " + qtyUOM;            

                        document.getElementById("splitTagsFromTagID").blur();                           
                    }                                                            
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });
            },
 
            fetchToTag : function (tagID) {
                logWriter.writeLog("splitTags.js",0,"fetchToTag()");
                var that = this;
                var tagNotFoundMessage = "Tag " + tagID + " was not found.";
                
                var promise = dmsi.model.getTagInfoDataSource(tagID);

                promise.done(function (detailDataSource) {
                    logWriter.writeLog("splitTags.js",0,"fetchToTag():promise.done");
                    dmsi.model.hideLoading();
                    detailDataSource.read();
                    
                    logWriter.writeLog("splitTags.js",0,"fetchToTag(): detailDataSource.at(0): " + detailDataSource.at(0) );
                   
     
                    if ( detailDataSource.at(0).lErrorMessage === true &&
                         detailDataSource.at(0).cMessage      !== tagNotFoundMessage) {
                        that.clearToTagValues();                 
                        $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: detailDataSource.at(0).cMessage},"ErrorSplit");                         
                        return;
                    }
                    else if ( detailDataSource.at(0).lErrorMessage === true &&
                              detailDataSource.at(0).cMessage      === tagNotFoundMessage) {

                       if (tagID.startsWith("T") === true ||
                           tagID.startsWith("t") === true) {
                            that.clearToTagValues();                             
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "New tags cannot start with 'T'."},"ErrorSplit");                         
                            return;                           
                       }
                            
                       that.currToTagTag = tagID;
                       that.currToTagExistingQty = 0;  
                       that.currToTagNewTag = true;
                                                           
                       if (that.currFromTagItem !== "undefined" &&  
                           that.currFromTagItem !== null &&
                           that.currFromTagItem !== "") {
                           that.currToTagItem = that.currFromTagItem;
                           that.currToTagWidth = that.currFromTagWidth;
                           that.currToTagLength = that.currFromTagLength;
                           that.currToTagLotCode = that.currFromTagLotCode;
                           that.currToTagContentCode = that.currFromTagContentCode;
                           that.currToTagOrigLocation = "";
                           that.currToTagOrigLot = "";
                           that.currToTagOrigContent = "";                               
                           that.currToTagPieceCount = that.currFromTagPieceCount;
                           that.currToTagQtyUOM = that.currFromTagQtyUOM;
                       }
                       else {
                          that.currToTagItem = "";
                          that.currToTagWidth = 0;
                          that.currToTagLength = 0;
                          that.currToTagLotCode = true;
                          that.currToTagContentCode = true;
                          that.currToTagOrigLocation = "";
                          that.currToTagOrigLot = "";
                          that.currToTagOrigContent = "";                           
                          that.currToTagPieceCount = 0;
                          that.currToTagQtyUOM = "";                          
                       }
                                  
                       var dTotalToTagQty = that.currFromTagsTotalQty;
                                    
                       document.getElementById("splitTagsToTagTotalQty").textContent = dTotalToTagQty + " " + that.currToTagQtyUOM;                                                
                       document.getElementById("splitTagsToTagExistingQty").textContent = that.currToTagExistingQty + " " + that.currToTagQtyUOM;  
                       document.getElementById("splitTagsToTagID").blur();                                  
                    }
                    else {
                        var item            = detailDataSource.at(0).item;
                        var size            = detailDataSource.at(0).size;
                        var desc            = detailDataSource.at(0).description;
                        var dimensionString = detailDataSource.at(0).dimension_string;
                        var location        = detailDataSource.at(0).location;
                        var lot             = detailDataSource.at(0).lot;
                        var qty             = detailDataSource.at(0).display_uom_quantity;
                        var qtyUOM          = detailDataSource.at(0).display_uom;                                             
                        
                       if (that.currFromTagItem !== "undefined" &&
                           that.currFromTagItem !== null && 
                           that.currFromTagItem !== "" &&
                           that.currFromTagItem !== item) {
                            that.clearToTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Item on from/to tags must match."},"ErrorSplit");                           
                            return;                            
                        }                            
                        
                       if ( detailDataSource.at(0).has_multiple_contents === true ) {
                            that.clearToTagValues();   
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Tag has multiple content level records."},"ErrorSplit");      
                            return;                                                                    
                        }
                        
                       if (that.currFromTagItem !== "undefined" &&
                           that.currFromTagItem !== null && 
                           that.currFromTagItem !== "" &&
                           that.currFromTagWidth !== detailDataSource.at(0).width) {
                            that.clearToTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Width on from/to tags must match."},"ErrorSplit");                                       
                            return;                            
                        }
                        
                       if (that.currFromTagItem !== "undefined" &&
                           that.currFromTagItem !== null && 
                           that.currFromTagItem !== "" &&
                           that.currFromTagLength !== detailDataSource.at(0).length) {
                            that.clearToTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Length on from/to tags must match."},"ErrorSplit");       
                            return;                            
                        } 
                        
                       if (that.currFromTagItem !== "undefined" &&
                           that.currFromTagItem !== null && 
                           that.currFromTagItem !== "" &&
                           that.currFromTagPieceCount !== detailDataSource.at(0).piece_count) {
                            that.clearToTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Piece count on from/to tags must match."},"ErrorSplit");                             
                            return;                            
                        } 

                        if (detailDataSource.at(0).inv_tag_type === "Mult Unit, Sngl PC Cnt & Lngth" ||
                            detailDataSource.at(0).inv_tag_type === "Sngl Unit & PC Cnt, Mult Lngth") {
                           that.clearToTagValues();
                            $("#splitTagsStaticNotification").data("kendoNotification").show({ErrorSplitMessage: "Tag type assigned to item not allowed."},"ErrorSplit");                           
                            return;                                                        
                        }                        
                        
                        that.currToTagTag = tagID;
                        that.currToTagNewTag = false;
                        that.currToTagItem = item;
                        that.currToTagWidth = detailDataSource.at(0).width;
                        that.currToTagLength = detailDataSource.at(0).length;
                        that.currToTagPieceCount = detailDataSource.at(0).piece_count;
                        that.currToTagLotCode = detailDataSource.at(0).lotcode;
                        that.currToTagContentCode = detailDataSource.at(0).contentcode;
                        that.currToTagOrigLocation = detailDataSource.at(0).location;
                        that.currToTagOrigLot = detailDataSource.at(0).lot;
                        that.currToTagOrigContent = detailDataSource.at(0).content;                       
                        that.currToTagExistingQty = qty;
                        that.currToTagQtyUOM = qtyUOM;
                                   
                        var dTotalToTagQty = that.currToTagExistingQty + that.currFromTagsTotalQty;
                        
                        if (dimensionString !== "undefined" &&
                            dimensionString !== null &&
                            dimensionString !== "") {
                          document.getElementById("splitTagsFromTagInfo").textContent = item + " " + size + " " + desc + " - " + dimensionString;   
                        }
                        else {
                          document.getElementById("splitTagsFromTagInfo").textContent = item + " " + size + " " + desc;       
                        }
                                                
                        document.getElementById("splitTagsToTagTotalQty").textContent = dTotalToTagQty + " " + that.currToTagQtyUOM;                                                
                        document.getElementById("splitTagsToTagExistingQty").textContent = qty + " " + that.currToTagQtyUOM;
                        document.getElementById("splitTagsToTagLocation").value = location;
                                                
                        if (detailDataSource.at(0).lotcode === false) {
                            document.getElementById("splitTagsToTagLot").disabled = true;
                        }
                        else {
                            document.getElementById("splitTagsToTagLot").disabled = false;
                            document.getElementById("splitTagsToTagLot").value = lot;                               
                        }  
                        
                        document.getElementById("splitTagsToTagID").blur();
                    }                                                            
                });
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("model.js",99,"promise fail: " + errorMsg);
                });
            },
            
            onInit: function() {
                 var that = this;
             
                 logWriter.writeLog("splitTags.js",0,"onInit()");

                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: that.onTabSelect                
                 });       
                 
                 $("#splitTagsStaticNotification").kendoNotification({
                     templates: [{
                         type: "SuccessfulSplit",
                         template: $("#splitTagsSuccessfulSplitTemplate").html()
                     },
                     {
                         type: "ErrorSplit",
                         template: $("#splitTagsErrorSplitTemplate").html()
                     }],                 
                     autoHideAfter: 4000,                    
                     hideOnClick: true,
                     button: true,
                     position: {
                         pinned: true,
                         top: 0,
                         left: 0
                     }
                     
                 });      
                 
                 $("#splitTagsResultList").kendoListView({
                    dataSource: {
                        data: []
                    },
                    template: $("#splitTagsResultListTemplate").text(), 
                    style: "inset",                    
                    selectable: false,
                    pullToRefresh: true                 
                });   
                               
                 
                $('#splitTagsFromTagID').on('focus', function () {
                    var input = $(this);
                    if (input.val().length > 0) {
                        setTimeout( function () { input[0].setSelectionRange(0,999); }, 0);            
                    }
                });
            
                // Register the event listener
                window.addEventListener("resize", this.onWindowResize, true);     
            },
            
            onShow: function() {
                
                var that = this;
                logWriter.writeLog("splitTags.js",0,"onShow()");
                dmsi.splitTags.origHeight = document.documentElement.clientHeight;
                dmsi.splitTags.origWidth = document.documentElement.clientWidth;     
                document.getElementById("splitTagsbranchbarid").textContent = dmsi.model.session.branchId;  
                that.defaultLabelPrinter = "";
                that.loadLabelPrinters();      
            }, 
            
            onShowStep2: function() {
                
                var that = this;
                logWriter.writeLog("splitTags.js",0,"onShowStep2()");
                that.onWindowResize();
                that.clearValues();
                that.getFromTagListDataSource();
                
                if (that.currFromTagDimensionString !== "undefined" &&
                    that.currFromTagDimensionString !== null &&
                    that.currFromTagDimensionString !== "") {
                   document.getElementById("splitTagsFromTagInfo").textContent = that.currFromTagItem + " " + that.currFromTagSize + " " + that.currFromTagDesc + " - " + that.currFromTagDimensionString;   
                }
                else {
                   document.getElementById("splitTagsFromTagInfo").textContent = that.currFromTagItem + " " + that.currFromTagSize + " " + that.currFromTagDesc;      
                }
                
                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);                    
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            }, 

            getFromTagListDataSource: function() {
                           
                var that = this;
                
                dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                "SELECT * FROM splitTags WHERE branch_id=? ORDER BY sequence DESC", 
                                [dmsi.model.session.branchId], 
                                function(tx, rs){   
                                    var splitTagsResultTable= []; 
                                    var i=0;
                                    
                                    that.currFromTagList      = [];
                                    that.currFromTagSequence  = 0;
                                    that.currFromTagsTotalQty = 0;                                    
                                    
                                    if (rs.rows.length > 0) {                                                                                                                                                            
                                        that.tagsEntered = true;
                                        that.numTagsEntered = rs.rows.length;                                      
                                    }
                                    else {                                                                                                                                                                             
                                     that.tagsEntered = false;
                                     that.numTagsEntered = 0;                                                                           
                                     that.currFromTagItem = "";
                                     that.currFromTagSize = "";
                                     that.currFromTagDesc = "";
                                     that.currFromTagDimensionString = "";
                                     that.currFromTagThickness = 0;
                                     that.currFromTagWidth = 0;                                     
                                     that.currFromTagLength = 0;
                                     that.currFromTagPieceCount = 0;
                                     that.currFromTagLotCode = true;
                                     that.currFromTagContentCode = true;
                                     that.currFromTagLocation = "";
                                     that.currFromTagLot = "";   
                                     that.currFromTagContent = "";
                                     that.currFromTagQtyUOM = "";                                     
                                        
                                     if (that.currToTagItem === "undefined" ||
                                         that.currToTagItem === null ||
                                         that.currToTagItem === "") {
                                          document.getElementById("splitTagsFromTagInfo").textContent = "";  
                                     }                                     
                                    }
                                                                        
                                    for(i=0;i<rs.rows.length;i++){
                                        splitTagsResultTable.push(rs.rows.item(i));
                                        
                                        that.currFromTagsTotalQty = that.currFromTagsTotalQty + rs.rows.item(i).split_qty;
                                        that.currFromTagList.push(rs.rows.item(i).tag);
                                        if (i === (rs.rows.length - 1)) {                                            
                                            that.currFromTagItem = rs.rows.item(i).item;
                                            that.currFromTagSize = rs.rows.item(i).size;
                                            that.currFromTagDesc = rs.rows.item(i).desc;
                                            that.currFromTagDimensionString = rs.rows.item(i).dimension_string;
                                            that.currFromTagThickness = rs.rows.item(i).thickness;
                                            that.currFromTagWidth = rs.rows.item(i).width;
                                            that.currFromTagLength = rs.rows.item(i).length;
                                            that.currFromTagPieceCount = rs.rows.item(i).piece_count;                                            
                                            that.currFromTagQtyUOM = rs.rows.item(i).split_qty_uom;
                                            that.currFromTagLocation = rs.rows.item(i).location;
                                            that.currFromTagLot = rs.rows.item(i).lot;
                                            
                                            if (rs.rows.item(i).lotcode === "true") {
                                                that.currFromTagLotCode = true;
                                            }
                                            else {
                                                that.currFromTagLotCode = false;
                                            }
                                            
                                            if (rs.rows.item(i).contentcode === "true") {
                                                that.currFromTagContentCode = true;
                                            }
                                            else {
                                                that.currFromTagContentCode = false;
                                            }
                                                      
                                            if (that.currFromTagDimensionString !== "undefined" &&
                                                that.currFromTagDimensionString !== null &&
                                                that.currFromTagDimensionString !== "") {
                                              document.getElementById("splitTagsFromTagInfo").textContent = that.currFromTagItem + " " + that.currFromTagSize + " " + that.currFromTagDesc + " - " + that.currFromTagDimensionString;   
                                            }
                                            else {
                                              document.getElementById("splitTagsFromTagInfo").textContent = that.currFromTagItem + " " + that.currFromTagSize + " " + that.currFromTagDesc;       
                                            }                                                                                       
                                        }
                                        
                                        if (rs.rows.item(i).sequence > that.currFromTagSequence) {
                                            that.currFromTagSequence = rs.rows.item(i).sequence;
                                        }
                                        
                                    }
                                    
                                    var dTotalToTagQty = that.currToTagExistingQty + that.currFromTagsTotalQty;                                    
                                    
                                    if (document.getElementById("splitTagsToTagID").value !== "" &&
                                        document.getElementById("splitTagsToTagID").value !== null &&
                                        document.getElementById("splitTagsToTagID").value !== "undefined"){                             
                                        document.getElementById("splitTagsToTagExistingQty").textContent = that.currToTagExistingQty + " " + that.currToTagQtyUOM;
                                        document.getElementById("splitTagsToTagTotalQty").textContent = dTotalToTagQty + " " + that.currToTagQtyUOM;
                                    }
                                  
                                    if (document.getElementById("splitTagsToTagID").value === "" ||
                                        document.getElementById("splitTagsToTagID").value === null ||
                                        document.getElementById("splitTagsToTagID").value === "undefined" ||
                                        that.currToTagNewTag === true){                                           
                                        
                                         if (that.tagsEntered === false) {
                                              
                                            if (document.getElementById("splitTagsToTagID").value === "" ||
                                                document.getElementById("splitTagsToTagID").value === null ||
                                                document.getElementById("splitTagsToTagID").value === "undefined") {                                                    
                                                        document.getElementById("splitTagsToTagLocation").value = "";    
                                            }
                                             
                                            if (document.getElementById("splitTagsToTagID").value !== "" &&
                                                document.getElementById("splitTagsToTagID").value !== null &&
                                                document.getElementById("splitTagsToTagID").value !== "undefined" &&
                                                that.currToTagLotCode === false) {
                                                document.getElementById("splitTagsToTagLot").disabled = true; 
                                                document.getElementById("splitTagsToTagLot").value    = "";
                                            }
                                            else {
                                                document.getElementById("splitTagsToTagLot").disabled = false; 
                                                
                                                if (document.getElementById("splitTagsToTagID").value === "" ||
                                                    document.getElementById("splitTagsToTagID").value === null ||
                                                    document.getElementById("splitTagsToTagID").value === "undefined") {
                                                        document.getElementById("splitTagsToTagLot").value = "";
                                                    }
                                            }                                       
                                        }
                                        else {
                                         
                                             if (document.getElementById("splitTagsToTagLocation").value === "" ||
                                                 document.getElementById("splitTagsToTagLocation").value === null ||
                                                 document.getElementById("splitTagsToTagLocation").value === "undefined") {
                                                  document.getElementById("splitTagsToTagLocation").value = that.currFromTagLocation;
                                             }
                                        
                                            if (that.currFromTagLotCode === true) {
  
                                                document.getElementById("splitTagsToTagLot").disabled = false;
                                            
                                                if (document.getElementById("splitTagsToTagLot").value === "" ||
                                                    document.getElementById("splitTagsToTagLot").value === null ||
                                                    document.getElementById("splitTagsToTagLot").value === "undefined") {                                             
                                                  document.getElementById("splitTagsToTagLot").value = that.currFromTagLot;                                                                                       
                                                }                                            
                                            }
                                            else {
                                                document.getElementById("splitTagsToTagLot").disabled = true; 
                                                document.getElementById("splitTagsToTagLot").value    = "";
                                            }
                                        }
                                    }        
                                    
                                    
                                    var newDataSource = new kendo.data.DataSource({data: splitTagsResultTable}); 
                                    $("#splitTagsResultList").data("kendoListView").setDataSource(newDataSource); 
                                    $("#splitTagsResultList").data("kendoListView").dataSource.read(); 
                                    
                                },                               
                                function(tx, rs){
                                })
                            	});                                             
            },
            
            clearValues: function() {
                var that = this;
                logWriter.writeLog("splitTags.js",0,"clearValues()");
                dmsi.app.scroller().reset(); 
                document.getElementById("splitTagsFromTagID").value = "";
                document.getElementById("splitTagsFromTagInfo").textContent = "";
                document.getElementById("splitTagsFromTagExistingQty").textContent = 0;
                document.getElementById("splitTagsFromTagQty").value = "";    
                document.getElementById("splitTagsToTagID").value = "";
                document.getElementById("splitTagsToTagExistingQty").textContent = 0;
                document.getElementById("splitTagsToTagTotalQty").textContent = 0;
                document.getElementById("splitTagsToTagLocation").value = "";
                document.getElementById("splitTagsToTagLot").value = "";      
                document.getElementById("splitTagsToTagLot").disabled = false; 
                
                if (window.localStorage["splitTagsPrintOption_rem"] === "Auto print" ||
                    window.localStorage["splitTagsPrintOption_rem"] === undefined ||
                    window.localStorage["splitTagsPrintOption_rem"] === null ||
                    window.localStorage["splitTagsPrintOption_rem"] === "") {
                           
                    document.getElementById("splitTagsPrintTagsOptionList").value = "Auto print";
                    document.getElementById("splitTagsPrinter").disabled = false;                    
                        
                    if (that.defaultLabelPrinter === undefined ||
                        that.defaultLabelPrinter === null ||
                        that.defaultLabelPrinter === "") {
                        document.getElementById("splitTagsPrinter").value = ""; 
                    }
                    else {
                        document.getElementById("splitTagsPrinter").value = that.defaultLabelPrinter; 
                    }
                }
                else {
                    document.getElementById("splitTagsPrintTagsOptionList").value = window.localStorage["splitTagsPrintOption_rem"];
                    document.getElementById("splitTagsPrinter").value = "";
                    document.getElementById("splitTagsPrinter").disabled = true;                    
                }
                
                that.splitInProcess       = false;
                that.currFromTagList      = [];
                that.currFromTagSequence  = 0;
                that.currFromTagsTotalQty = 0;                           
                
                var emptyDataSource = new kendo.data.DataSource({data: []});
                $("#splitTagsResultList").data("kendoListView").setDataSource(emptyDataSource);                             
                that.tagsEntered = false;
                that.numTagsEntered = 0;
                that.currFromTagTag = "";
                that.currFromTagItem = "";
                that.currFromTagSize = "";
                that.currFromTagDesc = "";
                that.currFromTagDimensionString = "";
                that.currFromTagThickness = 0;
                that.currFromTagWidth = 0;
                that.currFromTagLength = 0;
                that.currFromTagPieceCount = 0;
                that.currFromTagLotCode = true;
                that.currFromTagContentCode = true;
                that.currFromTagLocation = "";
                that.currFromTagLot = "";
                that.currFromTagContent = "";
                that.currFromTagQtyUOM = "";
                that.currFromTagQtyConv = "";
                that.currFromTagOnHandQty = 0;
                that.currFromTagSequence = 0;
                that.currFromTagsTotalQty = 0;
                that.currToTagTag = "";
                that.currToTagNewTag = false;
                that.currToTagItem = "";
                that.currToTagWidth = 0;
                that.currToTagLength = 0;
                that.currToTagLotCode = true;
                that.currToTagContentCode = true;
                that.currToTagOrigLocation = "";
                that.currToTagOrigLot = "";
                that.currToTagOrigContent = "";                
                that.currToTagPieceCount = 0;
                that.currToTagQtyUOM = "";
                that.currToTagExistingQty = 0;
            },
            
            clearFromTagValues: function() {
                var that = this;
                logWriter.writeLog("splitTags.js",0,"clearFromTagValues()");        
                               
                that.currFromTagTag = "";
                that.currFromTagLocation = "";
                that.currFromTagLot = "";
                that.currFromTagContent = "";                
                that.currFromTagQtyConv = "";    
                that.currFromTagOnHandQty = 0;
                
                if (that.tagsEntered === false) {
                   that.currFromTagItem = "";
                   that.currFromTagSize = "";
                   that.currFromTagDesc = "";
                   that.currFromTagDimensionString = "";
                   that.currFromTagThickness = 0;
                   that.currFromTagWidth = 0;
                   that.currFromTagLength = 0;
                   that.currFromTagPieceCount = 0;
                   that.currFromTagLotCode = true;
                   that.currFromTagContentCode = true; 
                   that.currFromTagQtyUOM = "";
                
                   if (that.currFromTagItem !== "undefined" &&  
                       that.currFromTagItem !== null &&
                       that.currFromTagItem !== "") {                    
                        if (that.currToTagLotCode === false) {                            
                             document.getElementById("splitTagsToTagLot").disabled = true;
                        }
                        else {                              
                             document.getElementById("splitTagsToTagLot").disabled = false;
                        }                         
                   }    
                   else {      
                       document.getElementById("splitTagsToTagLot").disabled = false;                       
                       
                       if (that.currToTagItem === "undefined" ||  
                           that.currToTagItem === null ||
                           that.currToTagItem === "") {             
                             document.getElementById("splitTagsFromTagInfo").textContent = "";                                
                       } 
                       else {
                           if (that.currToTagLotCode === false) {
                             document.getElementById("splitTagsToTagLot").disabled = true;                                 
                           }
                       }
                   }
               }                                                           
               else {
                   if (that.currFromTagLotCode === false) {                            
                         document.getElementById("splitTagsToTagLot").disabled = true;
                    }
                    else {                              
                         document.getElementById("splitTagsToTagLot").disabled = false;
                    }                     
               }        
        
                document.getElementById("splitTagsFromTagID").value = "";
                document.getElementById("splitTagsFromTagQty").value = "";
                document.getElementById("splitTagsFromTagExistingQty").textContent = 0;                                               
            },
            
            clearToTagValues: function() {
                var that = this;
                logWriter.writeLog("splitTags.js",0,"clearToTagValues()");                
                document.getElementById("splitTagsToTagID").value = "";
                document.getElementById("splitTagsToTagExistingQty").textContent = 0;
                document.getElementById("splitTagsToTagTotalQty").textContent = 0;                
                that.currToTagTag = "";
                that.currToTagNewTag = false;
                that.currToTagItem = "";
                that.currToTagWidth = 0;
                that.currToTagLength = 0;
                that.currToTagPieceCount = 0;
                that.currToTagLotCode = true;
                that.currToTagContentCode = true;
                that.currToTagOrigLocation = "";
                that.currToTagOrigLot = "";
                that.currToTagOrigContent = "";                
                that.currToTagQtyUOM = "";
                that.currToTagExistingQty = 0;
                
                if (that.tagsEntered === false) {
                    document.getElementById("splitTagsToTagLocation").value = "";
                    document.getElementById("splitTagsToTagLot").value = "";                        
                }
                
                if (that.currFromTagItem === "undefined" ||  
                    that.currFromTagItem === null ||
                    that.currFromTagItem === "") {             
                     document.getElementById("splitTagsFromTagInfo").textContent = "";                               
                }
                
                if (that.currFromTagLotCode === false) {                            
                     document.getElementById("splitTagsToTagLot").disabled = true;
                }
                else {                              
                     document.getElementById("splitTagsToTagLot").disabled = false;
                }                                     
            },
            
           disableEnableFields: function(setDisabled) {
                var that = this;
                logWriter.writeLog("splitTags.js",0,"disableEnableFields()");                
                document.getElementById("splitTagsFromTagID").disabled = setDisabled;                               
                document.getElementById("splitTagsFromTagQty").disabled = setDisabled;    
                document.getElementById("splitTagsToTagID").disabled = setDisabled;                
                document.getElementById("splitTagsToTagLocation").disabled = setDisabled;                 
                document.getElementById("splitTagsToTagLot").disabled = setDisabled; 
                document.getElementById("splitTagsPrintTagsOptionList").disabled = setDisabled;
                document.getElementById("splitTagsPrinter").disabled = setDisabled;  
                document.getElementById("splitTagsUpdateBtn").disabled = setDisabled;
                              
            },
            
            onPrintOptionsChange: function() {
               
               var that = this;
                
               if ( document.getElementById("splitTagsPrintTagsOptionList").value === "Auto print" ) {
                
                    document.getElementById("splitTagsPrinter").disabled = false;
                        
                    if (that.defaultLabelPrinter === undefined ||
                        that.defaultLabelPrinter === null ||
                        that.defaultLabelPrinter === "") {
                        document.getElementById("splitTagsPrinter").value = ""; 
                    }
                    else {
                        document.getElementById("splitTagsPrinter").value = that.defaultLabelPrinter; 
                    }
                }
                else {                  
                    document.getElementById("splitTagsPrinter").value = "";
                    document.getElementById("splitTagsPrinter").disabled = true;                    
                }                
            },
            
            deleteTagListEntry: function(tagToDelete, tagSequenceToDelete) {
                             var that = this;
                             logWriter.writeLog("splitTags.js",0,"DeleteTagListEntry()");
                             dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                "DELETE FROM splitTags WHERE branch_id=? AND tag=? AND sequence=?", 
                                [dmsi.model.session.branchId,tagToDelete,tagSequenceToDelete], 
                                function(tx, rs){   
                                    that.getFromTagListDataSource();                                     
                                },                               
                                function(tx, rs){
                                })
                            	});                                 
            },               
            
            fromTagQtyKeyPressed: function (e){
                
                var that = this;               
                 
                if(e.keyCode === 13){                                                                                    
                    that.addToListSelected(true); 
                }   
            },
            
            fromTagKeyPressed: function (e){
                
                var that = this;               
                 
                if(e.keyCode === 13){                                                                                    
                    that.fromTagEntered(); 
                }   
            },
            
            toTagKeyPressed: function (e){
                
                var that = this;               
                 
                if(e.keyCode === 13){                                                                                    
                    that.toTagEntered(); 
                }   
            }, 
           
        })
    };

} ( dmsi ) ); //pass in global namespace
