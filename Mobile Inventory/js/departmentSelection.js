(function( dmsi ) {
    "use strict";
    
    dmsi.departmentSelection = {       
        
        departmentSelectionInProcess: false,
        
        viewModel: kendo.observable ( {
            
            onWindowResize: function() {
                var title = "";
                
                //Small phones and the gun
                if (document.documentElement.clientWidth < 350) {
                    title = "Dept Selection";    
                }
                else {
                    title = "Department Selection";
                }
                
                $("#departmentSelectionTabTitle").html(title);
            },
            
            onBackKeyPressed: function() {
                logWriter.writeLog("departmentSelection.js",0,"onBackKeyPressed()");
                dmsi.app.replace (dmsi.config.views.mainMenu, "slide:right");
            },
            
            loadDepartments: function () {
                logWriter.writeLog("departmentSelection.js",0,"loadDepartments()");
                var loadDepartmentsFunc = this;
                
                var deferred = $.Deferred();
                
                if (dmsi.model.session.departments.length > 0) {
                    var formattedDataSource = new kendo.data.DataSource({data: dmsi.model.session.departments});
                              
                    loadDepartmentsFunc.listView = $("#departmentsList").kendoListView({
                        dataSource: formattedDataSource,
                        template: kendo.template($("#departmentsTemplate").html()),
                        selectable: true,
                        change: loadDepartmentsFunc.onDepartmentChange
                    });
                }
                else {
                    var promise = dmsi.model.getDepartmentDataSource();

                    promise.done(function (departmentsDataSource) {
                        departmentsDataSource.read().then(function() {
                            logWriter.writeLog("departmentSelection.js",0,"loadDepartmentsPromiseDone()");
                            dmsi.model.session.departments = departmentsDataSource.data()[0];
                            
                            
                            
                            if (dmsi.model.session.departments === undefined) {
                                dmsi.model.session.departments = [];
                            }
                            
                            if (dmsi.model.session.departments.length > 0){
                                
                                // show the workfiles list
                                $("#departmentsListEmpty").addClass('hidden');
                                $("#departmentsListEmpty").removeClass('visible');
                                $("#departmentsList").addClass('visible');
                                $("#departmentsList").removeClass('hidden');
                                
                                // Use in status update page
                                dmsi.model.session.reasons = departmentsDataSource.data()[1];
                                
                                var formattedDataSource = new kendo.data.DataSource({data: dmsi.model.session.departments});
                              
                                loadDepartmentsFunc.listView = $("#departmentsList").kendoListView({
                                    dataSource: formattedDataSource,
                                    template: kendo.template($("#departmentsTemplate").html()),
                                    selectable: true,
                                    change: loadDepartmentsFunc.onDepartmentChange
                                });
                                
                            }
                            else {
                                $("#departmentsListEmpty").addClass('visible');
                                $("#departmentsListEmpty").removeClass('hidden');
                                $("#departmentsList").addClass('hidden');
                                $("#departmentsList").removeClass('visible');
                            }
                        });
                        return deferred.resolve();
                    });
                    
                    promise.fail(function (errorMsg) {
                        logWriter.writeLog("departmentSelection.js",99,"promise fail: " + errorMsg);
                        return deferred.reject();
                    });
                }
            },
            
            onLogout : function () {
                logWriter.writeLog("departmentSelection.js",0,"onLogout()");
                
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },
                         
            onInit: function() {
                
                logWriter.writeLog("departmentSelection.js",0,"onInit()");
            
                // Register the event listener
                window.addEventListener("resize", this.onWindowResize, true);     
                
            },
            
            onDepartmentChange: function(e) {
                  
                    var list = $('#departmentsList').data('kendoListView');
                    var view = list.dataSource.view();
                    var trackStatus = "";
                    
                    if (list.select().index() < 0) {
                        return;
                    }
                    
                    dmsi.model.session.selectedDept = view[list.select().index()].department;
                    
                    logWriter.writeLog("departmentSelection.js",0,"onDepartmentChange(): Selected " + dmsi.model.session.selectedDept );
                     
                    var connection = navigator.onLine ? "online" : "offline";
                    if (connection === "offline") {
                      $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                      return;
                    }

                    
                    trackStatus                           = view[list.select().index()].mobilewh_track_status.toUpperCase();
                    dmsi.model.session.deptTrackTime      = view[list.select().index()].mobilewh_track_time;
                    dmsi.model.session.deptDefaultNumEmployees = view[list.select().index()].mobilewh_default_num_employees;
                    dmsi.model.session.deptAllowReasonCodes = view[list.select().index()].mobilewh_allow_reason_codes;

                    if(trackStatus === "SCAN INTO DEPARTMENT")
                        dmsi.app.replace (dmsi.config.views.departmentScanOnly, "slide:left"); 
                    else 
                        dmsi.app.replace (dmsi.config.views.departmentScanning, "slide:left");                  
                  
            },
            
            onShow: function() {
                
                var onShowFunc = this;
                logWriter.writeLog("departmentSelection.js",0,"onShow()");
                onShowFunc.onWindowResize();
                document.getElementById("departmentSelectionbranchbarid").textContent = dmsi.model.session.branchId;
                
                onShowFunc.loadDepartments(); 
                
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },
        })
    };

} ( dmsi ) ); //pass in global namespace
