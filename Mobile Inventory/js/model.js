(function (dmsi) {
    "use strict";
    
    dmsi.model = {
        
        init: function () {
            logWriter.writeLog("model.js",0,"init()");
            progress.util.jsdoSettingsProcessor(dmsi.config.jsdoSettings);
        },
        
        jsdoUserSession: null,
        
        isUserLoggedIn: false,

        userDefaults: {
        	userIdWithTenant: "",
            tenant: "",
            userId: "",
            userPassword: "",
            defaultBranchId: "",
            availBranchList: "",
            labelPrinterList: [],
            labelPrinterArray: [],
            labelPrinterDefault: ""
        },
        
        session : {
            branchId: "",
            sessionId: "",
            sessionTimedOut: false,
            workfile_id: "",
            workfile_seq: 0,
            workfileIdSeqDesc: "",
            workfileSelectionCriteria: "",
            workfileLocationRange: "",
            workfileCreatedDate: "",
            printTagsItemInfo: "",
            printTagsResultsTable: [],    
            outstandingData: false,
            diySessionID: "",
            diyObjectId: "",
            tagInfoTag: "",
            departments: [],
            selectedDept: "",
            deptTrackTime: false,
            deptDefaultNumEmployees: 1,
            deptAllowReasonCodes: false,
            reasons: [],
            
        },

        showLoading: function(){
            
            kendo.ui.progress($(document.body),true);
        },
             
        hideLoading: function(){
            
            kendo.ui.progress($(document.body),false);
        },
        
        getUserDefaultsDataSource : function (filter, userIdentification, recursiveCall) {
            
            var deferred = $.Deferred(),
                that = this;

            logWriter.writeLog("model.js",50,"getUserDefaultsDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof that.UserDefaults === "undefined" || that.UserDefaults === null)
                that.UserDefaults = new progress.data.JSDO({ name: 'MobileWHUserDefaults' });

            var jsdoObject = that.UserDefaults.GetUserDefaults({Filter: filter,  UserIdentification: userIdentification}).deferred;

            jsdoObject.done(
                function onGetUserDefaults(jsdo, success, request) {
                    var res = request.response;

                    dmsi.model.hideLoading();
                    
                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetUserDefaults():success");                    
                        
                        that.userDefaultsDataSource = new kendo.data.HierarchicalDataSource({
                            data: [res.a_mobilewhuserdefaultsds.a_mobilewhuserdefaultsds.a_mobilewhuserdefaults,
                                  res.a_mobilewhuserdefaultsds.a_mobilewhuserdefaultsds.a_mobilewhUserMenuMain,
                                  res.a_mobilewhuserdefaultsds.a_mobilewhuserdefaultsds.a_mobilewhUserMenuLevel1,
                                  res.a_mobilewhuserdefaultsds.a_mobilewhuserdefaultsds.a_mobilewhLocation]
                        });
                        deferred.resolve(that.userDefaultsDataSource);
                    } else {         
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.cannotLogin, "error");
                   
                       
            		    that.goLogout();
                                    
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );            
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getUserDefaultsDataSource(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noServer, "error");
                dmsi.model.goLogout();
            });
            return deferred.promise();
        },
        
        printTag : function (printTagData) {
            var deferred = $.Deferred(),
                that     = this;
            
            logWriter.writeLog("model.js",50,"printTag(" + printTagData + ")");
            dmsi.model.showLoading();
            
            if ( typeof dmsi.model.PrintTagJSDO === "undefined" || dmsi.model.PrintTagJSDO === null)
                dmsi.model.PrintTagJSDO = new progress.data.JSDO({ name: 'MobileWHTagPrinting' });
            
            var printTagsDataSource = kendo.stringify(printTagData);

            var jsdoObject = dmsi.model.PrintTagJSDO.PrintTags({ IncomingData: printTagsDataSource }).deferred;
            
            jsdoObject.done(
                function onPrintTag(jsdo, success, request) {
                    logWriter.writeLog("model.js", 50, "printTag() returned");
                    var res = request.response;
                    
                    dmsi.model.hideLoading();
                    if(success) {
                        logWriter.writeLog("model.js", 0, "printTagData():success");
                        deferred.resolve(true);
                    }
                    else {
                        logWriter.writeLog("model.js", 50, "printTagData():fail");
                        deferred.reject(false);
                    }
                }
            )
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"printTag(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        getLabelPrintersDataSource : function () {
              var deferred = $.Deferred(),
                  that     = this;
            logWriter.writeLog("model.js",50,"getLabelPrintersDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.LabelPrintersJSDO === "undefined" || dmsi.model.LabelPrintersJSDO === null)
              dmsi.model.LabelPrintersJSDO = new progress.data.JSDO({ name: 'MobileWHTagPrinting' });
            
            var jsdoObject = dmsi.model.LabelPrintersJSDO.GetLabelPrinters({BranchID: this.session.branchId}).deferred;
            
            jsdoObject.done(
                function onGetLabelPrinters(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getLabelPrintersDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetLabelPrinters():success");
                        that.labelPrinterDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhlabelprintersds.a_mobilewhlabelprintersds.a_mobilewhlabelprinters
                        });
                        deferred.resolve(that.labelPrinterDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onGetLabelPrinters():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getLabelPrintersDataSource(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noServer, "error");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        getNewTagDataSource : function () {
              var deferred = $.Deferred(),
                  that     = this;
            logWriter.writeLog("model.js",50,"getNewTagDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.TagSequenceJSDO === "undefined" || dmsi.model.TagSequenceJSDO === null)
              dmsi.model.TagSequenceJSDO = new progress.data.JSDO({ name: 'MobileWHTagSequence' });
            
            var jsdoObject = dmsi.model.TagSequenceJSDO.GetTagSequences({BranchID: this.session.branchId, NumberOfTags: 1}).deferred;
            
            jsdoObject.done(
                function onGetTagSequences(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getNewTagDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetTagSequences():success");
                        that.newTagDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhtagsequenceds.a_mobilewhtagsequenceds.a_mobilewhtagsequence
                        });
                        deferred.resolve(that.newTagDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onGetTagSequences():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getNewTagDataSource(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        getTagInfoDataSource : function (tagID) {
              var deferred = $.Deferred(),
                  that     = this;
            logWriter.writeLog("model.js",50,"getTagInfoDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.TagInfoJSDO === "undefined" || dmsi.model.TagInfoJSDO === null)
              dmsi.model.TagInfoJSDO = new progress.data.JSDO({ name: 'MobileWHInventoryTag' });
            
            var jsdoObject = dmsi.model.TagInfoJSDO.GetTag({BranchID: this.session.branchId, Tag: tagID}).deferred;
            
            jsdoObject.done(
                function onGetInvTag(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getTagInfoDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetInvTag():success");
                        that.tagInfoDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhinventorytagds.a_mobilewhinventorytagds.a_mobilewhinventorytag
                        });
                        deferred.resolve(that.tagInfoDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onGetInvTag():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getTagInfoDataSource(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        getWorkfilesDataSource: function() {
            var deferred = $.Deferred(),
                  that     = this;
            logWriter.writeLog("model.js",50,"getWorkfilesDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.WorkfilesJSDO === "undefined" || dmsi.model.WorkfilesJSDO === null)
              dmsi.model.WorkfilesJSDO = new progress.data.JSDO({ name: 'MobileWHPIWorkfile' }); //????? where is this defined?? and what is it called?
            
            var jsdoObject = dmsi.model.WorkfilesJSDO.GetPIWorkfiles({"BranchID": dmsi.model.session.branchId}).deferred; /// ???? I think this is it, based on the Runscope tests
            
            jsdoObject.done(
                function onGetPIWorkfiles(jsdo, success, request) {
                    var res = request.response;
                    logWriter.writeLog("model.js",50,"getWorkfilesDataSource().onGetPIWorkfiles()");
                    
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",0,"getWorkfilesDataSource():success");
                        that.workfilesDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhpiworkfileds.a_mobilewhpiworkfileds.a_mobilewhpiworkfile
                        });
                        deferred.resolve(that.workfilesDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"getWorkfilesDataSource():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                       
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }
                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getWorkfilesDataSource(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },

        getWOIDInfoDataSource : function (WOID) {
              var deferred        = $.Deferred(),
                  getWOIDInfoFunc = this;
            logWriter.writeLog("model.js",50,"getWOIDInfoDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.WOIDJSDO === "undefined" || dmsi.model.WOIDJSDO === null)
              dmsi.model.WOIDJSDO = new progress.data.JSDO({ name: 'MobileWHWOComplete' });
            
            var jsdoObject = dmsi.model.WOIDJSDO.GetWOHeader({BranchID: this.session.branchId, WOID: WOID}).deferred;
            
            jsdoObject.done(
                function onGetWOID(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getWOIDInfoDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetWOID():success");
                        getWOIDInfoFunc.WOIDDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhwoheaderds.a_mobilewhwoheaderds.a_mobilewhwoheader
                        });
                        deferred.resolve(getWOIDInfoFunc.WOIDDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onGetWOID():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getWOIDInfoDataSource(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        completeWorkorder : function (completeWorkorderTable) {
              var deferred                 = $.Deferred(),
                  completeWorkorderFunc    = this,
                  completeWorkorderDataSource;
            
            logWriter.writeLog("model.js",50,"completeWorkorder()");
            
            if (typeof dmsi.model.WOIDJSDO === "undefined" || dmsi.model.WOIDJSDO === null)
              dmsi.model.WOIDJSDO = new progress.data.JSDO({ name: 'MobileWHWOComplete' });
            
            var newDataSource = completeWorkorderTable;
            completeWorkorderDataSource = kendo.stringify(newDataSource);   
            var jsdoObject = dmsi.model.WOIDJSDO.CompleteWO({IncomingData: completeWorkorderDataSource}).deferred;
            
            jsdoObject.done(
                function onCompleteWO(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"completeWorkorder() returned");

                    if (success) {
                        logWriter.writeLog("model.js",50,"onCompleteWO():success");
                        completeWorkorderFunc.WOIDDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhwoheaderds.a_mobilewhwoheaderds.a_mobilewhwoheader
                        });
                        deferred.resolve(completeWorkorderFunc.WOIDDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onCompleteWO():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"completeWorkorder(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
  
        completeDeptQty : function (completeDeptQtyTable) {
             var deferred = $.Deferred(),
                 completeDeptQtyFunc = this,
                 completeDeptQtyDataSource;
            
            logWriter.writeLog("model.js",50,"completeDeptQty()");
            
            if (typeof dmsi.model.CompleteDeptQtyJSDO === "undefined" || dmsi.model.CompleteDeptQtyJSDO === null)
              dmsi.model.CompleteDeptQtyJSDO = new progress.data.JSDO({ name: 'MobileWHDeptScanning' });
            
            var newDataSource = completeDeptQtyTable;
            completeDeptQtyDataSource = kendo.stringify(newDataSource);   
            var jsdoObject = dmsi.model.CompleteDeptQtyJSDO.CompleteDeptQty({IncomingData: completeDeptQtyDataSource}).deferred;
            
            jsdoObject.done(
                function onCompleteDeptQty(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"completeDeptQty() returned");

                    if (success) {
                        logWriter.writeLog("model.js",50,"onCompleteDeptQty():success");
                        completeDeptQtyFunc.WOIDDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhdeptscanningds.a_mobilewhdeptscanningds.a_mobilewhwoheader
                        });
                        deferred.resolve(completeDeptQtyFunc.WOIDDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onCompleteDeptQty():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"completeDeptQty(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        getDepartmentDataSource : function () {
              var deferred          = $.Deferred(),
                  getDepartmentFunc = this;
            logWriter.writeLog("model.js",50,"getDepartmentDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.DepartmentJSDO === "undefined" || dmsi.model.DepartmentJSDO === null)
              dmsi.model.DepartmentJSDO = new progress.data.JSDO({ name: 'MobileWHDepartmentInfo' });
            
            var jsdoObject = dmsi.model.DepartmentJSDO.GetDepartments({BranchID: this.session.branchId}).deferred;
            
            jsdoObject.done(
                function onGetDepartments(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getDepartmentDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetDepartments():success");
                        getDepartmentFunc.DepartmentDataSource = new kendo.data.HierarchicalDataSource({
                            data: [res.a_mobilewhdepartmentinfods.a_mobilewhdepartmentinfods.a_mobilewhdepartment,
                                  res.a_mobilewhdepartmentinfods.a_mobilewhdepartmentinfods.a_mobilewhreasoncodes]
                        });

                        deferred.resolve(getDepartmentFunc.DepartmentDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onGetDepartments():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getDepartmentDataSource(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        getScheduledDepartmentDataSource : function (dept, WOID) {
              var deferred = $.Deferred(),
                  getScheduledDepartment = this;
            logWriter.writeLog("model.js",50,"getScheduledDepartmentDataSource()");
            
            dmsi.model.showLoading();
            
            if (typeof dmsi.model.ScheduledDepartmentJSDO === "undefined" || dmsi.model.ScheduledDepartmentJSDO === null)
              dmsi.model.ScheduledDepartmentJSDO = new progress.data.JSDO({ name: 'MobileWHDeptScanning' });
            
            var jsdoObject = dmsi.model.ScheduledDepartmentJSDO.GetScheduledDepartment({BranchID: this.session.branchId, DepartmentCode: dept, WOID: WOID}).deferred;
            
            jsdoObject.done(
                function onGetDepartments(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getScheduledDepartmentDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",50,"onGetScheduledDepartment():success");
                        getScheduledDepartment.scheduledDepartmentDataSource = new kendo.data.HierarchicalDataSource({
                            data: [res.a_mobilewhdeptscanningds.a_mobilewhdeptscanningds.a_mobilescheduletran]
                        });

                        deferred.resolve(getScheduledDepartment.scheduledDepartmentDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onGetScheduledDepartment():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                                   
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getScheduledDepartmentDataSource(): jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },
        
        moveInventory : function(moveTagsTable){
              var deferred = $.Deferred(),
                  that     = this,
                  moveInventoryDataSource;
                  
            
            logWriter.writeLog("model.js",50,"moveInventory()");
            
            if (typeof dmsi.model.MobileWHItemAdjMoveInventory === "undefined" || dmsi.model.MobileWHItemAdjMoveInventory === null)
              dmsi.model.MobileWHItemAdjMoveInventory = new progress.data.JSDO({ name: 'MobileWHInventoryAdj' });

            var newDataSource = moveTagsTable;
            moveInventoryDataSource = kendo.stringify(newDataSource);    
            var jsdoObject = dmsi.model.MobileWHItemAdjMoveInventory.MoveInventory({IncomingData: moveInventoryDataSource}).deferred;                               
 
            jsdoObject.done(
                function onMoveInventory(jsdo, success, request) {
                    var res = request.response;
                    logWriter.writeLog("model.js",50,"moveInventory() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",0,"onMoveInventory():success");
                        deferred.resolve();
                    } else {         
                        logWriter.writeLog("model.js",0,"onMoveInventory():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                       
            		    that.goLogout();
                                    
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }
                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"moveInventory(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();            
        },
        
        verifyTags : function(verifyTagsTable){
              var deferred = $.Deferred(),
                  that     = this,
                  verifyTagDataSource;
                  
            
            logWriter.writeLog("model.js",50,"verifyTags()");
            
            if (typeof dmsi.model.workfileTagsJSDO === "undefined" || dmsi.model.workfileTagsJSDO === null)
              dmsi.model.workfileTagsJSDO = new progress.data.JSDO({ name: 'MobileWHWorkfileTag' });

            var newDataSource = verifyTagsTable;
            verifyTagDataSource = kendo.stringify(newDataSource);    
            var jsdoObject = dmsi.model.workfileTagsJSDO.TagVerification({IncomingData: verifyTagDataSource}).deferred;
            
            jsdoObject.done(
                function onVerifyTag(jsdo, success, request) {
                    var res = request.response;
                    logWriter.writeLog("model.js",50,"onVerifyTag().onMoveInventory()");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",0,"onVerifyTag():success");
                        deferred.resolve();
                    } else {         
                        logWriter.writeLog("model.js",0,"onVerifyTag():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                       
            		    that.goLogout();
                                    
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }
                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"verifyTags(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();            
        },
        
        splitTagsStep1 : function (toTagID, toTagNewQty, toTagLocation, toTagLot, toTagOrigQty, toTagOrigLocation, toTagOrigLot, toTagOrigContent, printerOption, printerSelection) {
            logWriter.writeLog("model.js",0,"splitTagsStep1()");
            var that = this;
            var deferred = $.Deferred();
            var printTagsResultTable= [];
            var splitTagsCtr = 0;
            
            dmsi.model.showLoading();
            
            function printTagRecord ( branch, tag, tagQty, printer, sequence ) {
                  this.branch_id   = branch;
                  this.tag         = tag;
                  this.tag_qty     = tagQty;                  
                  this.printer_id  = printer;
                  this.sequence    = sequence;
            }            
          
            function splitTagRecord ( branch, tag, content, thickness, width, length, piece_count, qty, qty_uom, sequence) {
                  this.branch_id    = branch;
                  this.tag          = tag;
                  this.new_tag      = toTagID;
                  this.content      = content;
                  this.thickness    = thickness;
                  this.width        = width;
                  this.length       = length;
                  this.piece_count  = piece_count;
                  this.new_location = toTagLocation;
                  this.new_lot      = toTagLot;
                  this.adj_qty      = qty;
                  this.adj_qty_uom  = qty_uom;
                  this.sequence     = sequence;
            }                        
                        
            dmsi.model.session.printTagsResultsTable = [];
            
            dmsi.dbManager.db.transaction(function(tx) {
                   tx.executeSql(
                        "SELECT * FROM splitTags WHERE branch_id=? ORDER BY sequence ASC", 
                        [that.session.branchId], 
                        selectSuccess,
                        selectFail)
              	});                                          
            
            function selectSuccess (tx, rs) {
                logWriter.writeLog("model.js",0,"splitTagsStep1(): Query complete ");                 
                              
                if(rs !== undefined && rs.rows.length > 0 ) {                                
                     var splitTagsResultTable= []; 
                     
                     var i = 0;
                     for(i=0;i<rs.rows.length;i++){  
                         if (i===0) {
                             if (printerOption !== "Do not print") {                           
                                 var printTagRec = new printTagRecord(dmsi.model.session.branchId,toTagID,toTagNewQty,printerSelection,1);
                    
                                 dmsi.model.session.printTagsResultsTable.push(printTagRec);
                                 printTagsResultTable.push(printTagRec);                                   
                             }
                             
                             if (toTagOrigQty      > 0 &&
                                (toTagOrigLocation !== toTagLocation ||
                                (toTagOrigLot      !== "" &&
                                 toTagOrigLot      !== "N/A" &&
                                 toTagOrigLot      !== toTagLot))) {                                                                   
                                     
                                   splitTagsCtr = splitTagsCtr + 1;
                                     
                                   var splitTagRec = new splitTagRecord(dmsi.model.session.branchId,toTagID,toTagOrigContent,rs.rows.item(i).thickness,rs.rows.item(i).width,rs.rows.item(i).length,
                                                                        rs.rows.item(i).piece_count,toTagOrigQty,rs.rows.item(i).split_qty_uom,splitTagsCtr);
                                     
                                   splitTagsResultTable.push(splitTagRec);                                                                           
                             }
                         }                                                  
                         
                         splitTagsCtr = splitTagsCtr + 1;
                         
                         var splitTagRec = new splitTagRecord(dmsi.model.session.branchId,rs.rows.item(i).tag,rs.rows.item(i).content,rs.rows.item(i).thickness,rs.rows.item(i).width,rs.rows.item(i).length,
                                                              rs.rows.item(i).piece_count,rs.rows.item(i).split_qty,rs.rows.item(i).split_qty_uom,splitTagsCtr);
                
                         splitTagsResultTable.push(splitTagRec);      
               
                         if (printerOption !== "Do not print" &&
                             rs.rows.item(i).remaining_qty > 0) {
                             var printTagRec = new printTagRecord(dmsi.model.session.branchId,rs.rows.item(i).tag,rs.rows.item(i).remaining_qty + " " + rs.rows.item(i).split_qty_uom,printerSelection,rs.rows.item(i).sequence);
                             dmsi.model.session.printTagsResultsTable.push(printTagRec);
                             printTagsResultTable.push(printTagRec);                         
                         }
                     }                
                }
                
                var promise = dmsi.model.splitTagsStep2(splitTagsResultTable,printTagsResultTable,printerOption);

                promise.done(function () {
                    dmsi.model.hideLoading();
                    deferred.resolve();                                         
                });   
                
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("splitTags.js",99,"splitTagsStep1 promise fail: " + errorMsg);
                    deferred.reject();
                });
                
           
            }
            
            function selectFail (tx, rs) {
                logWriter.writeLog("model.js",0,"splitTagsStep1(): Query failed " + rs.code + ", " + rs.message);
                deferred.reject();
            }
            
            return deferred.promise();
        },
        
       splitTagsStep2 : function (splitTagsResultTable, printTagsResultTable,printerOption) {
            logWriter.writeLog("model.js",0,"splitTagsStep2()");
            var that = this;
            var deferred = $.Deferred();
                                        
            var promise = dmsi.model.splitTagsStep3(splitTagsResultTable);

            promise.done(function () {
                   
                if (printerOption === "Auto print") {
                    var promise2 = dmsi.model.printTag(printTagsResultTable);
                
                    promise2.done(function (detailDataSource) {
                        logWriter.writeLog("model.js",0,"splitTagsStep2:printTag():promise.done");
                        deferred.resolve();    
                    });
                    
                    promise2.fail(function (errorMsg) {
                        logWriter.writeLog("model.js",99,"splitTagsStep2:printTag():promise fail: " + errorMsg);
                        deferred.reject();
                    });
                }
                else {
                    deferred.resolve();                                         
                }
            });   
                
            promise.fail(function (errorMsg) {
                logWriter.writeLog("splitTags.js",99,"splitTagsStep2 promise fail: " + errorMsg);
                deferred.reject();
            });
                                      
            
            return deferred.promise();
        },
          
        splitTagsStep3 : function (splitTagsResultTable) {
           
           logWriter.writeLog("model.js",0,"splitTagsStep3()");
           var that = this;
           var deferred = $.Deferred();
           var splitTagsDataSource;
            
           if (typeof dmsi.model.MobileWHItemAdjSplitTags === "undefined" || dmsi.model.MobileWHItemAdjSplitTags === null)
              dmsi.model.MobileWHItemAdjSplitTags = new progress.data.JSDO({ name: 'MobileWHInventoryAdj' });

            var newDataSource = splitTagsResultTable;
            splitTagsDataSource = kendo.stringify(newDataSource);    
            var jsdoObject = dmsi.model.MobileWHItemAdjSplitTags.SplitTag({IncomingData: splitTagsDataSource}).deferred;                               
 
            jsdoObject.done(
                function onSplitTag(jsdo, success, request) {
                    var res = request.response;
                    logWriter.writeLog("model.js",50,"SplitTag() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",0,"onSplitTag():success");
                            
                        dmsi.dbManager.db.transaction(function(tx) {
                        		tx.executeSql(
                                    "DELETE FROM splitTags WHERE branch_id=?", 
                                    [dmsi.model.session.branchId], 
                                    function(tx, rs){   
                                        logWriter.writeLog("model.js",0,"splitTagsStep3(): Delete splitTags completed for " + dmsi.model.session.branchId);
                                        deferred.resolve();
                                    },                               
                                    function(tx, rs){
                                        logWriter.writeLog("dbManager.js",99,"doTagMove(): Delete tags failed: " + rs.code + ", " + rs.message);
                                        deferred.resolve();
                                    })
                            	});                                                  
                    } else {         
                        logWriter.writeLog("model.js",0,"onSplitTag():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                       
            		    that.goLogout();
                                    
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }
                }
            );
            
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"splitTag(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();            
        },
        
        getfetchLocationDataSource : function(locID){
              var deferred = $.Deferred(),
                  that     = this;
            logWriter.writeLog("model.js",50,"getfetchLocationDataSource()");
            
            if (typeof dmsi.model.MobileWHFetchLocation === "undefined" || dmsi.model.MobileWHFetchLocation === null)
              dmsi.model.MobileWHFetchLocation = new progress.data.JSDO({ name: 'MobileWHLocation' });
            
            var jsdoObject = dmsi.model.MobileWHFetchLocation.ValidateLocation({BranchID: this.session.branchId, Location: locID}).deferred;
            
            jsdoObject.done(
                function onValidateLocation(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getfetchLocationDataSource().onValidateLocation()");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",0,"onValidateLocation():success");
                        that.fetchLocationDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhlocationds.a_mobilewhlocationds.a_mobilewhlocation
                        });
                        deferred.resolve(that.fetchLocationDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onValidateLocation():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                       
            		    that.goLogout();
                                    
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }

                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getfetchLocationDataSource(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();           
        },
        
        getmoveTagsTagDataSource : function (tagID) {
            var deferred = $.Deferred(),
                that     = this;
            logWriter.writeLog("model.js",50,"getmoveTagsTagDataSource()");
            
            if (typeof dmsi.model.MoveGetTagInfo === "undefined" || dmsi.model.MoveGetTagInfo === null)
              dmsi.model.MoveGetTagInfo = new progress.data.JSDO({ name: 'MobileWHInventoryTag' });
            
            var jsdoObject = dmsi.model.MoveGetTagInfo.ValidateTag({BranchID: this.session.branchId, Tag: tagID}).deferred;
            
            jsdoObject.done(
                function onValidateTag(jsdo, success, request) {
                   var res = request.response;
                    logWriter.writeLog("model.js",50,"getmoveTagsTagDataSource() returned");
     
                    dmsi.model.hideLoading();

                    if (success) {
                        logWriter.writeLog("model.js",0,"onValidateTag():success");
                        that.MoveTagsTagDataSource = new kendo.data.DataSource({
                            data: res.a_mobilewhinventorytagds.a_mobilewhinventorytagds.a_mobilewhvalidatetag
                        });
                        deferred.resolve(that.MoveTagsTagDataSource);
                    } else {         
                        logWriter.writeLog("model.js",0,"onValidateTag():unsuccessful");
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.serviceFailure, "error");               
                       
            		    that.goLogout();
                                    
            		    if (res && res._errors &&
                            res._errors.length > 0) {
                                
                            var lenErrors = res._errors.length;
                            for (var idxError = 0; idxError < lenErrors; idxError++) {
                                var errorEntry = res._errors[idxError];
                                var errorMsg = errorEntry._errorMsg;
                                var errorNum = errorEntry._errorNum;
                                
                                logWriter.writeLog("model.js",99,errorNum.toString + " - " + errorMsg);                                
                            }                            
                            deferred.reject(res._errors[0]._errorMsg);
                        } else {
                            deferred.reject();
                        }
                    }
                }
            );
            jsdoObject.fail( function(jsdosession, result, info) {
                logWriter.writeLog("model.js",99,"getmoveTagsTagDataSource(): User jsdoObject.fail: " + info.xhr.response + " logging out.");
                dmsi.model.goLogout();
            });
            
            return deferred.promise();
        },

        goLogout: function(){
            logWriter.writeLog("model.js",0,"goLogout()");
            var that = this,
            promise;
             try {
                 
                promise = that.jsdoUserSession.logout();

                dmsi.model.hideLoading();
             
                promise.done( function( jsdosession, result, info ) {
                    
                    /* added the following based on https://community.progress.com/community_groups/mobile/f/17/t/20582 article */
                    dmsi.model.jsdoUserSession = null;                    
                    dmsi.model.UserDefaults = null;
                    dmsi.model.TagInfoJSDO = null;
                    dmsi.model.WorkfilesJSDO = null;
                    dmsi.model.MobileWHItemAdjMoveInventory = null;
                    dmsi.model.workfileTagsJSDO = null;
                    dmsi.model.MobileWHFetchLocation = null;
                    dmsi.model.MoveGetTagInfo = null;                                       
                    dmsi.model.PrintTagJSDO = null;
                    dmsi.model.LabelPrintersJSDO = null;
                    dmsi.model.TagSequenceJSDO = null;
                    dmsi.model.MobileWHItemAdjSplitTags = null;
                    dmsi.model.WOIDJSDO = null;
                    dmsi.model.DepartmentJSDO = null;
                    dmsi.model.ScheduledDepartmentJSDO = null;
                    dmsi.model.CompleteDeptQtyJSDO = null;
                    dmsi.model.session.departments = [];
                    dmsi.model.session.reasons = [];
                    progress.data.ServicesManager._services = [];    
                    progress.data.ServicesManager._resources = [];
                    progress.data.ServicesManager._data = [];
                    progress.data.ServicesManager._sessions = [];
                    
                    try {
                        that.isUserLoggedIn = false;
                        if (dmsi.model.session.sessionTimedOut === false) {     
                            that.userDefaults.availBranchList   = "";                          
                            that.session.branchId               = "";                    
                        }
                        if (dmsi.model.session.sessionTimedOut === false){
                            logWriter.writeLog("model.js",0,"slideright");
                            dmsi.app.replace ( dmsi.config.views.login, "slide:right");  
                        } else { 
                            logWriter.writeLog("model.js",0,"goLogout(): User running calluserlogin");
                            dmsi.model.userLogin();
                        }
                    }
                    catch(ex) {
                        that.isUserLoggedIn = false;
                        logWriter.writeLog("model.js",99,"goLogout(): User promise.done exception: " + ex);
                    }
                });
                 
                promise.fail( function(jsdosession, result, info) {
                    
                    dmsi.model.jsdoUserSession = null;                    
                    dmsi.model.UserDefaults = null;
                    dmsi.model.TagInfoJSDO = null;
                    dmsi.model.WorkfilesJSDO = null;
                    dmsi.model.MobileWHItemAdjMoveInventory = null;
                    dmsi.model.workfileTagsJSDO = null;
                    dmsi.model.MobileWHFetchLocation = null;
                    dmsi.model.MoveGetTagInfo = null;     
                    dmsi.model.PrintTagJSDO = null;
                    dmsi.model.LabelPrintersJSDO = null;
                    dmsi.model.TagSequenceJSDO = null;
                    dmsi.model.MobileWHItemAdjSplitTags = null;                                                          
                    dmsi.model.WOIDJSDO = null;
                    dmsi.model.DepartmentJSDO = null;
                    dmsi.model.ScheduledDepartmentJSDO = null;
                    dmsi.model.CompleteDeptQtyJSDO = null;
                    dmsi.model.session.departments = [];
                    dmsi.model.session.reasons = [];
                    
                    progress.data.ServicesManager._services = [];    
                    progress.data.ServicesManager._resources = [];
                    progress.data.ServicesManager._data = [];
                    progress.data.ServicesManager._sessions = [];
                    
                    try {
                        logWriter.writeLog("model.js",99,"goLogout(): User promise.fail: " + result);                       
                        dmsi.app.replace ( dmsi.config.views.login, "slide:right");  
                    }
                    catch(ex) {
                        logWriter.writeLog("model.js",99,"goLogout(): User promise.fail exception: " + ex);                        
                        dmsi.app.replace ( dmsi.config.views.login, "slide:right");  
                    }                    
                });
                 
            }
            catch(ex) {
                logWriter.writeLog("model.js",99,"goLogout(): User logout exception: " + ex);
            }            
        },
        
      diyGetURL: function (success, error){
           
            var data = {
                "objName": "environment",                
                "sessionId": dmsi.model.session.diySessionID,
                "id": dmsi.model.session.diyObjectId,               
                "output": "json"
            };
          
            this._ajaxCall("POST", "getRecord", data, success, error);
           
       },
        
       diyGetURLId: function (success, error){
           
            var data = {
                "sessionId": dmsi.model.session.diySessionID,
                "startRow": 0,
                "maxRows": 1,
                "query": "SELECT id from environment where name = '" + dmsi.model.userDefaults.tenant + "'",
                "output": "json"
            };
          
            this._ajaxCall("POST", "selectQuery", data, success, error);
           
       },
        
       diyLogin: function (success, error) {
            
            var data = {
                "loginName": "serviceinfo@diy",
                "password": "serviceinfo",
                "output": "json"
            };
          
            this._ajaxCall("POST", "login", data, success, error);
        },

        _ajaxCall: function (type, method, data, success, error) {
            
            var rollbaseUrl = dmsi.config.rollbaseSettings.baseUrl + method + '?' + $.param(data);

            var options = {
                url: rollbaseUrl,
                contentType: 'application/json',
                success: function (rsp) {
                    success(rsp);
                }
            }

            $.ajax(options).fail(function (err) {
                
                logWriter.writeLog("model.js",0,"ajaxCall failed");                
                
                dmsi.model.hideLoading();
                
                $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.loginFailed, "error");
            });
        },
        
        userLogin : function () {

            var that = this;
                
            logWriter.writeLog("model.js",0,"userLogin()");
            dmsi.model.showLoading();
            
            that.diyLogin(function(result){
                
                dmsi.model.session.diySessionID = result.sessionId;
                
                that.diyGetURLId(function(result){
                    
                    if (result[0] === undefined || result[0] === null) {
                        dmsi.model.session.diyObjectId = "";
                    }
                    else {
                        dmsi.model.session.diyObjectId = (result[0][0]);
                    }
                    
                    that.diyGetURL(function(result){
            
                        var TomcatPort = result.Tomcat_Port;
                        var restServicePrefix = result.REST_Service_Prefix;                        
                          
                        if (TomcatPort === undefined || TomcatPort === null) {
                            TomcatPort = "";
                        } 
                        else{
                            TomcatPort = ":" + TomcatPort;
                        }
                        
                        if (restServicePrefix === undefined || restServicePrefix === null) {
                            restServicePrefix = "";
                        } 
                        
                        if (result.Use_HTTPS === false){
                            dmsi.config.jsdoSettings.serviceURI = "http://" + result.DB_Server_External_IP + TomcatPort + "/" + restServicePrefix + "MobileWH";                          
                        }
                        else {
                            dmsi.config.jsdoSettings.serviceURI = "https://" + result.DB_Server_External_IP + TomcatPort + "/" + restServicePrefix + "MobileWH";    
                        }
                        
                        if (dmsi.config.jsdoSettings.serviceURI === null) {
            
                            dmsi.model.hideLoading();
                            $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.invalidURL, "error");
                        
                        } else {
                
                            dmsi.config.jsdoSettings.catalogURIs = dmsi.config.jsdoSettings.serviceURI + "/static/mobile/MobileWH.json"                    
        
                            if (dmsi.model.jsdoUserSession === null) {
                              dmsi.model.jsdoUserSession = new progress.data.JSDOSession(dmsi.config.jsdoSettings);    
                            }
                        }
                
                        that.finishLogin();
                   }, function (err) {
                          dmsi.model.hideLoading();
                   });                       
                }, function (err) {
                         
                        dmsi.model.hideLoading();
                     
                });
            }, function (err) {
                
                dmsi.model.hideLoading();
            });            
                
        },
        
        
        finishLogin: function () {
             var promise,
                 details;

            logWriter.writeLog("model.js",0,"finishLogin()");
            
             try {
                 
                promise = dmsi.model.jsdoUserSession.login(dmsi.model.userDefaults.userId, dmsi.model.userDefaults.userPassword);

                promise.done( function( jsdosession, result, info ) {
                    try {
                        
                        dmsi.model.isUserLoggedIn = true;
                                                
                        var catPromise = jsdosession.addCatalog(dmsi.config.jsdoSettings.catalogURIs);
                
                        catPromise.done( function( jsdosession, result, details ) {
                
                            logWriter.writeLog("model.js",0,"catPromise.done");
                            
                            var userDefaultsPromise = dmsi.model.getUserDefaultsDataSource("", dmsi.model.userDefaults.userId, false);

                            userDefaultsPromise.done(function (detailDataSource) {
                                var that = this;
                                var sequenceSet;
                    
                                detailDataSource.read();
                                var userDefaults = detailDataSource.data()[0];
                                dmsi.model.session.validLocationList = detailDataSource.data()[3];                                
                                dmsi.model.session.userMenuLevel1 = detailDataSource.data()[2];
                                dmsi.model.userDefaults.defaultBranchId = userDefaults[0].default_branch_id;
                                
                                dmsi.model.hideLoading();
                                
                                if (dmsi.model.userDefaults.defaultBranchId === "") {     
                                  $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.loginFailed, "error");
                                  dmsi.model.goLogout();
                                } else {
                                    if (window.plugins && window.plugins.EqatecAnalytics) {

                                      var monitor = window.plugins.EqatecAnalytics.Monitor;
                    
                                      monitor.SetInstallationInfo(dmsi.model.userDefaults.userIdWithTenant);
                    
                                      var track = dmsi.model.userDefaults.tenant.concat(".");
                                      var trackFeature = track.concat(dmsi.model.userDefaults.userId);
                    
                                      monitor.TrackFeature(trackFeature);
                                    }
                                    
                                    dmsi.model.userDefaults.userId = userDefaults[0].user_id;
                                    dmsi.model.userDefaults.defaultBranchId = userDefaults[0].default_branch_id;
                                    dmsi.model.userDefaults.availBranchList = userDefaults[0].avail_branch_list;
                                    dmsi.model.session.branchId = dmsi.model.userDefaults.defaultBranchId;

                                  
                                    logWriter.writeLog("model.js",0,"login: " + dmsi.model.userDefaults.userId + 
                                                          " default branch: " + dmsi.model.userDefaults.defaultBranchId +
                                                          " branch list: " + dmsi.model.userDefaults.availBranchList + 
                                                                " successful");        
                                    
                                    logWriter.writeLog("model.js",0,"Update process starting after login ...");
                                    dmsi.updateProcess.updateIsRunning = true;                                    
                                    dmsi.dbManager.getSequencesQuery().done ( function (sequencesResultSet) {
                                        that.sequenceSet = sequencesResultSet;
                                        
                                        if(that.sequenceSet !== undefined) {
                                            var seq = 0;
                                            
                                            var processLoop = function () {
                                                if ( seq < that.sequenceSet.rows.length) {
                                                    var rowSequence = that.sequenceSet.rows.item(seq).sequence;
                                                    var tranType = that.sequenceSet.rows.item(seq).transaction_type;
                                                    seq+= 1;    
                                                    dmsi.updateProcess.processSequence(rowSequence, tranType).done ( processLoop );
                                                }
                                                return;
                                            }
                                            processLoop();
                                        }
                                    });        
                                    logWriter.writeLog("model.js",0,"Update process after login complete");
                                    dmsi.updateProcess.updateIsRunning = false;
                                    
                                    dmsi.app.replace (dmsi.config.views.mainMenu, "slide");                                    
                                                    
                                }

                            });
                            userDefaultsPromise.fail(function (errorMsg) {
                                logWriter.writeLog("model.js",99,"finishLogin() userDefaultsPromise.fail: " + errorMsg);
                            });
                        });

                        catPromise.fail( function( jsdosession, result, details) {
                            logWriter.writeLog("model.js",99,"finishLogin() catPromise.fail: " + result);
                        });
                    }
                    catch(ex) {
                        details = [{"catalogURI": dmsi.config.jsdoSettings.catalogURIs, errorObject: ex}];
                        logWriter.writeLog("model.js",99,"finishLogin(): promise.done exception: " + result + " " + ex + details);
                    }

                });
                promise.fail( function(jsdosession, result, info) {
                    logWriter.writeLog("model.js",99,"promise fail result: " + result + " info: " + info);
                    
                    dmsi.model.hideLoading();

                    if (dmsi.model.session.sessionTimedOut === false) {
                        $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.loginFailed, "error");
                    }
                });
            }
            catch(ex) {
                logWriter.writeLog("model.js",99,"Catch Fail: " + ex);
            }

        },
    };
}( dmsi ));
