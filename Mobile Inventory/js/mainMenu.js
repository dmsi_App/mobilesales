
( function( dmsi ) {
    "use strict";
    
    dmsi.mainMenu = {

        
        viewModel: kendo.observable ( {
            
            onBackKeyPressed: function() {
                logWriter.writeLog("mainMenu.js",0,"onBackKeyPressed()");
                dmsi.model.goLogout();              
            },

            onInit: function() {
               var that = this;
             
               logWriter.writeLog("mainMenu.js",0,"onInit()");          
                
                that.onBranchInit();
                
                // Drop down menu
                $("#menu").kendoMenu({
                    animation: { open: {effects: "fadeIn"} }
                });
                
                $("#dropDownMenu").kendoDropDownList({
                    animation: { open: {effects: "fadeIn"} }
                });
                                
                $("#mOptions").kendoListView({
                    dataSource: {
                        data: dmsi.model.session.userMenuLevel1
                    },
                    template: $("#mOptionsTemplate").text(), 
                    style: "inset",                    
                    selectable: true,
                    pullToRefresh: true,                    
                    change: onMenuChange                   
                });
                
               function onMenuChange(e) {
                  
                  var list = $('#mOptions').data('kendoListView');
                  var view = list.dataSource.view();
                  
                  logWriter.writeLog("mainMenu.js",0,"onMenuChange(): Selected " + view[list.select().index()].menu_name);
                  if ( view[list.select().index()].menu_name === "Tag Info" ) {
                      dmsi.app.navigate (dmsi.config.views.tagInfo, "slide"); 
                  } else if ( view[list.select().index()].menu_name === "Move Tags" ) {
                      dmsi.app.navigate (dmsi.config.views.moveTags, "slide"); 
                  } else if ( view[list.select().index()].menu_name === "Split Tags" ) {
                      dmsi.app.navigate (dmsi.config.views.splitTags, "slide"); 
                  } else if ( view[list.select().index()].menu_name === "Verify Tags" ) {
                      dmsi.app.navigate (dmsi.config.views.workfiles, "slide"); 
                  } else if ( view[list.select().index()].menu_name === "Work Order Completion" ) {
                      dmsi.app.navigate (dmsi.config.views.workorderCompletion, "slide"); 
                  } else if ( view[list.select().index()].menu_name === "Department Status Update" ) {
                      dmsi.app.navigate (dmsi.config.views.departmentSelection, "slide"); 
                  }
              }

            },
            
              onBranchInit: function() {
                  var branchList = $("#branchList"); 
                  branchList.empty();
                  var branchArray = dmsi.model.userDefaults.availBranchList.split(",");
                  logWriter.writeLog("mainMenu.js",0,"onBranchInit()");
                  branchList.append("<option disabled>" + "Change branch" + "</option>");
                  for(var i=0; i < branchArray.length; i++) {
                      branchList.append("<option>" + branchArray[i] + "</option>");              
                  }          
              },
            
            onShow: function() {
                
                var that = this;
                logWriter.writeLog("mainMenu.js",0,"onShow()");
                document.getElementById("branchbarid").textContent = dmsi.model.session.branchId;
                that.clearValues();                
                that.onBranchInit();
                var emptyDataSource = new kendo.data.DataSource({data: []});
                $("#mOptions").data("kendoListView").setDataSource(emptyDataSource); 
                var newDataSource = new kendo.data.DataSource({data: dmsi.model.session.userMenuLevel1});
                $("#mOptions").data("kendoListView").setDataSource(newDataSource);
                $("#mOptions").data("kendoListView").dataSource.read(); 
                var list = $('#mOptions').data('kendoListView');
                dmsi.app.scroller().reset(); 

                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);
            },  
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },

                        
            clearValues: function() {
                logWriter.writeLog("mainMenu.js",0,"clearValues()");
            },
                    
            onAppDrawerHide: function(e){
                logWriter.writeLog("mainMenu.js",0,"onAppDrawerHide()");
                $("#branchList").blur();
            },
            
            onAppDrawerShow: function(e){
                logWriter.writeLog("mainMenu.js",0,"onAppDrawerShow()");
                document.getElementById("branchList").value = "";
            },
            
            onBranchChange: function(){
                logWriter.writeLog("mainMenu.js",0,"onBranchChange()");    
                
                var cBranch = document.getElementById("branchList").value;
                
                if (dmsi.model.session.branchId !== cBranch) {
                    dmsi.dbManager.outstandingData().done ( function () {
                        if ( dmsi.model.session.outstandingData ) {
                            navigator.notification.alert("There are records waiting to be sent to Agility. You cannot change branches until the records are processed.");
                        }
                        else {
                            dmsi.model.session.branchId = cBranch;    
                            document.getElementById("branchbarid").textContent = cBranch;
                            dmsi.model.session.selectedDept = "";
                            dmsi.model.session.departments = [];
                            dmsi.model.session.reasons = [];
                             $("#departmentSelectionOptionList option:selected").prop("selected", false);
                        }
                    });
                }
                
                $("#appDrawer").data("kendoMobileDrawer").hide();
                                
            },
            
            onLogout: function() {
                logWriter.writeLog("mainMenu.js",0,"onLogout()");
                $("#appDrawer").data("kendoMobileDrawer").hide();
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else {
                        dmsi.model.goLogout();
                    }
                });
            },             

                            
        })
    };

} ( dmsi ) ); //pass in global namespace
