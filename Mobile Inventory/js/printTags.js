(function( dmsi ) {
    "use strict";
    
    dmsi.printTags = {                       
        
        defaultLabelPrinter: "",
        
        viewModel: kendo.observable ( {
            
            onBackKeyPressed: function() {
                logWriter.writeLog("printTags.js",0,"onBackKeyPressed()");
                var that = this;
                if (dmsi.model.session.printTagsResultsTable.length > 0) {
                    navigator.notification.confirm(
                        "If you exit this page the tags listed will not be printed. Do you want to exit?",
                        that.onBackConfirm,
                        "Confirm",
                        ["Exit","Cancel"]
                    );               
                }
                else {
                    dmsi.app.replace (dmsi.config.views.splitTags, "slide:right");  
                }
            },
                                      
            onBackConfirm: function(button) {
                if (button === 1) {
                    dmsi.app.replace (dmsi.config.views.splitTags, "slide:right");    
                }
            },
            
            onLogout : function () {
                logWriter.writeLog("printTags.js",0,"onLogout()");
                var that = this;
                
                dmsi.dbManager.outstandingData().done ( function () {
                    if ( dmsi.model.session.outstandingData ) {
                        navigator.notification.alert(dmsi.config.messages.logoutOutstandingData);
                        return;
                    }
                    else if (dmsi.model.session.printTagsResultsTable.length > 0) {
                        navigator.notification.confirm("If you continue to logout the tags listed will not be printed. Do you want to logout?",that.onLogoutConfirm,"Confirm",["Logout","Cancel"]);                          
                    }
                    else {
                        dmsi.model.goLogout();                       
                    }
                });
            },           
          
            onLogoutConfirm: function(button) {
                if (button === 1) {
                   dmsi.model.goLogout();   
                }
            },
            
             onInit: function() {
                 var that = this;
             
                 logWriter.writeLog("printTags.js",0,"onInit()");

                 $("#tabstrip").kendoTabStrip({
                     animation: {
                        open: {
                            effects: "fadeIn"
                        }
                     },
                    show: that.onTabSelect                
                 });        
                 
                 $("#printTagsStaticNotification").kendoNotification({
                     templates: [{
                         type: "SuccessfulPrint",
                         template: $("#printTagsSuccessfulPrintTemplate").html()
                     },
                     {
                         type: "ErrorPrint",
                         template: $("#printTagsErrorPrintTemplate").html()
                     }],                 
                     autoHideAfter: 4000,                    
                     hideOnClick: true,
                     button: true,
                     position: {
                         pinned: true,
                         top: 0,
                         left: 0
                     }
                     
                 });      
                 
                 $("#printTagsResultList").kendoListView({
                    dataSource: {
                        data: []
                    },
                    template: $("#printTagsResultListTemplate").text(), 
                    style: "inset",                    
                    selectable: false,
                    pullToRefresh: true                 
                });                                       
            },
            
            onShow: function() {
                
                var that = this;
                logWriter.writeLog("printTags.js",0,"onShow()");
                document.getElementById("printTagsbranchbarid").textContent = dmsi.model.session.branchId;  
                that.defaultLabelPrinter = "";
                for ( var ix = 0;ix < dmsi.model.userDefaults.labelPrinterList.length;ix++) {
                      if ( dmsi.model.userDefaults.labelPrinterList[ix] === window.localStorage["labelPrinter_rem"] )
                        that.defaultLabelPrinter = window.localStorage["labelPrinter_rem"];
                }   
                that.clearValues();                   
                document.getElementById("printTagsItemInfo").textContent = dmsi.model.session.printTagsItemInfo;
                var newDataSource = new kendo.data.DataSource({data: dmsi.model.session.printTagsResultsTable}); 
                $("#printTagsResultList").data("kendoListView").setDataSource(newDataSource); 
                $("#printTagsResultList").data("kendoListView").dataSource.read(); 
                dmsi.app.scroller().reset();
                
                // Register the event listener
                document.addEventListener("backbutton", this.onBackKeyPressed, true);
            }, 
            
            onHide: function() {
                document.removeEventListener("backbutton", this.onBackKeyPressed);
            },            
            
            clearValues: function() {
                var that = this;
                logWriter.writeLog("printTags.js",0,"clearValues()");
                document.getElementById("printTagsItemInfo").textContent = "";                       
                var emptyDataSource = new kendo.data.DataSource({data: []});
                dmsi.app.scroller().reset();        
                $("#printTagsResultList").data("kendoListView").setDataSource(emptyDataSource);  
                if (that.defaultLabelPrinter === undefined ||
                    that.defaultLabelPrinter === null ||
                    that.defaultLabelPrinter === "") {
                   document.getElementById("printTagsPrinter").value = ""; 
                }
                else {
                   document.getElementById("printTagsPrinter").value = that.defaultLabelPrinter; 
                }                  
            },              
            
            processPrint: function(e) {                
                var connection = navigator.onLine ? "online" : "offline";
                var that = this;
                
                if (connection === "offline") {
                    $("#staticNotification").data("kendoNotification").show(dmsi.config.messages.noConnection, "error");
                    return;
                }
               
                if (dmsi.model.session.printTagsResultsTable.length === 0) {
                    $("#printTagsStaticNotification").data("kendoNotification").show({ErrorPrintMessage: "There are no tags to print."},"ErrorPrint");    
                    return;
                }
                  
                var labelPrinter = document.getElementById("printTagsPrinter").value.toUpperCase();
                                                                
                if (labelPrinter === undefined ||
                    labelPrinter === null ||
                    labelPrinter === "") {
                    $("#printTagsStaticNotification").data("kendoNotification").show({ErrorPrintMessage: "You must specify a valid printer."},"ErrorPrint");  
                    return;                           
                }
                else {
                    var foundPrinter = false;
                       
                     for ( var ix = 0;ix < dmsi.model.userDefaults.labelPrinterList.length;ix++) {
                         if ( dmsi.model.userDefaults.labelPrinterList[ix] === labelPrinter )
                             foundPrinter = true;
                     }           
                       
                     if(!foundPrinter) {
                        $("#printTagsStaticNotification").data("kendoNotification").show({ErrorPrintMessage: "You must specify a valid printer."},"ErrorPrint");   
                        return;                    
                     }                                                                   
                }
 
                for (var x = 0; x < dmsi.model.session.printTagsResultsTable.length; x++) {
                    dmsi.model.session.printTagsResultsTable[x].printer_id = labelPrinter;  
                }
                
                window.localStorage["labelPrinter_rem"] = labelPrinter;  
                that.defaultLabelPrinter = labelPrinter;          
                
                var promise = dmsi.model.printTag(dmsi.model.session.printTagsResultsTable);

                promise.done(function () {
                    
                     var successMessage = "Printed ";
                     var numTagsPrinted = dmsi.model.session.printTagsResultsTable.length;                     
            
                     if (numTagsPrinted > 1) {                                                   
                         successMessage = successMessage + numTagsPrinted + " tags to printer " + labelPrinter;
                     }
                     else {
                         successMessage = successMessage + " 1 tag to printer " + labelPrinter;
                     }                     
                    
                     that.clearValues();  
                    
                    $("#printTagsStaticNotification").data("kendoNotification").show({SuccessfulPrintMessage: successMessage},"SuccessfulPrint");
                    
                    dmsi.app.replace (dmsi.config.views.splitTags, "slide:right");                                         
                });   
                
                promise.fail(function (errorMsg) {
                    logWriter.writeLog("printTags.js",99,"promise fail: " + errorMsg);                    
                });
            }, 
            
            deleteTagListEntry: function(tagToDelete, tagSequenceToDelete) {
                             var that = this;
                             var indexToDelete = 0;
                
                             logWriter.writeLog("printTags.js",0,"DeleteTagListEntry()");
                             
                             for ( var ix = 0;ix < dmsi.model.session.printTagsResultsTable.length;ix++) {                                 
                                  if ( dmsi.model.session.printTagsResultsTable[ix].tag === tagToDelete) {
                                     indexToDelete = ix;
                                  }
                             }    
                             
                             dmsi.model.session.printTagsResultsTable.splice(indexToDelete, 1);
                             var newDataSource = new kendo.data.DataSource({data: dmsi.model.session.printTagsResultsTable}); 
                             $("#printTagsResultList").data("kendoListView").setDataSource(newDataSource); 
                             $("#printTagsResultList").data("kendoListView").dataSource.read(); 
                             dmsi.app.scroller().reset();          
            },  
            
            clearPrinter: function (e) {
                               
                var printerInput = document.getElementById("printTagsPrinter");
                printerInput.value = "";
                printerInput.text  = "";
                printerInput.focus();                
                               
            },             
        })
    };

} ( dmsi ) ); //pass in global namespace
